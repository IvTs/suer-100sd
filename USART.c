//Project : ������ USART ��� ��������� HART
//        :
//        : ��������� ������ ��������� � USART.h
//        : ������ �������� � ���� ���������:
//        : USART_Init(); - ������������� �����;
//        : parser_USART(); - ������ �������� ������ �� ���������� ������
//        : CRC (*<������>, <�����>) - ������ ����������� ����� CRC
//        :
//        :
//        :
//        :
//        :
//Version : 1.0
//Date    : 27-10-2012. (DD-MM-YY)
//Author  : ����� ������� ������������
//Company : ���"�������-��������"
//Comments: IAR Workbench target descriptor for MSP430 version 4.20.1/WIN

#include "USART.h"

//************************************
//** �/� ������������� ����� USART ***
//************************************
void USART_Init (void)
{
P3SEL |= 0x30;                        // USART0 option select
ME1 |= UTXE0 + URXE0;                 // ��������� ������ USART0 TXD/RXD
UCTL0 |= CHAR + PENA;                 // 8-bit ������, 1 �������� ���, ���� ��� ��������, ��������� ��������� ��� ��������
UTCTL0 |= SSEL0;                      // UCLK= ACLK
UBR00 = 0x80;                         // 3686400Hz/1200��� = 3072 = 0x0C00 // 1843210/1200 = 1536 = 0x0600 // 1000000/1200 = 833 = 0x0341 // (3686400/8) /1200 = 384 = 0x0180
UBR10 = 0x01;                         //
UMCTL0 = 0x00;                        // modulation
UCTL0 &= ~SWRST;                      // Initialize USART state machine
IE1 |= URXIE0 + UTXIE0;               // Enable USART0 RX interrupt

Rx_read = Rx_write = (unsigned char *)&Rx_bufer;// ����������� ���������� ����� ������ ���������� ������
Tx_write = Tx_read = (unsigned char *)&Tx_bufer;//

P3OUT |= 0x02;                        // ����� ����������� �� ����� ������

flag_USART = 0x00;                    // ���� ������� �������� �� USART ����������.
}

//************************************
//***** �/� ��������� ����������� ****
//********* ������ ����� USART *******
//************************************
#pragma vector=USART0RX_VECTOR
__interrupt void EndResive (void)
{
if (count_time_HART <= time_1_HART)   // ���� �� ���������� ������ ������� ������ �� ����� 1 ��������� �������
 {
  if (count_Rx_byte_HART == 58)       // � ���� ��� ���� ������� 58 ����� ������ � ��������� ��� ��������� ���:
  {
  *Rx_write = RXBUF0;               // ���������� �������� ���� � ������, ����� ������� �������� ���������,
                    // �� �� �������� ��� ���������, �.�. ����������� �������� ����
  TBR = 0x00;
  count_time_HART = 0;              // ���������� ������� ������� ����������
  }
  else   // ���� ����� ������ ����� 48 ���� - �� ���������� �������� ������ � ��������� �����.
  {
  count_Rx_byte_HART += 1;          // ����������� ������� �������� ����
  *Rx_write = RXBUF0;               // ���������� �������� ���� � ������, ����� ������� �������� ���������.
  if (Rx_write == ((unsigned char *)&Rx_bufer) + N_Resive - 1) // ���� �������� ����� ������
    {
    Rx_write = (unsigned char *)&Rx_bufer;// ����������� ��������� ����� ������ ������.
    }
  else Rx_write+=1;                 // ����� ��������� �� ��������� ������ ������

  TBR = 0x00;
  count_time_HART = 0;              // ���������� ������� ������� ����������
  }
  }
else
  {
  if (count_time_HART <= time_8_HART) // ���� �� ���������� ������ ������� ������ �� ����� 1 � �� ����� 8 ���������� �������
  {                                 // �� ��� ��� ������ - ������� ����� ��� �����, ����� �� �����������.
  *Rx_write = RXBUF0;               // ���������� �������� ���� � ������, ����� ������� �������� ���������,
                    // �� �� �������� ��� ���������, �.�. ����������� �������� ����
  TBR = 0x00;
  count_time_HART = 0;              // ���������� ������� ������� ����������
  flag_HART_ = 1;                   // ������������� ���� ��������������� � ������ ������������� �����
  }
  else   // ������� ������ ������ �����
  {
  count_Rx_byte_HART = 1;           // ������������� ������� �������� ����
  flag_HART_ = 0;                   // ��� ������ ������ ����� - ���������� ��� ������ ������������� ������
  *Rx_write = RXBUF0;               // ���������� �������� ���� � ������, ����� ������� �������� ���������.
  if (Rx_write == ((unsigned char *)&Rx_bufer) + N_Resive - 1) // ���� �������� ����� ������
    {
    Rx_write = (unsigned char *)&Rx_bufer;// ����������� ��������� ����� ������ ������.
    }
  else Rx_write+=1;                 // ����� ��������� �� ��������� ������ ������

  TBR = 0x00;
  count_time_HART = 0;              // ���������� ������� ������� ����������
  }
  }
}

//************************************
//** �/� �������� ������ ����� USART *
//************************************
#pragma vector=USART0TX_VECTOR
__interrupt void EndTransmit (void)
{
if ((Tx_write != Tx_read) && Tx_read < (((unsigned char *)&Tx_bufer) + N_Transmit - 1)) // ���� � ������ ���� ������ ��� ��������
  {                                   //
  Tx_read +=1;                        // ����� ��������� �� ��������� ������ ������
  TXBUF0 = *Tx_read;                  // ���������� ��������� ���� �� ��������
  }                                   //
else
  {
  flag_USART &=~ 0x01;                // ���������� ���� �������� �� ������.
  Tx_read = (unsigned char *)&Tx_bufer;// �� ���������� �������� - ��������� �� ������ ������ ��� ���������� � �������� ���������� �����.
  P3OUT |= 0x02;
  if (flag_HART_restart) WDTCTL = WDTCNTCL;// ������ ��������� ������ ��� ���� ����� �������� �����
  }
}

//************************************
//*** �/� ������� ��������� ����� ****
//************************************
void parser_USART(void)
{
flag_HART_start = 0;

if (count_Rx_byte_HART > 7)           // ���� ����� ����� ����� ����� 7 ��������
  {
  if (flag_HART_ == 0)                // ���� ����� ������ ��� ��������
  {
  while (Rx_read != Rx_write)       // ���������� �������� ����� ������� � ������� ���������� �����
    {
    if (flag_HART_start)            // ���� ����� ����� ��������� ������ ���� ��������� ���� 0x02 ��� 0x82
    {
    if (*Rx_read == 0x02 || *Rx_read == 0x82) //
      {
      break;                      // ���������� ����� ������ �����
      }
    else
      {
      flag_HART_start = 0;        // ���������� ���� ��������� ���������
      if (*Rx_read == 0x01 || *Rx_read == 0x06 || *Rx_read == 0x81 || *Rx_read == 0x86) break; // ���� ��� ����� ����� ��������� - �� ����� ��������� �� ���, �����������.
      }
    }
    if (*Rx_read == 0xFF && flag_HART_start == 0)// ���� ����� ���� ���������
    {
    flag_HART_start = 1;          //
    }

    if (Rx_read >= ((unsigned char *)&Rx_bufer) + N_Resive - 1) // ���� �������� ����� ������ ���������
    {
    Rx_read = (unsigned char *)&Rx_bufer;// ����������� ��������� ����� ������ ������
    }
    else Rx_read += 1;              // ����� ��������� �� ��������� ������ ������
    count_Rx_byte_HART -= 1;        // - 1 �� ���-�� �������� ����
    }

  if (flag_HART_start)              // ���� ��������� ���� ������
    {
    comnd = (unsigned char *)&Command;
    while (Rx_read != Rx_write && comnd < (unsigned char *)&Command + N_comnd)  // ����������� �������� ���� ��� ��������� � ������������� ����� ��� ��� �������� � ����������� �������
    {
    *comnd = *Rx_read;
    comnd += 1;
    if (Rx_read == ((unsigned char *)&Rx_bufer) + N_Resive - 1) // ���� �������� ����� ������
      {
      Rx_read = (unsigned char *)&Rx_bufer;// ����������� ��������� ����� ������ ������.
      }
    else Rx_read +=1;               // ����� ��������� �� ��������� ������ ������
    }
    comnd = (unsigned char *)&Command; // �������� �� ��������� ����
    if (*comnd == 0x02)             // ���� ���� � ���� ��������� ������
    {
    comnd += 2;                   // ���������� ��������� ����, ����������� �� �������
    }
    else                            // ���� ���� � ���� �������� ������
    {
    comnd += 6;                   // ���������� ��������� ����, ����������� �� �������
    }
    if (*comnd == 254)              // ���� ���� ������� �������� �������� 254 - �� ������� ������� � ����������� ����������
    {
    comnd += 2;                   // ���������� ��������� �� ���-�� ���� � �������� ������
    flag_HART_EXT = 1;            // ������������� ���� ������������ ��� ������ ��� �������� � ����������� ����������� �������
    }
    else
    {
    comnd += 1;                   //
    flag_HART_EXT = 0;            //
    }
    DATA_NUMBER_HART = *comnd;      // ��������� ���-�� �������� ���� ������ ��� ����������� ������� �������
    comnd += (*comnd + 1);          // ��������� ���������� �� ���� �������� ����������� �����
    if (comnd < (unsigned char *)&Command + N_comnd) // ������ �������� ��� ��������� �� ������� �� ������� ������
    {
    count_Rx_byte_HART = comnd - (unsigned char *)&Command + 1; // ��������� ���-�� ���� � �������� ������

    if ( *comnd == CRC ((unsigned char *)&Command, count_Rx_byte_HART - 1)) // ���� CRC ��������� ����� ��������� � ��������� ��������� ID !
      {
      comnd = (unsigned char *)&Command; // ��������� �� ��������� ����
      if (*comnd == 0x02)                //����� ������������ ���������� � �������� ������, �������� ������������ �����
      {
      comnd += 1;
      if ((*comnd & 0x0F) == DEV_ID)// ���� ����� ��������� ����� ����������
        {
        command_parcer ();      // ���� ��������� ����������� �������
        }
      }
      else                        // ����� ������������ ���������� � ������� ������, �������� ������������ �����
      {
      comnd += 1;               // ������ �������� ID ���������� � �������
      temp_HART = 0;
      adres_MASTER = 0;
      if ((*comnd & 0x3F) == UIN [temp_HART]) // �� �������������� ������� ��� ������ ������� � ��� ��������� ������
        {
        adres_MASTER += 1;      //
        }
      temp_HART += 1;
      comnd += 1;

      while (temp_HART < 5)     // ��������� ��������� 4 ����� ����������� ��������������
        {
        if (*comnd == UIN [temp_HART])
        {
        adres_MASTER += 1;    //
        }
        temp_HART += 1;
        comnd += 1;
        }
      if (adres_MASTER == 5)    // ���� ���������� ����� � ������� ��������� � ���������� ������� �������
        {
        command_parcer ();      // ���� ��������� ����������� �������
        }
      else                      // ���� ������ ������ � ���� �������� ������, ���������� ID � ������� �� ��������� � ID ������ ����������
        {                       // �� ��������� �������� ID �� ����������������� ������, � ����������� 11 �������
        comnd -= 5;             // ��������� �� ������ ��������� ��������������
        temp_HART = 0;
        adres_MASTER = 0;
        if ((*comnd & 0x3F) == 0x00) // �� �������������� ������� ��� ������ ������� � ��� ��������� ������
        {
        adres_MASTER += 1;    //
        }
        temp_HART += 1;
        comnd += 1;

        while (temp_HART < 5)   // ��������� ��������� 4 ����� ����������� ��������������
        {
        if (*comnd == 0x00)
          {
          adres_MASTER += 1;  //
          }
        temp_HART += 1;
        comnd += 1;
        }
        if (adres_MASTER == 5)  // ���� ���������� ����� � ������� ����� ��� ����
        {
        if (*comnd == 11)     // ���� ������������� ������� ����� 11
          {
          comnd += 2;         // ��������� �� �������� ���
          ptr_HART = (unsigned char *) 0xF600;// ��������� �� ��� ����������

          temp_HART = 0;
          adres_MASTER = 0;
          while (temp_HART < 6)   // ��������� �������� ��� �� ������������ ������������ ����
          {
          if (*comnd == *ptr_HART)
            {
            adres_MASTER += 1;  //
            }
          temp_HART += 1;
          comnd += 1; ptr_HART += 1;
          }
          if (adres_MASTER == 6)  // ���� ��� ���������
          {
          command_parcer ();// ���� ��������� ����������� �������
          }
          }
        }
        }
      }
      }
    }
    }
  }
  }

Rx_read = Rx_write;                   // |
CDR_END_HART = 0;                     // | ����� ��������� ������, ���������� ���� ������ ������ � ��������� ��������� ���������
}

//************************************
//*�/� ���������� CRC ��������� �����*
//************************************
unsigned char CRC (unsigned char *nach, unsigned char dlina)
{
unsigned char temp_CRC=0;
while(dlina--)
  {
  temp_CRC ^= *nach;
  if (nach == ((unsigned char *)&Rx_bufer) + N_Resive - 1) // ���� �������� ����� ������ ���������
  {
  nach = (unsigned char *)&Rx_bufer;// ����������� ��������� ����� ������ ������.
  }
  else nach += 1;                     // ����� ��������� �� ��������� ������ ������
  }
return temp_CRC;
}

//************************************
//** �/� ������ � ����� ����������� **
//******** ������ �������� ***********
void write_to_tx_bufer (void)
{
ptr_HART += 3;
*Tx_write = *ptr_HART;
Tx_write += 1;
ptr_HART -= 1;
*Tx_write = *ptr_HART;
Tx_write += 1;
ptr_HART -= 1;
*Tx_write = *ptr_HART;
Tx_write += 1;
ptr_HART -= 1;
*Tx_write = *ptr_HART;
Tx_write += 1;
}

//************************************
//*** �/� ������� �������� ������� ***
//************************************
void command_parcer (void)
{
Tx_write = (unsigned char *)&Tx_bufer;
temp_HART = PREAMBULE;
while (temp_HART)                     // ��������� �������� �������
  {
  *Tx_write = 0xFF;                   // ���������� ���������
  Tx_write += 1;
  temp_HART -= 1;
  }

comnd = (unsigned char *)&Command;    // ��������� �� ������ �������� �������
comnd += 1;
if (*comnd & 0x80) adres_MASTER = 0x80; // ��������� ����� ��������
else adres_MASTER = 0x00;
comnd -= 1;                           // ��������� �� ��������� ���� ��� ����������� ������
                    //**************************************
if (*comnd & 0x80)                    //** ���� ���� � ���� �������� ������ **
  {                                   //**************************************
  frame_HART = 1;                     // ��������� ��� ����� ���������� ���������� � ���� �������� ������
  comnd += 6;                         // ��������� �� ����� �������
  *Tx_write = 0x86;                   // ��������� ���� �������� �� ������������ � ��������
  temp_HART = 5;
  while (temp_HART)                   // � ����� �������� ���������� ������������� �������
  {
  Tx_write += 1;
  *Tx_write = UIN [5 - temp_HART];  //
  if (temp_HART == 5) *Tx_write |= adres_MASTER;  // ����� �������� ���������� �������� ������������ �����
  temp_HART -= 1;
  }
  }                                   //***************************************
else                                  //** ���� ���� � ���� ��������� ������ **
  {                                   //***************************************
  frame_HART = 0;                     // ��������� ��� ����� ���������� ���������� � ���� ��������� ������
  comnd += 2;                         // ��������� �� ����� �������
  *Tx_write = 0x06;                   // ��������� ���� �������� �� ������������ � ��������
  }

Tx_write += 1;
if (frame_HART == 0 )                 //
  {
  *Tx_write = DEV_ID | adres_MASTER;  // �������� ID ���������� � ����� �������� ���������� �������� ������������ �����
  Tx_write += 1;
  }
*Tx_write = *comnd;                   // � �������� ������� �������� ����� ����������� �������
Tx_write += 1;                        // ��������� ��������� ������ ����������� �� ������� ����

switch (*comnd)
{
// ������������� �������
case 0:   // ������� ���������� �������������
  {
  *Tx_write = 14;                     // ���������� ������������ ���� = 12 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  *Tx_write = 254;                    // ���� 0 ��� ����������
  Tx_write += 1;
  *Tx_write = CODE_MADE;              // ���� 1 ��� ������������
  Tx_write += 1;
  *Tx_write = TYPE_DEV;               // ���� 2 ��� ���� ����������
  Tx_write += 1;
  *Tx_write = PREAMBULE;              // ���� 3 ����� ��������
  Tx_write += 1;
  *Tx_write = VERSION_NUMBER_1;       // ���� 4 ������ ������������� ������
  Tx_write += 1;
  *Tx_write = VERSION_NUMBER_2;       // ���� 5 ������ ������������� ������
  Tx_write += 1;
  *Tx_write = REV_PROG;               // ���� 6 ������ ������������ �����������
  Tx_write += 1;
  *Tx_write = REV_DEV;                // ���� 7 ������ ����������� �����������
  Tx_write += 1;
  *Tx_write = 1;                      // ���� 8 ����� ������� ����������
  Tx_write += 1;
  *Tx_write = UIN [2];                // ���� 9-11 ����������������� ����� ����������
  Tx_write += 1;
  *Tx_write = UIN [3];                //
  Tx_write += 1;
  *Tx_write = UIN [4];                //
  Tx_write += 1;
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }
case 1:   // ������� ��������� ����������
  {
  *Tx_write = 7;                      // ���������� ������������ ���� = 5 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  *Tx_write = edinica_izmer;          // ������� ��������� ���, ���
  Tx_write += 1;
  ptr_HART = (unsigned char *) & davlenie;   // �������� ������
  write_to_tx_bufer ();               //
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;                              //
  }
case 2:   // ������� ��� � ������� ���������
  {
  *Tx_write = 10;                     // ���������� ������������ ���� = 8 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  ptr_HART = (unsigned char *) & i_out;// �������� �������� ��������� ����
  write_to_tx_bufer ();               //
  ptr_HART = (unsigned char *) & procent_diappazona;// �������� �������� ������� ���������
  write_to_tx_bufer ();               //
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }
case 3:   // ������� ��� � �������� ������� (����������������) ����������
  {
  *Tx_write = 26;                     // ���������� ������������ ���� = 24 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  ptr_HART = (unsigned char *) & i_out;// �������� �������� ��������� ����
  write_to_tx_bufer ();
  *Tx_write = edinica_izmer;          // ������� ��������� ��� (���)
  Tx_write += 1;
  ptr_HART = (unsigned char *) & davlenie;// �������� �������� ����������� ��������
  write_to_tx_bufer ();               //
  *Tx_write = 0x24;                   // ������� ��������� ��
  Tx_write += 1;
  f = 0;
  if (SERVICE_MODE) ptr_HART = (unsigned char *) & u;   // �������� �������� ��������� ���������� � ������� ������ ����
  else ptr_HART = (unsigned char *) & f;                // � ��������� ������, ����� �������� ����
  write_to_tx_bufer ();               //
  *Tx_write = 0x25;                   // ������� ��������� ��
  Tx_write += 1;
  if (SERVICE_MODE) ptr_HART = (unsigned char *) & R_mosta;// �������� �������� ������������� ��������� �����
  else ptr_HART = (unsigned char *) & f;                   // � ��������� ������, ����� �������� ����
  write_to_tx_bufer ();               //
  *Tx_write = 0x20;                   // ������� ��������� � (������� �������)
  Tx_write += 1;
  ptr_HART = (unsigned char *) & temperatura;// �������� �������� �����������
  write_to_tx_bufer ();               //
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }
case 6:   // �������� ����� �������� ����� ����������
  {
  if (DATA_NUMBER_HART == 1)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 1 ���� ������
  {
  *Tx_write = 3;                      // ���������� ������������ ���� = 1 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� ������ (ID)
  *Tx_write = *comnd;                 // � ����� �������� �������� ID
  Tx_write += 1;

  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  if (*comnd <= 15)                   // ��������� �������� ������ �� ���������� ��������
  {
  DEV_ID = *comnd;                  //
  flag_conf_changed = 1;            // ���� - ������������ ������� ��������
  EEPROM_write_conf ();             // ��������� ������ � EEPROM
  if (DEV_ID != 0x00)               // ���� ����� ���������� �� ������� - ������ ������� ����� ����������
    {
    flag_i_out_auto = 0;            // ������ � ����� �������������� ����
    loop_current = 4.000;
    OUT_current ();                 // ������������� �������� ��������� ���� ������ 4 ��
    }
  else
    {
    flag_i_out_auto = 1;            // ��������� ������ �������� ������
    }
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  STATUS1_HART = 0x02;              // �������� ������ "�������� �����"
  add_status2_HART();
  }
  }
  else
  {
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }
case 11:  // ������� ���������� �������������, ��������� � �������� �����
  {
  *Tx_write = 14;                     // ���������� ������������ ���� = 12 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  *Tx_write = 254;                    // ���� 0 ��� ����������
  Tx_write += 1;
  *Tx_write = CODE_MADE;              // ���� 1 ��� ������������
  Tx_write += 1;
  *Tx_write = TYPE_DEV;               // ���� 2 ��� ���� ����������
  Tx_write += 1;
  *Tx_write = PREAMBULE;              // ���� 3 ����� ��������
  Tx_write += 1;
  *Tx_write = VERSION_NUMBER_1;       // ���� 4 ������ ������������� ������
  Tx_write += 1;
  *Tx_write = VERSION_NUMBER_2;       // ���� 5 ������ ������������� ������
  Tx_write += 1;
  *Tx_write = REV_PROG;               // ���� 6 ������ ������������ �����������
  Tx_write += 1;
  *Tx_write = REV_DEV;                // ���� 7 ������ ����������� �����������
  Tx_write += 1;
  *Tx_write = 0x01;                   // ���� 8 ����� ������� ����������
  Tx_write += 1;
  *Tx_write = UIN [2];                // ���� 9-11 ����������������� ����� ����������
  Tx_write += 1;
  *Tx_write = UIN [3];                //
  Tx_write += 1;
  *Tx_write = UIN [4];                //
  Tx_write += 1;
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }
case 12:  // ������� ���������
  {
  *Tx_write = 26;                     // ���������� ������������ ���� = 24 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  temp_HART = 0;
  ptr_HART = (unsigned char *) 0xF400;// ��������� �� ������ EEPROM (������� 5)
  while (temp_HART < 24)              // ��������� ���������
   {
   *Tx_write = *ptr_HART;             //
   temp_HART += 1;
   Tx_write += 1; ptr_HART += 1;      //
   }
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }
case 13:  // ������� ���, ���������, ����
  {
  *Tx_write = 23;                     // ���������� ������������ ���� = 21 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������

  ptr_HART = (unsigned char *) 0xF600;// ��������� �� ������ EEPROM (������� 4)
  while (temp_HART < 21)              // �������� � ����� ���, ����������, ����
   {
   *Tx_write = *ptr_HART;             //
   temp_HART += 1;
   Tx_write += 1; ptr_HART += 1;      //
   }
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }
case 14:  // ������� ���������� � �������������� �������� ��������� ��������
  {
  *Tx_write = 18;                     // ���������� ������������ ���� = 16 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������

  temp_HART = 0;
  ptr_HART = (unsigned char *) 0xF200;
  while (temp_HART < 3)               // �������� � ����� �������� ����� ��������������� ��������
  {
  *Tx_write = *ptr_HART;
  temp_HART += 1;
  Tx_write += 1; ptr_HART += 1;
  }

  ptr_HART = (unsigned char *) 0xF800;

  *Tx_write = *ptr_HART;              // ������� ��������� ��� ������� (���)
  Tx_write += 1;

  ptr_HART += 1;
  write_to_tx_bufer ();

  ptr_HART += 4;
  write_to_tx_bufer ();               //

  ptr_HART += 4;
  write_to_tx_bufer ();               //
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }
case 15:  // ������� ���������� � ��������� ����������
  {
  *Tx_write = 19;                     // ���������� ������������ ���� = 17 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������

  *Tx_write = 0x00;                   // alarm select code
  Tx_write += 1;

  *Tx_write = koren;                  // Transfer function code
  Tx_write += 1;

  *Tx_write = edinica_izmer;          // ������� ��������� ��� ��������� ���������� (���, ���)
  Tx_write += 1;

  USART_temp_float = perevod_v_davlenie (diapazon_v);
  ptr_HART = (unsigned char *) & USART_temp_float;// �������� �������� �������� �������
  write_to_tx_bufer ();
  USART_temp_float = perevod_v_davlenie (diapazon_n);
  ptr_HART = (unsigned char *) & USART_temp_float;// �������� �������� ������� �������
  write_to_tx_bufer ();                     //

  ptr_HART = (unsigned char *) & damping_value; // ����� �������������
  write_to_tx_bufer ();                     //

  *Tx_write = 0x00;                   // Write protect code
  Tx_write += 1;

  *Tx_write = DISTRIBUTE_CODE;        // private label distributor code
  Tx_write += 1;
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }
case 16:  // ��������� Final Assembly Number
  {
  *Tx_write = 5;                      // ���������� ������������ ���� = 3 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  temp_HART = 0;
  ptr_HART = (unsigned char *) 0xFA00;// ��������� �� ������ EEPROM (������� 2)
  while (temp_HART < 3)               // ��������� ���������
   {
   *Tx_write = *ptr_HART;             //
   temp_HART += 1;
   Tx_write += 1; ptr_HART += 1;      //
   }
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }
case 17:  // �������� ���������
  {
  if (DATA_NUMBER_HART == 24)         // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 24 ����� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 26;                   // ���������� ������������ ���� = 24 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  write_EEPROM_MESSAGE (comnd + 2); // ���������� �������� ������ � EEPROM
  temp_HART = 0;
  ptr_HART = (unsigned char *) 0xF400;// ��������� �� ������ EEPROM (������� 5)
  while (temp_HART < 24)            // ��������� ���������
    {
    *Tx_write = *ptr_HART;          //
    temp_HART += 1;
    Tx_write += 1; ptr_HART += 1;   //
    }
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }
case 18:  // �������� ���, ���������, ����
  {
  if (DATA_NUMBER_HART == 21)         // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 21 ���� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 23;                   // ���������� ������������ ���� = 21 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  write_EEPROM_TEG (comnd + 2);     // ���������� �������� ������ � EEPROM
  temp_HART = 0;
  ptr_HART = (unsigned char *) 0xF600;// ��������� �� ������ EEPROM (������� 5)
  while (temp_HART < 21)            // ��������� ���������
    {
    *Tx_write = *ptr_HART;          //
    temp_HART += 1;
    Tx_write += 1; ptr_HART += 1;   //
    }
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }
case 19:  // �������� Final Assembly Number
  {
  if (DATA_NUMBER_HART == 3)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 3 ����� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 5;                    // ���������� ������������ ���� = 3 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  write_EEPROM_ASSEMBLY_NUMBER (comnd + 2);// ���������� �������� ������ � EEPROM
  temp_HART = 0;
  ptr_HART = (unsigned char *) 0xFA00;// ��������� �� ������ EEPROM (������� 5)
  while (temp_HART < 3)             // ��������� ���������
    {
    *Tx_write = *ptr_HART;          //
    temp_HART += 1;
    Tx_write += 1; ptr_HART += 1;   //
    }
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

//************************************
//***** ���������������� ������� *****
//************************************
case 34:  // �������� ����� �������������
  {
  if (DATA_NUMBER_HART == 4)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 4 ����� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 6;                    // ���������� ������������ ���� = 4 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  ptr_HART = (unsigned char *) & USART_temp_float + 3;
  comnd += 2;                       // ��������� �� �������� �������� ����� �������������
  temp_HART = 0;
  while (temp_HART < 4)             // ������������ �������� �������� � USART_temp_float ��� �������� ��������
    {
    *ptr_HART = *comnd;             //
    temp_HART += 1;
    comnd += 1; ptr_HART -= 1;      //
    }
  if (USART_temp_float >= 0.05 && USART_temp_float <= 20.00) // ���� �������� �������� ��������� � ���������
    {
    damping_value = USART_temp_float;
    N_integr = (unsigned char) (damping_value / 0.0786) + 1;
    N_integr_tmp=1;
    flag_conf_changed = 1;        // ���� - ������������ ������� ��������
    EEPROM_write_conf ();           // ��������� �������� � EEPROM
    STATUS1_HART = 0x00;            // �������� ������ "��"
    add_status2_HART();
    }
  else
    {
    STATUS1_HART = 0x03;            // �������� ������ "���������� �������� ������� �����"
    add_status2_HART();
    }
  ptr_HART = (unsigned char *) & USART_temp_float;// �������� �������� �������� ������� �������������
  write_to_tx_bufer ();
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

case 35:  // ���������� ����� ������� ��������� � ������� �������� ���������
  {
  if (DATA_NUMBER_HART == 9)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 9 ���� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 11;                     // ���������� ������������ ���� = 9 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ������ ��������� �������������
  if (*comnd == 0x0C || *comnd == 0xED || *comnd == 0x05 || *comnd == 0xEF || *comnd == 0x0A || *comnd == 0xB0 || *comnd == 0x07 || *comnd == 0x0B || *comnd == 0x08) // ���� �������� ������� � ������ �������� ���������
  {
  volatile char edinici = *comnd;
  comnd += 1;                       // ��������� �� ������ ��������� �������� �������� ������������
  temp_HART = 0;
  ptr_HART = (unsigned char *) & tmp_diapazon_v + 3;
  while (temp_HART < 4)             // ������������ �������� �������� � tmp_diapazon_v ��� �������� ��������
    {
    *ptr_HART = *comnd;             //
    temp_HART += 1;
    comnd += 1; ptr_HART -= 1;      //
    }
  tmp_diapazon_v = perevod_v_uslovnie_edinici (tmp_diapazon_v, edinici); // ����������� �������� ��������� �������� ������� � �������������� ��������

  if (tmp_diapazon_v <= 1.0)        // ���� ������� �������� �� ��������� �������� ��������� ��������� ��
    {
    if (tmp_diapazon_v >= diapazon_n_PD - 0.001)  // ���� ������� �������� �� ������ ������� ��������� ��������� ��
    {
    temp_HART = 0;
    ptr_HART = (unsigned char *) & tmp_diapazon_n + 3;
    while (temp_HART < 4)         // ������������ �������� �������� � tmp_diapazon_n ��� �������� ��������
      {
      *ptr_HART = *comnd;         //
      temp_HART += 1;
      comnd += 1; ptr_HART -= 1;  //
      }
    tmp_diapazon_n = perevod_v_uslovnie_edinici (tmp_diapazon_n, edinici); // ����������� �������� ��������� ������� ������� � �������������� ��������

    if (tmp_diapazon_n <= 1.0)    // ���� ������ �������� ������ �������� ��������� ��������� ��
      {
      if (tmp_diapazon_n >= diapazon_n_PD - 0.001)  // ���� ������ �������� �� ������ ������� ��������� ��������� ��
      {
      if (tmp_diapazon_v - tmp_diapazon_n >= min_diapazon_PD - 0.001) // �������� �� ����������� ��������
        {
        diapazon_v = tmp_diapazon_v;
        diapazon_n = tmp_diapazon_n;
        flag_conf_changed = 1;        // ���� - ������������ ������� ��������
        EEPROM_write_conf ();   // ��������� �������� � EEPROM
        STATUS1_HART = 0x00;    // �������� ������ "��"
        add_status2_HART();
        }
      else
        {
        STATUS1_HART = 0x0E;    // �������� ������ "�������� ������� ���"
        add_status2_HART();
        }
      }
      else
      {
      STATUS1_HART = 0x0A;      // �������� ������ "������ �������� ��������� ������� ����"
      add_status2_HART();
      }
     }
    else
      {
      STATUS1_HART = 0x09;        // �������� ������ "������ �������� ��������� ������� ������"
      add_status2_HART();
      }
    }
    else
    {
    STATUS1_HART = 0x0C;          // �������� ������ "������� �������� ��������� ������� ����"
    add_status2_HART();
    }
    }
  else
    {
    STATUS1_HART = 0x0B;            // �������� ������ "������� �������� ��������� ������� ������"
    add_status2_HART();
    }
  }
  else
  {
  STATUS1_HART = 0x12;              // �������� ������ "�������� ��� ������ ���������"
  add_status2_HART();
  }
  comnd = (unsigned char *)&Command;  // ��������� �� �������� ������
  if (frame_HART) comnd += 8;
  else comnd += 4;
  temp_HART = 0;
  while (temp_HART < 9)               // ������������ �������� �������� � �������� �������
  {
  *Tx_write = *comnd;               //
  temp_HART += 1;
  comnd += 1; Tx_write += 1;        //
  }
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

case 38:  // ����� ����� ��������� ������������
  {
  *Tx_write = 2;                      // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
    flag_conf_changed = 0;        // ���� - ������������ ������� ��������
    EEPROM_write_conf();
    STATUS1_HART = 0x00;              // �������� ������ "��"
    add_status2_HART();
  }
  else
  {
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  break;
  }

case 40:  // ����/����� � ����� �������������� ����
  {
  if (DATA_NUMBER_HART == 4)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 4 ����� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 6;                      // ���������� ������������ ���� = 4 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ����

  temp_HART = 0;
  ptr_HART = (unsigned char *) & USART_temp_float + 3;
  while (temp_HART < 4)               // ������������ �������� �������� � USART_temp_float ��� �������� ��������
  {
  *ptr_HART = *comnd;               //
  temp_HART += 1;
  comnd += 1; ptr_HART -= 1;        //
  }

  if (DEV_ID != 0x00)                 // ���� ���������� ����� ����������
  {
  STATUS1_HART = 11;                // �������� ������ "������� ����� ���������"
  add_status2_HART();
  }
  else                                // � ������� ������
  {
  if (USART_temp_float < 3.5 && USART_temp_float > 0)
    {
    STATUS1_HART = 4;               // �������� ������ "�������� �������� ������� ����"
    add_status2_HART();
    }
  else
    {
    if (USART_temp_float > 23.0)
    {
    STATUS1_HART = 3;             // �������� ������ "�������� �������� ������� �������"
    add_status2_HART();
    }
    else
    {
    if (USART_temp_float == 0)
      {
      flag_i_out_auto = 1;        // ���� �������� �������� 0 - ������� �� ������ �������������� ����
      loop_current = i_out;
      OUT_current ();             //
      }
    else
      {
      flag_i_out_auto = 0;        // ������ � ����� �������������� ����
      loop_current = USART_temp_float;
      OUT_current ();             // ������������� �� ������ �������� �������� ����
      }
    STATUS1_HART = 0x00;          // �������� ������ "��"
    add_status2_HART();
    }
    }
  }

  ptr_HART = (unsigned char *) & loop_current;// �������� ������� �������� ��������� ����
  write_to_tx_bufer ();
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

case 41:  // ������ �� ���������� ���������������
  {
  *Tx_write = 2;                      // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������

  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }

case 42:  // ������������ �������
  {
  *Tx_write = 2;                      // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  flag_HART_restart = 1;              // ������������� ���� ��� ������������ ������� ����� �������� ��������� � ����������� ������� ������������
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }

case 43:  // ���������� ���� ��������� ����������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 2;                      // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  temp_HART = proverka_delta_P((delta_P + p), 0);

  if (temp_HART == 0)                 // ���� �������� �� ����������� ������ ������ �������
  {
  delta_P += p;                     //
  flag_conf_changed = 1;        // ���� - ������������ ������� ��������
  EEPROM_write_conf ();             // ��������� ����� �������� ��������� ��������
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  if (temp_HART == 1)
    {
    STATUS1_HART = 0x09;            // �������� ������ "������������� ���� ������� ����������, ������ 5%"
    add_status2_HART();
    }
  else
    {
    STATUS1_HART = 0x0A;            // �������� ������ "������������� ���� ������� ����������, ������ -5%"
    add_status2_HART();
    }
  }
  }
  }
  else
  {
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  break;
  }

case 44:  // ���������� ����� ������� ��������� ��������� ����������
  {
  if (DATA_NUMBER_HART == 1)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 1 ���� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 3;                      // ���������� ������������ ���� = 1 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ������ ��������� �������������
  if (*comnd == 0x0C || *comnd == 0xED || *comnd == 0x05 || *comnd == 0xEF || *comnd == 0x0A || *comnd == 0xB0 || *comnd == 0x07 || *comnd == 0x0B || *comnd == 0x08)// ������ ���� ������� ���������� ������� ���������
  {
  edinica_izmer = *comnd;
  flag_conf_changed = 1;        // ���� - ������������ ������� ��������
  EEPROM_write_conf ();             // ��������� ����� �������� ��������� ��������
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  STATUS1_HART = 0x02;              // �������� ������ "�������� ��� ������ ���������"
  add_status2_HART();
  }
  *Tx_write = *comnd;                 // � ����� �������� �������� ��������
  Tx_write += 1;
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

case 45:  // ��������� ���� ���
  {
  if (DATA_NUMBER_HART == 4)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 4 ����� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 6;                      // ���������� ������������ ���� = 4 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ����

  temp_HART = 0;
  ptr_HART = (unsigned char *) & USART_temp_float + 3;
  while (temp_HART < 4)               // ������������ �������� �������� � USART_temp_float ��� �������� ��������
  {
  *ptr_HART = *comnd;               //
  temp_HART += 1;
  comnd += 1; ptr_HART -= 1;        //
  }

  if (DEV_ID != 0x00)                 // ���� ���������� ����� ����������
  {
  STATUS1_HART = 11;                // �������� ������ "������� ����� ���������"
  add_status2_HART();
  }
  else                                // � ������� ������
  {
  if (USART_temp_float < 3.5)
    {
    STATUS1_HART = 4;               // �������� ������ "�������� �������� ������� ����"
    add_status2_HART();
    }
  else
    {
    if (USART_temp_float > 4.5)
    {
    STATUS1_HART = 3;             // �������� ������ "�������� �������� ������� �������"
    add_status2_HART();
    }
    else
    {
    //b_current = (unsigned int) (4.0 / (USART_temp_float / b_current));// ����� �������� ������������ �������� ���� ���
    //EEPROM_write_conf ();         // ��������� ����� �������� ������������ � EEPROM

    tok_low_input = USART_temp_float; // ��������� �������� �������� ���� ��� ������� ������������� ��� ���������� � ����� 20 ��

    loop_current = USART_temp_float;
    OUT_current ();               // ������������� �� ������ �������� �������� ����

    STATUS1_HART = 0x00;          // �������� ������ "��"
    add_status2_HART();
    }
    }
  }

  ptr_HART = (unsigned char *) & loop_current;// �������� ������� �������� ��������� ����
  write_to_tx_bufer ();
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

case 46:  // ��������� ������������ �������� ���
  {
  if (DATA_NUMBER_HART == 4)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 4 ����� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 6;                      // ���������� ������������ ���� = 4 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ����

  temp_HART = 0;
  ptr_HART = (unsigned char *) & USART_temp_float + 3;
  while (temp_HART < 4)               // ������������ �������� �������� � USART_temp_float ��� �������� ��������
  {
  *ptr_HART = *comnd;               //
  temp_HART += 1;
  comnd += 1; ptr_HART -= 1;        //
  }

  if (DEV_ID != 0x00)                 // ���� ���������� ����� ����������
  {
  STATUS1_HART = 11;                // �������� ������ "������� ����� ���������"
  add_status2_HART();
  }
  else                                // � ������� ������
  {
  if (USART_temp_float < 19.5)
    {
    STATUS1_HART = 4;               // �������� ������ "�������� �������� ������� ����"
    add_status2_HART();
    }
  else
    {
    if (USART_temp_float > 20.5)
    {
    STATUS1_HART = 3;             // �������� ������ "�������� �������� ������� �������"
    add_status2_HART();
    }
    else
    {
    //volatile unsigned long yk;
    //yk = b_current + (long)((20.0 - 4.0)/k_current); // �������� ��� ��� �������� � ��� ������� ��� ��� ���� 20 ��
    //k_current = (USART_temp_float - 4.0)/(yk - b_current);// ����� �������� ������������ �������� ���

    tok_hi_input = USART_temp_float; // ������������ � ��� ���������� ��� ���� ����� �� �������� ���������� � ������� ������� �������������

    k_current = (tok_hi_input - tok_low_input) / ((long)((16.) / k_current)); // �������� ����� ����������� �������� ���
    b_current = b_current + (int)((4.0 - tok_low_input) / k_current);           // � ����������� ��������

    flag_conf_changed = 1;        // ���� - ������������ ������� ��������
    EEPROM_write_conf ();         // ��������� ����� �������� ������������ � EEPROM

    loop_current = USART_temp_float;
    OUT_current ();               // ������������� �� ������ �������� �������� ����

    STATUS1_HART = 0x00;          // �������� ������ "��"
    add_status2_HART();
    }
    }
  }

  ptr_HART = (unsigned char *) & loop_current;// �������� ������� �������� ��������� ����
  write_to_tx_bufer ();
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

case 47:  // ���������� ������������ ������� ��������� ����������
  {
  if (DATA_NUMBER_HART == 1)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 1 ���� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 3;                      // ���������� ������������ ���� = 1 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� ��������
  if (*comnd == 0x00 || *comnd == 0x01)
  {
  koren = *comnd;
  flag_conf_changed = 1;        // ���� - ������������ ������� ��������
  EEPROM_write_conf ();             // ��������� �������� � EEPROM
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  STATUS1_HART = 0x02;              // �������� ������ "�������� �����"
  add_status2_HART();
  }
  *Tx_write = *comnd;                 // � ����� �������� �������� ��������
  Tx_write += 1;
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

  case 48:  // ������� �������������� ������
    /* 
    * 0-5 ����� - Device-Specific Status                                                ����� ������������ �� ������ �������. �������� � 0�00 ����� 0�01 - ��� �������� ������ 4-20                                 
    * 6 ���� - Extended Device Status (����������� ������ ����������)                   0x01 - Maintenance required (��������� ����������� ������������)           
    *                                                                                   0x02 - Device variable alert (�����-���� ���������� ���������� ��������� � ��������� ������� ��� ��������������)              
    * 7 ���� - Device Operating Mode                                                    �������������� � ������������ HART                       
    * 8-10 ����� - Analog Channel Saturated
    * 11-13 ����� - Analog Channel Fixed                                                ���������� 11� ���� ��� �������� ����� �������������� ��������� ����  
    * 14-24 ����� - Device-Specific Status                                              ����� ������������ �� ������ �������. �������� � 24 ����� ���/����� ������ error_number                                                    
    */
    {
        *Tx_write = 27;                                                                 // ���������� ������������ ���� = 25 + 2 ����� ������
        Tx_write += 3;                                                                  // ��������� �� ������������ ������
        *Tx_write = 0x01;                                                               // ��� �������� ������: 4-20 (����� ��� ��������� HART Config, ��� ����� ����� �� ���������� ����� 0-5 ��) 
        Tx_write += 1;                                                                  //
        temp_HART = 0;
        while (temp_HART < 5)                                                           //
        {
            *Tx_write = 0x00;                                                           //
            temp_HART += 1;                                                             //
            Tx_write += 1;                                                              //
        }
        //----6-�� ����-----------------------------------------------------------------
/*        if((error_number == EEPROM_ERR) || (error_number == SENS_ERR) || (error_number == ADC_ERR)) // ���� �������� ������ ��������� � ����������� ���������� �������, �� ���������� ��� 0�01
        {
            *Tx_write = 0x01;                                                             
            Tx_write += 1;                                                                  
        }
        if((error_number == PRES_OVER_HI) || (error_number == PRES_OVER_LO))                // ���� �������� ������ ��������� � ������� ����� ���� ���������� �� �������, �� ���������� ��� 0�02
        {
            *Tx_write = 0x02;                                                             
            Tx_write += 1;                                                                  
        }
        if(error_number == ALL_OK)                                                        // ���� ������ ���, �� ���������� 0�00
        {
            *Tx_write = 0x00;                                                             
            Tx_write += 1;                                                                  
        }
*/
        *Tx_write = 0x00;                                 // �� �������� ���������� � ����������� ��������� �������                                                            
        Tx_write += 1;
        //-----7-�� ����----------------------------------------------------------------
        temp_HART = 0;

        *Tx_write = 0x00 ;                                 
        Tx_write += 1;

        //-----8-�� ����----------------------------------------------------------------
    //    *Tx_write = 0x00 | flag_loop_current_saturated;                                 // ���� ������� ����� � ���������
        *Tx_write = 0x00;                               // �� �������� ����������, ��� ���������� ������ 1-24 � ���������
        Tx_write += 1;
        
        while (temp_HART < 2)                                                           //
        {
            *Tx_write = 0x00;                                                           //
            temp_HART += 1;                                                             //
            Tx_write += 1;                                                              //
        }

        //-----11-�� ����----------------------------------------------------------------
    //    *Tx_write = 0x01 & (~flag_i_out_auto);                                          // ���� �������������� ��������� ����
        *Tx_write = 0x00;                                                             //
        Tx_write += 1;                                                                  // �� �������� ����������, ��� ���������� ������ 1-24 � ������ ���� ����

        temp_HART = 0;
        while (temp_HART < 12)                                                          // ��� ��������� ����� ��������� ������
        {
            *Tx_write = 0x00;                                                           //
            temp_HART += 1;                                                             //
            Tx_write += 1;                                                              //
        }
        *Tx_write = error_number;                                                       // 24� ���� ���������� ���/����� ������ (���� �� ����)                                                     
        Tx_write += 1;
        STATUS1_HART = 0x00;                                                            // �������� ������ "��"
        add_status2_HART();
        break;  
    }

case 49:  // �������� �������� ����� ��
  {
 if (DATA_NUMBER_HART == 3)           // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 3 ����� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 5;                      // ���������� ������������ ���� = 3 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� ��������
  write_EEPROM_SERIAL_PD (comnd);     // ��������� �������� ����� �� � EEPROM
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();

  *Tx_write = *comnd;                 // � ����� �������� �������� ��������
  Tx_write += 1; comnd += 1;
  *Tx_write = *comnd;
  Tx_write += 1; comnd += 1;
  *Tx_write = *comnd;
  Tx_write += 1;
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

case 59:  // �������� ����� ��������
  {
  if (DATA_NUMBER_HART == 1)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 1 ���� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 3;                      // ���������� ������������ ���� = 1 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ����� ��������

  if (*comnd >= 5 && *comnd <= 20)    // ���� �������� �������� ��������� � ���������
  {
  PREAMBULE = *comnd;
  flag_conf_changed = 1;        // ���� - ������������ ������� ��������
  EEPROM_write_conf ();             // ��������� �������� � EEPROM
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  if (*comnd > 20)  STATUS1_HART = 0x03;  // �������� ������ "���������� �������� ������� �����"
  else STATUS1_HART = 0x04;               // �������� ������ "���������� �������� ������� ���"
  add_status2_HART();
  }
  *Tx_write = *comnd;                 // �������� �������� �������� ����� ��������
  Tx_write += 1;
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

//************************************
//** ������������� ������� ������� ***
//************************************
case 128:  // ������� ��������� �������� ������
  {
  *Tx_write = 3;                      // ���������� ������������ ���� = 1 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  *Tx_write = tok_signal_inv;         // ��� �������� ������: 4-20 ��� 20-4
  Tx_write += 1;                      //
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }

case 129:  // �������� ��������� �������� ������
  {
  if (DATA_NUMBER_HART == 1)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 1 ���� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
   {
  *Tx_write = 3;                      // ���������� ������������ ���� = 1 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ����� ��������

  if (DEV_ID != 0x00)                 // ���� ���������� ����� ����������
   {
   STATUS1_HART = 11;                 // �������� ������ "������� ����� ���������"
   add_status2_HART();
   }
  else
  {
  if (*comnd == 0 || *comnd == 1)   // ���� �������� �������� ��������� � ���������
    {
    tok_signal_inv = *comnd;
    flag_conf_changed = 1;        // ���� - ������������ ������� ��������
    EEPROM_write_conf ();           // ��������� �������� � EEPROM
    STATUS1_HART = 0x00;            // �������� ������ "��"
    add_status2_HART();
    }
  else
    {
    STATUS1_HART = 12;              // �������� ������ "���������� �������� ������� �����"
    add_status2_HART();
    }
  }
  *Tx_write = *comnd;                 // �������� �������� ��������
  Tx_write += 1;
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

case 130:  // ������� ��������� ������ ������ ��������� ������
  {
  *Tx_write = 3;                      // ���������� ������������ ���� = 1 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  *Tx_write = magnit_key_enable;      // ���������� ������ ��������� ������
  Tx_write += 1;                      //
  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  break;
  }

case 131:  // �������� ��������� ������ ������ ��������� ������
  {
  if (DATA_NUMBER_HART == 1)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 1 ���� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 3;                      // ���������� ������������ ���� = 1 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ����� ��������

  if (*comnd == 0 || *comnd == 1)   // ���� �������� �������� ��������� � ���������
    {
    magnit_key_enable = *comnd;
    flag_conf_changed = 1;        // ���� - ������������ ������� ��������
    EEPROM_write_conf ();           // ��������� �������� � EEPROM
    STATUS1_HART = 0x00;            // �������� ������ "��"
    add_status2_HART();
    }
  else
    {
    STATUS1_HART = 8;               // �������� ������ "���������� �������� �����"
    add_status2_HART();
    }
  *Tx_write = *comnd;                 // �������� �������� ��������
  Tx_write += 1;
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

case 132:  // ���������� ���
  {
  if (DATA_NUMBER_HART == 5)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 5 ���� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 7;                      // ���������� ������������ ���� = 5 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ������ ���������
  volatile char ed_izmereniya;
  //volatile float k_pr;
  if (*comnd == 0x0C || *comnd == 0xED || *comnd == 0x05 || *comnd == 0xEF || *comnd == 0x0A || *comnd == 0xB0 || *comnd == 0x07 || *comnd == 0x0B || *comnd == 0x08)// ������ ���� ������� ���������� ������� ���������
  {
  ed_izmereniya = *comnd;

  comnd += 1;                       // ��������� �� ������ ��������� �������� ��������
  temp_HART = 0;
  ptr_HART = (unsigned char *) & USART_temp_float + 3;
  while (temp_HART < 4)             // ������������ �������� �������� � USART_temp_float ��� �������� ��������
    {
    *ptr_HART = *comnd;             //
    temp_HART += 1;
    comnd += 1; ptr_HART -= 1;      //
    }

  znach_vpi = perevod_v_uslovnie_edinici (USART_temp_float ,ed_izmereniya); // ��������� �������� �������� �������� � �������� ������� ��� �������� ����������� ��������� �������������

    //k_pr = k_pressure / (znach_vpi - znach_npi);

  if (znach_vpi > (diapazon_v - 0.02) && znach_vpi < (diapazon_v + 0.02))//k_pr >= 0.98 && k_pr <= 1.02) // ���� ��������� ����������� �������� �� �������� � ������� - ��������� ����� ������������
    {
    //k_pressure = k_pr;
    //b_pressure -= znach_npi;
    k_pressure = k_pressure * ((diapazon_v - diapazon_n) / (znach_vpi - znach_npi));  // ����������� ��������
    b_pressure = b_pressure + (diapazon_n - znach_npi); // ����������� ��������

    EEPROM_write_pressure_cal ();   // ��������� ����� ������������� ������������ � EEPROM

    STATUS1_HART = 0x00;            // �������� ������ "��"
    add_status2_HART();
    }
  else                              // ���� �������� �������� ��� �������, �������� �� ������
    {
    if (znach_vpi > (diapazon_v + 0.02))
    {
    STATUS1_HART = 0x03;          // �������� ������ "���������� �������� �����"
    add_status2_HART();
    }
    else
    {
    STATUS1_HART = 0x04;          // �������� ������ "���������� �������� ������� ���"
    add_status2_HART();
    }
    }
  }
  else
  {
  STATUS1_HART = 0x02;              // �������� ������ "�������� ��� ������ ���������"
  add_status2_HART();
  }
  *Tx_write = ed_izmereniya;          // � ����� �������� �������� ��������
  Tx_write += 1;
  ptr_HART = (unsigned char *) & USART_temp_float;// �������� ������� �������� ��������� ����
  write_to_tx_bufer ();
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

case 133:  // ���������� ���
  {
  if (DATA_NUMBER_HART == 5)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 5 ���� ������
  {
  if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
  *Tx_write = 7;                      // ���������� ������������ ���� = 5 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ������ ���������
  volatile char ed_izmereniya;
  if (*comnd == 0x0C || *comnd == 0xED || *comnd == 0x05 || *comnd == 0xEF || *comnd == 0x0A || *comnd == 0xB0 || *comnd == 0x07 || *comnd == 0x0B || *comnd == 0x08)// ������ ���� ������� ���������� ������� ���������
  {
  ed_izmereniya = *comnd;

  comnd += 1;                       // ��������� �� ������ ��������� �������� ��������
  temp_HART = 0;
  ptr_HART = (unsigned char *) & USART_temp_float + 3;
  while (temp_HART < 4)             // ������������ �������� �������� � USART_temp_float ��� �������� ��������
    {
    *ptr_HART = *comnd;             //
    temp_HART += 1;
    comnd += 1; ptr_HART -= 1;      //
    }

  znach_npi = perevod_v_uslovnie_edinici (USART_temp_float ,ed_izmereniya); // ��������� �������� �������� �������� � �������� ������� ��� �������� ����������� ��������� �������������

  if (znach_npi > (diapazon_n - 0.02) && znach_npi < (diapazon_n + 0.02))//znach_npi >= -0.02 && znach_npi <= 0.02) // ���� �������� �������� �������� � ������� - ��
    {
    STATUS1_HART = 0x00;            // �������� ������ "��"
    add_status2_HART();            //
    }
  else                              // ���� �������� �������� ��� �������, �������� �� ������
    {
    if (znach_vpi > (diapazon_n + 0.02))
    {
    STATUS1_HART = 0x03;          // �������� ������ "���������� �������� �����"
    add_status2_HART();
    }
    else
    {
    STATUS1_HART = 0x04;          // �������� ������ "���������� �������� ������� ���"
    add_status2_HART();
    }
    }
  }
  else
  {
  STATUS1_HART = 0x02;              // �������� ������ "�������� ��� ������ ���������"
  add_status2_HART();
  }
  *Tx_write = ed_izmereniya;          // � ����� �������� �������� ��������
  Tx_write += 1;
  ptr_HART = (unsigned char *) & USART_temp_float;// �������� ������� �������� ��������� ����
  write_to_tx_bufer ();
  }
  else
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    //
  STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  break;
  }

case 136:  // �������������� ��������� �������� �������
  {
    *Tx_write = 2;                      // ���������� ������������ ���� = 0 + 2 ����� ������
    Tx_write += 3;                      // ��������� �� ������������ ������

    if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
  {
    STATUS1_HART = 0x00;              // �������� ������ "��"
    add_status2_HART();

    // ���� � ������ ������� ������������� ���� �������� ������������, �� ��������� �� CRC 
    // � ��������������� ��� ���������
    CALL_ptr = (unsigned char *) Addr_cur_EEPROM_conf;
    CRC16 (CALL_ptr, 126);                                          // ����������� �������� CRC ��� ����� ������ � ����������� �������
    CALL_ptr = (unsigned char *) Addr_cur_EEPROM_conf + 126;
    if (crc_H == *CALL_ptr)                                         // ��� ��������� ������ �������� CRC ������ � ����������� �������
    {
      CALL_ptr += 1;
      if (crc_L == *CALL_ptr)
        {
        read_store_conf ();                                         // ���� ����������� ����� ��������� - ������ �������� ������� �� EEPROM
        }
      else                                                        // ���� ����������� ����� EEPROM �� ��������� - �� ������� �� ��������� ��������� �� ������
        {                                                           // Err-6
        // ���� ��������������� ����������� ��������
        diapazon_v = 1;                   // ��� ������������� 1:1
        diapazon_n = 0;                   // ���
        tok_signal_inv = 0x00;            // ������� ������ 4-20 ��
        edinica_izmer = 0x0C;             // ������� ��������� - ���
        koren = 0;                        // �������� ����������� ��������� �������
        magnit_key_enable = 1;            // ������ ��������� ������ ���������
        DEV_ID = 0;                       // 0 ����� ���������� � ����
        damping_value = 2.5;              // ����� ������������� 2,5 ���.
        current_alarm = 0;                // ������������ �� �������� ������ Lo
        //        flagLang = 0;           // ���� ���� - ������� 
        }
    }
    else
    {
      // ���� ��������������� ����������� ��������
      diapazon_v = 1;                   // ��� ������������� 1:1
      diapazon_n = 0;                   // ���
      tok_signal_inv = 0x00;            // ������� ������ 4-20 ��
      edinica_izmer = 0x0C;             // ������� ��������� - ���
      koren = 0;                        // �������� ����������� ��������� �������
      magnit_key_enable = 1;            // ������ ��������� ������ ���������
      DEV_ID = 0;                       // 0 ����� ���������� � ����
      damping_value = 2.5;              // ����� ������������� 2,5 ���.
      current_alarm = 0;                // ������������ �� �������� ������ Lo
      //    flagLang = 0;               // ���� ���� - �������
      flag_conf_changed = 1;        // ���� - ������������ ������� ��������

    }

    EEPROM_write_conf ();             // ��������� �������� � EEPROM

    k_pressure = 1, b_pressure = 0;   // ��������� �������� ��������� �������������� �� ��������

    EEPROM_write_pressure_cal ();     // ��������� ����� ������������� ������������ � EEPROM
    /*
    uslEd = 0;                          // �������� ������� 0 - �� ���,
    for (unsigned char i = 0; i < 8; i++)           // � �������� �������� ������ �������
      nameUslEd[i] = ' ';
    
    vpiUslEd = 0.0;                       // ������� � ������ ������� �������� ������ = 0
    npiUslEd = 0.0;

    write_EEPROM_USL_ED();                    // ������ ���������� �������� ������
    */
  }
    else
  {
    STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
    add_status2_HART();
  }
    break;
  }

    case 137:  // ������� ��������� ����������� ������ (������ ��������� ������������ �� NAMUR NE43)
    {
        *Tx_write = 3;                                                                  // ���������� ������������ ���� = 1 + 2 ����� ������
        Tx_write += 3;                                                                  // ��������� �� ������������ ������
        *Tx_write = current_alarm;                                                      // �������� ��������� ����������� ������ (������ ��������� ������������ �� NAMUR NE43)
        Tx_write += 1;                                                                  //
        STATUS1_HART = 0x00;                                                            // �������� ������ "��"
        add_status2_HART();
        break;
    }

    case 138:  // �������� ��������� ����������� ������ (������ ��������� ������������ �� NAMUR NE43)
    {
        if (DATA_NUMBER_HART == 1)                                                      // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 1 ���� ������
        {
        if ((P2IN & 0x80) && (HART_disable == 0))                     // ���� ���������� �� ������������� �� ������
            {
                *Tx_write = 3;                                                          // ���������� ������������ ���� = 1 + 2 ����� ������
                Tx_write += 3;                                                          // ��������� �� ������������ ������
                comnd += 2;                                                             // ��������� �� �������� �������� ����� ��������

                if (*comnd == 0 || *comnd == 1)                                         // ���� �������� �������� ��������� � ���������
                {
                    current_alarm = *comnd;
                    flag_conf_changed = 1;        // ���� - ������������ ������� ��������
                    EEPROM_write_conf ();                                               // ��������� �������� � EEPROM
                    STATUS1_HART = 0x00;                                                // �������� ������ "��"
                    add_status2_HART();
                }
                else
                {
                    STATUS1_HART = 8;                                                   // �������� ������ "���������� �������� �����"
                    add_status2_HART();
                }
                *Tx_write = *comnd;                                                     // �������� �������� ��������
                Tx_write += 1;
            }
            else
            {
                *Tx_write = 2;                                                          // ���������� ������������ ���� = 0 + 2 ����� ������
                Tx_write += 3;                                                          //
                STATUS1_HART = 0x07;                                                    // �������� ������ "� ������ ������ �� ������"
                add_status2_HART();
            }
        }
        else                                                                            // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
        {
            *Tx_write = 2;                                                              // ���������� ������������ ���� = 0 + 2 ����� ������
            Tx_write += 3;                                                              // ��������� �� ������������ ������
            STATUS1_HART = 0x05;                                                        // �������� ������ "������������ ����� �������� ���� ���������� �����"
            add_status2_HART();
        }
        break;
    }

  case 140:   // ������� ������ � ������ ��������� �������� � ���� ��������
  //�� � ��� ��������: 0x00 � ��, 0x01 � ��, 0x02 � ���, 0x03 � ��, 0x04 ���;
  //  <������ (char)> � ������, ���������� ������ ��������� ��������, 
  //� ������� �������� ����������� ���������������. ����� ������  5 ��������, 
  //������� ���� ���������� ������. ���� ������ ��������� �������� ����� 5 ��������, 
  //�� ����������� ������� ����������� ��������� � ����� ������;
  {
    *Tx_write = 8;                                                                  // ���������� ������������ ���� = 6 + 2 ����� ������
    Tx_write += 3;                                                                  // ��������� �� ������������ ������
    temp_HART = 0;
    ptr_HART = (unsigned char *) 0xF80D;                                            // ��������� �� �����, ��� �������� ������ � ��� �� 
    //ptr_HART = modelPD;                                                           // �������� ������ � ��� �� 
    //ptr_HART = (unsigned char *) &modelPD [0];                                    // �������� ������ � ��� �� 
    while (temp_HART < 6)
    {
      *Tx_write = *ptr_HART;                                                      //
      temp_HART += 1;
      Tx_write += 1; ptr_HART += 1;                                               //
    }                                                                               //
    STATUS1_HART = 0x00;                                                            // �������� ������ "��"
    add_status2_HART();
    break;
  }
/*
  case 141:  // ������ ������ � ����� ������ �������� ���� � ������ ��� ���������� ��-2 
    {
        if (DATA_NUMBER_HART == 1)                                                      // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 1 ���� ������
        {
          if ((P2IN & 0x80) && (HART_disable == 0))                     // ���� ���������� �� ������������� �� ������
            {
                *Tx_write = 3;                                                          // ���������� ������������ ���� = 1 + 2 ����� ������
                Tx_write += 3;                                                          // ��������� �� ������������ ������
                comnd += 2;                                                             // ��������� �� �������� �������� ����� ��������

                if (*comnd == 0 || *comnd == 1)                                         // ���� �������� �������� ��������� � ���������
                {
                    flagLang = *comnd;
                    EEPROM_write_conf ();                                               // ��������� �������� � EEPROM
                    STATUS1_HART = 0x00;                                                // �������� ������ "��"
                    add_status2_HART();
                }
                else
                {
                    STATUS1_HART = 8;                                                   // �������� ������ "���������� �������� �����"
                    add_status2_HART();
                }
                *Tx_write = *comnd;                                                     // �������� �������� ��������
                Tx_write += 1;
            }
            else
            {
                *Tx_write = 2;                                                          // ���������� ������������ ���� = 0 + 2 ����� ������
                Tx_write += 3;                                                          //
                STATUS1_HART = 0x07;                                                    // �������� ������ "� ������ ������ �� ������"
                add_status2_HART();
            }
        }
        else                                                                            // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
        {
            *Tx_write = 2;                                                              // ���������� ������������ ���� = 0 + 2 ����� ������
            Tx_write += 3;                                                              // ��������� �� ������������ ������
            STATUS1_HART = 0x05;                                                        // �������� ������ "������������ ����� �������� ���� ���������� �����"
            add_status2_HART();
        }
        break;
    }
    */
  /*
  case 142:  // ������ ������ � ����� ������ �������� ���� � ������ ��� ���������� ��-2 
    {
        *Tx_write = 3;                                                                  // ���������� ������������ ���� = 1 + 2 ����� ������
        Tx_write += 3;                                                                  // ��������� �� ������������ ������
        *Tx_write = flagLang;                                                         // �������� ������� ���� ����
        Tx_write += 1;                                                                  //
        STATUS1_HART = 0x00;                                                            // �������� ������ "��"
        add_status2_HART();
        break;
    }
    */

    case 143: // ��������� ���� ����� ����� �������� ������������ �������� 
    {
      unsigned char *ptrTypeTrans;                            // ��������� ��������� �� ��� ��������� �������� � ������
    ptrTypeTrans = (unsigned char *) 0xF80D;                      // ������ ��������� �� ����� � ������ � ����� ��������� ��������
    if (*ptrTypeTrans == 1)                               // ���� ������ -��
    {
      if (DATA_NUMBER_HART == 4)                                                      // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 4 ���� ������
          {
            if ((P2IN & 0x80) && (HART_disable == 0))                     // ���� ���������� �� ������������� �� ������
              {
                  *Tx_write = 2;                                                          // ���������� ������������ ���� = 1 + 2 ����� ������
                  Tx_write += 3;                                                          // ��������� �� ������������ ������
                  ptr_HART = (unsigned char *) & USART_temp_float + 3;
                  comnd += 2;                                                             // ��������� �� �������� �������� ����� ��������
                  temp_HART = 0;
                    while (temp_HART < 4)                                                   // ������������ �������� �������� � USART_temp_float ��� �������� ��������
                    {
                        *ptr_HART = *comnd;             
                        temp_HART += 1;
                        comnd += 1; ptr_HART -= 1;      
                    }
                  if (USART_temp_float >= 85.44 && USART_temp_float <= 108.70)            // ���� �������� �������� ��������� � ���������
                  {
                      USART_temp_float = perevod_v_uslovnie_edinici (USART_temp_float, 0x0C);
                      temp_HART = proverka_delta_P((delta_P + (p - USART_temp_float)), 1);
            if (temp_HART == 0)                           // ������ �������� �������� ����
            {
              delta_P += p - USART_temp_float;                    // ������������ �������� �������� ����
              flag_conf_changed = 1;        // ���� - ������������ ������� ��������
              EEPROM_write_conf ();                           // ��������� ����� �������� ��������� ����
                        STATUS1_HART = 0x00;                                            // �������� ������ "��"
                        add_status2_HART();
                    } 
                    else
                    {
                      if (temp_HART == 1)
                      {
                        STATUS1_HART = 0x09;                                        // �������� ������ "������������� ���� ������� ����������, ������ 5%"
                          add_status2_HART();
                      }
                      else
                      {
                        STATUS1_HART = 0x0A;                                        // �������� ������ "������������� ���� ������� ����������, ������ -5%"
                              add_status2_HART();
                      }
                    }
                  }
                  else
                  {
                      STATUS1_HART = 8;                                                   // �������� ������ "���������� �������� �����"
                      add_status2_HART();
                  }
                  // *Tx_write = *comnd;                                                     // �������� �������� ��������
                  // Tx_write += 1;
              }
              else
              {
                  *Tx_write = 2;                                                          // ���������� ������������ ���� = 0 + 2 ����� ������
                  Tx_write += 3;                                                          //
                  STATUS1_HART = 0x07;                                                    // �������� ������ "� ������ ������ �� ������"
                  add_status2_HART();
              }
          }
          else                                                                            // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
          {
              *Tx_write = 2;                                                              // ���������� ������������ ���� = 0 + 2 ����� ������
              Tx_write += 3;                                                              // ��������� �� ������������ ������
              STATUS1_HART = 0x05;                                                        // �������� ������ "������������ ����� �������� ���� ���������� �����"
              add_status2_HART();
          }
    }
    else
    {
      *Tx_write = 2;                                                                  // ���������� ������������ ���� = 2 ����� ������
      Tx_write += 3;                                                                  // ��������� �� ������������ ������
      STATUS1_HART = 64;                                                              // �������� ������ "������� �� �������"
      add_status2_HART();
    }
      
      break;
    }
/*
    case 144:  // ������ ������ � ���/���� �������� �������� � ������ ��� ���������� ��-2 
    {
        if (DATA_NUMBER_HART == 1)                                                      // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 1 ���� ������
        {
          if ((P2IN & 0x80) && (HART_disable == 0))                   // ���� ���������� �� ������������� �� ������
            {
                *Tx_write = 3;                                                          // ���������� ������������ ���� = 1 + 2 ����� ������
                Tx_write += 3;                                                          // ��������� �� ������������ ������
                comnd += 2;                                                             // ��������� �� �������� �������� ����� ��������

                if (*comnd == 0 || *comnd == 1)                                         // ���� �������� �������� ��������� � ���������
                {
                    uslEd = *comnd;
                    write_EEPROM_USL_ED();                                              // ��������� �������� � EEPROM
                    STATUS1_HART = 0x00;                                                // �������� ������ "��"
                    add_status2_HART();
                }
                else
                {
                    STATUS1_HART = 8;                                                   // �������� ������ "���������� �������� �����"
                    add_status2_HART();
                }
                *Tx_write = *comnd;                                                     // �������� �������� ��������
                Tx_write += 1;
            }
            else
            {
                *Tx_write = 2;                                                          // ���������� ������������ ���� = 0 + 2 ����� ������
                Tx_write += 3;                                                          //
                STATUS1_HART = 0x07;                                                    // �������� ������ "� ������ ������ �� ������"
                add_status2_HART();
            }
        }
        else                                                                            // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
        {
            *Tx_write = 2;                                                              // ���������� ������������ ���� = 0 + 2 ����� ������
            Tx_write += 3;                                                              // ��������� �� ������������ ������
            STATUS1_HART = 0x05;                                                        // �������� ������ "������������ ����� �������� ���� ���������� �����"
            add_status2_HART();
        }
        break;
    }
*/
/*
    case 145:  // ������ ������ � ���/���� �������� �������� � ������ ��� ���������� ��-2 
    {
        *Tx_write = 3;                                                                  // ���������� ������������ ���� = 1 + 2 ����� ������
        Tx_write += 3;                                                                  // ��������� �� ������������ ������
        *Tx_write = uslEd;                                                          // �������� ������� ��������� ���/���� ���.��
        Tx_write += 1;                                                                  //
        STATUS1_HART = 0x00;                                                            // �������� ������ "��"
        add_status2_HART();
        break;
    }
*/
/*
    case 146:  // ������ ���, ���, �������� �������� ������ � ������ ��� ���������� ��-2  
    { 
      if (DATA_NUMBER_HART == 16)                                 // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 13 ���� ������
    {   
      if ((P2IN & 0x80) && (HART_disable == 0))                     // ���� ���������� �� ������������� �� ������
            { 
        *Tx_write = 18;                                         // ���������� ������������ ���� = 13 + 2 ����� ������
        Tx_write += 3;                                          // ��������� �� ������������ ������
        comnd += 2;                                             // ��������� �� �������� ��������
  
        temp_HART = 0;  
        ptr_HART = (unsigned char *) & vpiUslEd + 3;  
        while (temp_HART < 4)                                                     // ������������ �������� �������� � �������� �������
        { 
          *ptr_HART = *comnd;   
          *Tx_write = *comnd;                               // ������ ��������� �������� �������
          temp_HART += 1;
          comnd += 1; 
          Tx_write += 1;
          ptr_HART -= 1;
        }

        temp_HART = 0;
        ptr_HART = (unsigned char *) & npiUslEd + 3;
        while (temp_HART < 4)                                                     // ������������ �������� �������� � �������� �������
        { 
          *ptr_HART = *comnd;   
          *Tx_write = *comnd;                               // ������ ��������� �������� �������
          temp_HART += 1; 
          comnd += 1;   
          Tx_write += 1;  
          ptr_HART -= 1;  
        } 
  
        temp_HART = 0;  
        while (temp_HART < 8)                                                     // ������������ �������� �������� � �������� �������
        { 
          nameUslEd[temp_HART] = *comnd;                      // �������� ������ � ��������� �������� ������
          *Tx_write = *comnd;                               // ������ ��������� �������� �������
          comnd += 1; 
          Tx_write += 1;
          temp_HART += 1;
          ptr_HART -= 1;
        }
        write_EEPROM_USL_ED();                                                  // ��������� �������� � EEPROM
      }
            else
            {
                *Tx_write = 2;                                                            // ���������� ������������ ���� = 0 + 2 ����� ������
                Tx_write += 3;                                                            //
                STATUS1_HART = 0x07;                                                      // �������� ������ "� ������ ������ �� ������"
                add_status2_HART();
            }
    }
    else                                                        // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
    {
      *Tx_write = 2;                                          // ���������� ������������ ���� = 0 + 2 ����� ������
      Tx_write += 3;                                          // ��������� �� ������������ ������
      STATUS1_HART = 0x05;                                    // �������� ������ "������������ ����� �������� ���� ���������� �����"
      add_status2_HART();
    }
      break;
    }
*/
/*    case 147:  // ������ ���, ���, �������� �������� ������ � ������ ��� ���������� ��-2  
  {
    *Tx_write = 18;                                             // ���������� ������������ ���� = 16 + 2 ����� ������
    Tx_write += 3;                                              // ��������� �� ������������ ������
    ptr_HART = (unsigned char *) & vpiUslEd;                      // �������� �������� ��������� ����
    write_to_tx_bufer ();
    ptr_HART = (unsigned char *) & npiUslEd;                      // �������� �������� ����������� ��������
    write_to_tx_bufer ();                                       //
    ptr_HART = (unsigned char *) & nameUslEd;
    while (temp_HART < 8)
    {
      *Tx_write = *ptr_HART;                                                        //
      temp_HART += 1;
      Tx_write += 1; ptr_HART += 1;                                                 //
    }                                 

    STATUS1_HART = 0x00;                                        // �������� ������ "��"
    add_status2_HART();
    break;
  }
*/
  case 148: // ���������� ������� ������������ ������� 
  {
    *Tx_write = 2;                      // ���������� ������������ ���� = 0 + 2 ����� ������
      Tx_write += 3;                      // ��������� �� ������������ ������
    if (SERVICE_MODE == 1)
    {
      if ((P2IN & 0x80) && (HART_disable == 0)) // ���� ���������� �� ������������� �� ������
      {
        STATUS1_HART = 0x00;              // �������� ������ "��"
        add_status2_HART();

        // ������ ���������� ����� ���������, � �� ��� ������������ � ������� ��������� � ������ ������
        // diapazon_v = 1;                   // ��� ������������� 1:1
        // diapazon_n = 0;                   // ���
        // tok_signal_inv = 0x00;            // ������� ������ 4-20 ��
        // edinica_izmer = 0x0C;             // ������� ��������� - ���
        // koren = 0;                        // �������� ����������� ��������� �������
        // magnit_key_enable = 1;            // ������ ��������� ������ ���������
        // DEV_ID = 0;                       // 0 ����� ���������� � ����
        // damping_value = 2.5;              // ����� ������������� 2,5 ���.
        // current_alarm = 0;                // ������������ �� �������� ������ Lo
        // flagLang = 0;           // ���� ���� - �������

        // EEPROM_write_conf ();             // ��������� �������� � EEPROM

        write_current_conf();               // ��������� �������� � EEPROM
      }
        else
      {
        STATUS1_HART = 0x07;              // �������� ������ "� ������ ������ �� ������"
        add_status2_HART();
      }
    }
    else
    {
      STATUS1_HART = 64;                                                              // �������� ������ "������� �� �������"
      add_status2_HART();
    }
    break;
  }

  case 241:   // �������� ������ � ������ ��������� �������� � ���� ��������
  //�� � ��� ��������: 0x00 � ��, 0x01 � ��, 0x02 � ���, 0x03 � ��, 0x04 ���;
  //  <������ (char)> � ������, ���������� ������ ��������� ��������, 
  //� ������� �������� ����������� ���������������. ����� ������  5 ��������, 
  //������� ���� ���������� ������. ���� ������ ��������� �������� ����� 5 ��������, 
  //�� ����������� ������� ����������� ��������� � ����� ������;
  {
    
    if (SERVICE_MODE == 1)
    {
      if (DATA_NUMBER_HART == 6)                                  // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 6 ���� ������
        {
        *Tx_write = 8;                                                              // ���������� ������������ ���� = 6 + 2 ����� ������
        Tx_write += 3;                                                              // ��������� �� ������������ ������
        comnd += 2;                                                                 // ��������� �� �������� �������� ������ ������ �������������
        if (*comnd < 0x05)                              // ��������, ��� � 1 ����� ������ ��� ��������� �������� �� 0 �� 4 
        {
          temp_HART = 0;
          while (temp_HART < 6)                                                   // ������������ �������� �������� � �������� �������
          {
            modelPD[temp_HART] = *comnd;                      // �������� ������ ������ ��
            *Tx_write = *comnd;                             // ������ ��������� �������� �������
            temp_HART += 1;
            comnd += 1; 
            Tx_write += 1;
          }

          STATUS1_HART = 0x00;                                                    // �������� ������ "��"
          add_status2_HART();
        }
        else
        {
          STATUS1_HART = 0x08;                                                    // �������� ������ "���������� �������� �����"
          add_status2_HART();
        }
      }
      else
      {
        *Tx_write = 2;                                                              // ���������� ������������ ���� = 2 ����� ������
        Tx_write += 3;                                                              // ��������� �� ������������ ������
        STATUS1_HART = 0x05;                                                        // �������� ������ "����������� ����� ����"
        add_status2_HART();
      }
    }
    else
    {
      *Tx_write = 2;                                                                  // ���������� ������������ ���� = 2 ����� ������
      Tx_write += 3;                                                                  // ��������� �� ������������ ������
      STATUS1_HART = 64;                                                              // �������� ������ "������� �� �������"
      add_status2_HART();
    }

    break;
  }

case 245:  // ������� ������� � ��������� �����
  {
  *Tx_write = 2;                      // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������

  if (DATA_NUMBER_HART == 6)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 6 ���� ������
  {
  comnd += 2;                         // ��������� �� ������ ��������� ������
  volatile char nmm = 0;
  temp_HART = 0;
  while (temp_HART < 6)
  {
  if (PASSWORD [temp_HART] == *comnd) nmm += 1;
  temp_HART += 1; comnd += 1;
  }
  if (nmm == 6)                       // ���� �������� ������ ���������
  {
  SERVICE_MODE = 1;                 // ����������� ������ � ��������� �����.
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else                                // ���� ������ �� ���������
  {
  STATUS1_HART = 64;                // �������� ������ "������� �� �������"
  add_status2_HART();
  }
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  STATUS1_HART = 64;                // �������� ������ "������� �� �������"
  add_status2_HART();
  }
  break;
  }

case 246:  // ������ EEPROM
  {
  if (SERVICE_MODE == 1)
  {
  *Tx_write = 20;                     // ���������� ������������ ���� = 16 + 2 ����� ������ + 2 ����� �����
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ������, ������ ������ ������
  volatile unsigned int adress_EEPROM;
  ptr_HART = (unsigned char *) & adress_EEPROM + 1;
  *ptr_HART = *comnd;
  *Tx_write = *comnd;
  Tx_write += 1;

  ptr_HART -= 1; comnd += 1;
  *ptr_HART = *comnd;
  *Tx_write = *comnd;
  Tx_write += 1;

  if (adress_EEPROM <=0xFFF0 && adress_EEPROM >= 0x1000) // �������� ������ �� ���������� ��������
  {
  temp_HART = 0;
  ptr_HART = (unsigned char *) adress_EEPROM; // ��������� �� ������ EEPROM
  while (temp_HART < 16)            // ��������� 16 ���� ������ � ���������� ������
    {
    *Tx_write = *ptr_HART;          //
    temp_HART += 1;
    Tx_write += 1; ptr_HART += 1;   //
    }
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  Tx_write -= 5;
  *Tx_write = 4;                    // ���������� ������������ ���� = 2 + 2 ����� ������
  Tx_write += 5;                    //

  STATUS1_HART = 0x08;              // �������� ������ "������������ ��������"
  add_status2_HART();
  }
  }
  else                                // ���� �� � ��������� ������, �� ������� ��� ��� ����� �������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 64;                // �������� ������ "������� �� �������"
  add_status2_HART();
  }
  break;
  }

case 247:  // ������ ����� ���
  {
  if (SERVICE_MODE == 1)
  {
  *Tx_write = 10;                     // ���������� ������������ ���� = 8 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������

  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();

  ptr_HART = (unsigned char *) & b_ADC_mosta;// �������� �������� ���� ��� ��� ����������� �������� ��������
  write_to_tx_bufer ();
  ptr_HART = (unsigned char *) & r_ADC_mosta;// �������� �������� ���� ��� ��� ����������� �������� ������������� ��������� �����
  write_to_tx_bufer ();
  }
  else                                // ���� �� � ��������� ������, �� ������� ��� ��� ����� �������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 64;                // �������� ������ "������� �� �������"
  add_status2_HART();
  }
  break;
  }

case 248:  // �������� ��������� ����� ��
  {
  if (SERVICE_MODE == 1)
  {
  if (DATA_NUMBER_HART == 3)          // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 3 ����� ������
  {
  *Tx_write = 5;                      // ���������� ������������ ���� = 3 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� ��������
  write_EEPROM_ZAVOD_NUMBER_DD (comnd);// ��������� �������� ����� �� � EEPROM

  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();

  *Tx_write = *comnd;                 // � ����� �������� �������� ��������
  UIN [2] = *comnd;                   // � ����������� �������� ����� � UIN ��� ����� ��������� ������ �� ������������ �������
  Tx_write += 1; comnd += 1;
  *Tx_write = *comnd;
  UIN [3] = *comnd;                   //
  Tx_write += 1; comnd += 1;
  *Tx_write = *comnd;
  UIN [4] = *comnd;                   //
  Tx_write += 1;
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  }
  else                                // ���� �� � ��������� ������, �� ������� ��� ��� ����� �������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 64;                // �������� ������ "������� �� �������"
  add_status2_HART();
  }
  break;
  }

case 249:  // �������� ������� � ������ �������, ����������� ��������, ��. ��������� ��
  {
  if (SERVICE_MODE == 1)
  {
  if (DATA_NUMBER_HART == 13)         // ��������� ���-�� �������� ���� ������, � ���������� ������� ������ ���� 13 ���� ������
  {
  *Tx_write = 15;                     // ���������� ������������ ���� = 13 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� ��������

  if (*comnd == 0x0C || *comnd == 0xED)// ���� ���������� �������� �������� - � ��� ��� ���
  {
  write_data_PD (comnd);            // ��������� ������ � EEPROM
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  STATUS1_HART = 0x02;              // �������� ������ "�������� ��� ������ ���������"
  add_status2_HART();
  }
  temp_HART = 0;
  while (temp_HART < 13)              // ������������ �������� �������� � �������� �������
  {
  *Tx_write = *comnd;               //
  temp_HART += 1;
  comnd += 1; Tx_write += 1;        //
  }
  ptr_HART = (unsigned char *) & mastb; // ��� ��������� ������ �������� �������
  READ_4BYTE (ptr_HART, 0xF801);      // ����� ��������� ����� �������� � ���� ��� ������������
  ptr_HART = (unsigned char *) 0xF800;
  if (*ptr_HART == 0x0C) mastb = mastb * 1;// ������ ����������� ������������ (�������� ������� ��������� ��) ��� ������� ��������
  else mastb = mastb * 1000;          // ���� �������� ������� � ���, ����� ��������� ��������� �� 1000

  CALL_ptr = (unsigned char *) & znach_vpi;
  READ_4BYTE (CALL_ptr, 0xF801);        // ��������� �������� �������� ��������� ��������� ��������

  CALL_ptr = (unsigned char *) & znach_npi;
  READ_4BYTE (CALL_ptr, 0xF805);        // ��������� �������� �������� ��������� ��������� ��������

  diapazon_n_PD = znach_npi / znach_vpi;
  min_diapazon_PD = (1.0 - diapazon_n_PD)/50;
  }
  else                                // ���� ����� �������� ���� ������ �� ��������� - ������ ��������� �� ������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 0x05;              // �������� ������ "������������ ����� �������� ���� ���������� �����"
  add_status2_HART();
  }
  }
  else                                // ���� �� � ��������� ������, �� ������� ��� ��� ����� �������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 64;                // �������� ������ "������� �� �������"
  add_status2_HART();
  }
  break;
  }

case 250: // ������ ������������� ������������� � RAM
  {
  if (SERVICE_MODE == 1)
  {
  *Tx_write = 19;                     // ���������� ������������ ���� = 17 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ������ ������ �������������
  if (*comnd < 0x06)
  {
  ptr_HART = (unsigned char *) & ca + *comnd * 4 * 4;  // ��������� �� ������ ������������ ������������� ������
  comnd += 4;
  unsigned char cnt_rep = 4;
  while (cnt_rep)
    {
    temp_HART = 0;
    while (temp_HART < 4)           // ������������ �������� ��������
    {
    *ptr_HART = *comnd;           //
    temp_HART += 1;
    comnd -= 1; ptr_HART += 1;    //
    }
    cnt_rep -= 1;
    if (cnt_rep != 0) comnd += 8;
    }
  comnd -= 12;                      // ���������� ��������� �� ������ �������� ������ ��� ������ � �������� �������
  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  STATUS1_HART = 0x02;              // �������� ������ "�������� �����"
  add_status2_HART();
  }
  temp_HART = 0;
  while (temp_HART < 17)              // ������������ �������� �������� � �������� �������
  {
  *Tx_write = *comnd;               //
  temp_HART += 1;
  comnd += 1; Tx_write += 1;        //
  }
  }
  else                                // ���� �� � ��������� ������, �� ������� ��� ��� ����� �������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 64;                // �������� ������ "������� �� �������"
  add_status2_HART();
  }
  break;
  }

case 251: // ������ ������������� �������������.
  {
  if (SERVICE_MODE == 1)
  {
  *Tx_write = 19;                     // ���������� ������������ ���� = 17 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  comnd += 2;                         // ��������� �� �������� �������� ������ ������ �������������
  *Tx_write = *comnd;                 // ����� ����������� ������
  Tx_write += 1;
  if (*comnd < 0x06)
  {
  ptr_HART = (unsigned char *) & ca + *comnd * 4 * 4;      //
  write_to_tx_bufer ();

  ptr_HART = (unsigned char *) & ca + *comnd * 4 * 4 + 4;  //
  write_to_tx_bufer ();

  ptr_HART = (unsigned char *) & ca + *comnd * 4 * 4 + 8;  //
  write_to_tx_bufer ();

  ptr_HART = (unsigned char *) & ca + *comnd * 4 * 4 + 12; //
  write_to_tx_bufer ();

  STATUS1_HART = 0x00;              // �������� ������ "��"
  add_status2_HART();
  }
  else
  {
  STATUS1_HART = 0x02;              // �������� ������ "�������� �����"
  add_status2_HART();
  }
  }
  else                                // ���� �� � ��������� ������, �� ������� ��� ��� ����� �������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 64;                // �������� ������ "������� �� �������"
  add_status2_HART();
  }
  break;
  }

case 252: // ������ ������������� ������������� � EEPROM.
  {
  if (SERVICE_MODE == 1)
  {
  *Tx_write = 2;                      // ���������� ������������ ���� = 0 + 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  EEPROM_write_call ();               //
  //delta_P = 0;                        // �������� �������� ��������� ���� ��� ������ ����� ������������� ����������

  EEPROM_write_conf ();               // ����� ��� ����� �������� �������������� �� �������� ��������� �� ������ ��� ��������� ���� ��-�� �������� �������� delta_P
                    // ��������� �������� ��������� �������������� �� ��������
  k_pressure = 1;                     // ����������� ��������
  b_pressure = 0;                     // ����������� ��������
  EEPROM_write_pressure_cal();        // ��������� ����� ������������� ������������ � EEPROM

  STATUS1_HART = 0x00;                // �������� ������ "��"
  add_status2_HART();
  }
  else                                // ���� �� � ��������� ������, �� ������� ��� ��� ����� �������
  {
  *Tx_write = 2;                    // ���������� ������������ ���� = 2 ����� ������
  Tx_write += 3;                    // ��������� �� ������������ ������
  STATUS1_HART = 64;                // �������� ������ "������� �� �������"
  add_status2_HART();
  }
  break;
  }

// ���� ������� �� ���� ����������
default:
  {
  if (flag_HART_EXT)
  {
  comnd += 1;                       // ���� ������ �������� ����������� �������, �� � �������� ������� �������� ����� ����������� �������
  *Tx_write = *comnd;
  Tx_write += 1;
  }
  *Tx_write = 2;                      // ���������� ������������ ���� = 2 ����� ������
  Tx_write += 3;                      // ��������� �� ������������ ������
  STATUS1_HART = 64;                  // �������� ������ "������� �� �������"
  add_status2_HART();
  break;
  }
}
// ���������� ������
if (frame_HART)                       // ���� ���� � ���� �������� ������
  {
  ptr_HART = (unsigned char *) & Tx_bufer + PREAMBULE + 8 ;
  }
else                                  // ���� ���� � ���� ��������� ������
  {
  ptr_HART = (unsigned char *) & Tx_bufer + PREAMBULE + 4;
  }
if (flag_HART_EXT)  ptr_HART += 1;    // ��� ������ �� ����������� ������� - �������� ��������� ������� �� ���� ����

*ptr_HART = STATUS1_HART;             // ������
ptr_HART += 1;
*ptr_HART = STATUS2_HART;

CRC_Tx ();                            // ������� � ���������� ����������� �����
Tx_write += 1;

P3OUT &= ~0x02;                       // ����� ����������� �� �������� ������
TXBUF0 = *Tx_read;                    // �������� ��������
}

//************************************
//* �/� ���������� CRC ��� �������� **
//************************************
void CRC_Tx (void)
{
ptr_HART = (unsigned char *) & Tx_bufer;
ptr_HART += PREAMBULE;
unsigned char temp_CRC = 0;

while(ptr_HART != Tx_write)
  {
  temp_CRC ^= *ptr_HART;
  ptr_HART += 1;                      // ����� ��������� �� ��������� ������ ������
  }

*Tx_write = temp_CRC;                 // ���������� ���������� CRC � ����� �����
}


//==========================================================================================
//----�������� ������ ���� ������� � HART �����---------------------------------------------
//==========================================================================================
// HART Communication Fondation Document Number: HCF_SPEC-99 Page 43 of 75
void add_status2_HART (void)      
{
  STATUS2_HART = 0x00;    // ������� 2 ���� Field Device Status

  if((error_number == SENS_ERR) || (error_number == EEPROM_ERR) || (error_number == ADC_ERR)) // ���� ���� ��������� ������������� ������������� ��� Device Malfunction � More Status Available
  {
    STATUS2_HART |= 0x80;   // Device Malfunction
    STATUS2_HART |= 0x10;   // More Status Available
  }
  if(flag_conf_changed)   // ���� ���������� ����, ��� ������������ ���� ��������, �� ������������� ��� Configuration Changed
    STATUS2_HART |= 0x40;
  if(flag_cold_start)     // ���� ��� �������� ������ ��� ���������� ������� ������������� ��� Cold Start � ����� �������� ���������� ���
  {
    STATUS2_HART |= 0x20; // Cold Start
    flag_cold_start = 0;
  }
  if(!flag_i_out_auto)    // ���� ��������� � ������ ���� ���� ������������� ��� Loop Current Fixed
    STATUS2_HART |= 0x08;   // Loop Current Fixed
  if((flag_loop_current_saturated)  && (error_number != SENS_ERR) && (error_number != EEPROM_ERR) && (error_number != ADC_ERR)) // ���� ���������� ����� ����� � ���������, �� ��� ��������� �������������� ������������� ��� Loop Current Saturated
    STATUS2_HART |= 0x04; // Loop Current Saturated
  if((error_number == PRES_OVER_HI) || (error_number == PRES_OVER_LO)) // ���� �������� ���������� ����� �� ������� ��������� �� 10%, �� ������������� ���� Primary Variable Out Of Limits
    STATUS2_HART |= 0x01;   // Primary Variable Out Of Limits
}

