//Project : MSP430
//        : ������ �������� c ����������� HART (c)
//        : ��������� ����������� � ������� ����������. ���������������� �� 4-� ������
//        : � ������ ��� ������������ �������������, K=16.
//        : ���������� ������ ��� ������������� ���������.
//        : ������ ��� ����. ��������� ������ ��������� ������, ������ �� ������, �������� ����������� EEPROM ��� ���������
//        : ��������� ��� ������� HART �� ������. ��������� ���������� �� ��������
//        : ��������� ����� ������������� �� 20 ������.
//        : ��� ��������� ���� ����� ���� ��������� ������� �������� ��������.
//        : ��������� ������ ����������� ������ ��������, ��� ������� ��������� ������� Err-2.
//        : ���������� ������ ����� �������� ��� � ���
//        : ���������� �� ���� ����� ���� �������. �������� �������� ���������� �������� ������ �� HART ���������.
//        : ���������� �� �������� ����� ���� ������� � �� HART ���������.
//        : ��������� ������ ��� ������ ��� � ��� ��������� ��������. ������ ������������ �� ��������� ��� ����� ������ ��������� ��� � ��� ��������� ��������.
//        : ��������� ��������� ���������� �������� �������.
//        : ��������� �������� ���-�� �������� ���� ������ ��� ������, ��������� �������� ������.
//        : �������� ��������� �����, � ������� �������� ������������� ������� ������� ��� ��� ���������.
//        : ��������� ����������� ��� ��������� �������, ��� ���� ����� ������ ����� �������� ��������� �������� � �� ���������� �� ������������ �������� dP � ���� ������
//        : ���������� ������ ��� ������� ���� ��� ���������������� ����������� ��������� �������.
//        : ���������� ������ ������������ ������ Commnd ��� ������� ���-�� �������� ���� ������.
//        : ��������� ���������� ���������� ��� ���������� ���� ��� ���������� ������� ������ (��� ���������� �������).
//        : ��������� ������� �������������� ��������� �������� �������.
//        : �������� ����� �������� ���������� �� �������� ��� ���������� ������������� �������������
//        : ��������� ����������� ��������� ������������� �������� ��� ��� ��� (02-07-2014)
//        : �������� ������������ ���������� ������� ��� ���������������� �����������.(16.09.2014)
//        : ���������� ������ � �������������� �����, ��������� � ����������.
//        : �������� �������� ������������� �� 10,5 ���.
//        : �������� ��� ��. �������� � ���� ����� ��� ��������� ���� ������� ����� �������� ������������ �������� (31.07.2015)
//        : ������� ���� �������� ������������ ��������: ������ 100.00 ��� ��� ����� � ���� ��������� 000.00 (12.10.2016)
//        : �������� �������� ������������� �� 50,5 ��� (20.08.2018)
//        : �������������� ����� ��� �������� �������� ������ ����������, �������� ����������� ����� (20.08.2018)
//Version : 9.4
//Date    : 31-07-2015. (DD-MM-YY)
//Author  : ����� ������� ������������ (UB9AJN)
//Company : ���"�������-��������"
//Comments: IAR Workbench target descriptor for MSP430 version 4.20.1
//��������: 20-08-2018
//
//
//Chip type          : MSP430F149
//Clock frequency    : 3.6864 MHz External Quartz
//Internal SRAM size : 0
//External SRAM size : 0

//************************************
//****** ���������� � ��������� ******
//************************************
#define _EDIZMER_					  // ����������� �� ����� ����

#define CPU_FREQ  3686400             // 3,6864 ��� ������� ������
#define U_REF     1.17                // ������� ���������� ��� ���

#include <msp430.h>                   // ��������� � ��������� ����������
#include <math.h>

#define Addr_EEPROM_conf 0x1000       // ����� ������ ������/������ �������� �������
#define Addr_cur_EEPROM_conf 0xEC00   // ����� ������ ������/������ ������� ������������ �������

#define DIN_ADC   3                   //
#define DOUT_ADC  4                   //
#define SCLK_ADC  2                   // ������ ��� ���
#define PORT_ADC  P1OUT               //

#define latch_dac 5                   //
#define clock_dac 4                   // ������ ���
#define data_dac  3                   //
#define PORT_DAC  P2OUT               //

#define menu_key   h7                 // ������ � ������� ���������� ����������
#define nul_key    d7                 //
#define plus_key   c7                 //
#define minus_key  e7                 //

#define a7    1                       // ��� ��������������� ����������
#define b7    3                       //
#define c7    5                       //
#define d7    0                       //
#define e7    2                       //
#define f7    4                       //
#define g7    6                       //
#define h7    7                       //

//----����/������ ������--------------------------------------------------------------------
#define ALL_OK      0                                   // ��� ��
#define PRES_OVER_HI  1                                 // ���������� �������� ���� 1.1 ��
#define PRES_OVER_LO  2                                 // ���������� �������� ���� 0.01 ��, ��� ��� ���������� ���������� ������ 1.01 ��(-)
#define SET_ZERO_ERR  3                                 // ��� ��������� ���� �� ���������� ��������� �������� �������� ���� ������� �� ���������� �������
#define ADC_ERR     5                                   // ������ ���
#define EEPROM_ERR    6                                 // ������ ����������� ����� ������
#define SENS_ERR    9                                   // ������ �������
#define LCD_OVR     7                                   // ������������ ����������

//----������ ��������� ������������ (Namur)-------------------------------------------------
#define CRIT_ERR_LOW			3.6 														// ������������� ������� ������ ��� ������ ���������� ������� (����������� ������)
#define CURRENT_OUT_ALARM_LOW	3.8 														// ������������� ������� ������ - ���� ����������� ��������
#define CURRENT_OUT_ALARM_HIGH	20.5 														// ������������� ������� ������ - ���� ����������� ��������
#define CRIT_ERR_HIGH			22.5						                                // ������������� ������� ������ ��� ������ ���������� ������� (����������� ������)

//************************************
//******** ���������� ������� ********
//************************************
void u_print (char poddiapazon);      // ����� �� ��������� �������� ����������
void term_print (void);               // ����� �� ��������� �������� ���������� �����������

void I_ADC_write (unsigned char byteword);// ������ ������ � ��� ���������� ����
unsigned long I_ADC_read (void);      // ������ ������ �� ��� ���������� ����

unsigned char reset_DS18B20 (void);   //|
void write_DS18B20 (char byte);       //| ��� ������ � �������� �����������
unsigned char read_DS18B20 (void);    //|
void delay_DS (char num);             //|

void EEPROM_write_conf(void);
void EEPROM_write_call(void);
unsigned char EEPROM_read(unsigned int adress);
void READ_4BYTE (unsigned char *ptr2, unsigned int addr);
void write_EEPROM_TEG (unsigned char *prt);
void write_EEPROM_MESSAGE (unsigned char *prt);
void write_data_PD (unsigned char *prt);
void write_EEPROM_SERIAL_PD (unsigned char *prt);
void write_EEPROM_ASSEMBLY_NUMBER (unsigned char *prt);
void write_EEPROM_ZAVOD_NUMBER_DD (unsigned char *prt);
void EEPROM_write_pressure_cal (void);

void write_current_conf(void);                                // �/� ������ � EEPROM ������� ������������ �������
void read_store_conf (void);                                  // ������ �������� ����������� ������������ ������������� ������� �� EEPROM

void CALL_read (void);
void read_conf (void);
char convert (char c);
void work_menu(void);
void znach_print (void);
void save (unsigned char _num);
void Nul_menu (void);
void Nul_magnit_menu (void);
void davlenie_vivod_na_indicator (void);
void R_vivod_na_indicator (void);
void OUT_current (void);
float okruglenie (float abc, unsigned char num);
void curent_print (void);
float perevod_v_davlenie (float znach);
float perevod_v_uslovnie_edinici (float znach, char edinica);
char proverka_delta_P (float delt_P, unsigned char par);     // �/� ��� �������� ������������ ��������� ����
void CRC16 (unsigned char *nach, unsigned char dlina);

char raschet_n (void);
void preobrazovanie_v_chelochislennoe (float znach, char m);
float preobrazovanie_v_vehestvenoe (void);
void critical_error (void);

unsigned char Term_time=0, Term_flag=1;
unsigned char *CALL_ptr, addr_CALL=0;
unsigned char *Flash_ptr;
unsigned char count_num_term=0;
unsigned char n, count_time;
unsigned char i;
float temperatura=0, u=0, p=0, i_out=0, procent_diappazona = 0, f;
unsigned long bb, r;
unsigned long b_ADC_mosta, r_ADC_mosta;// � ��� �������� �������� ����� ��� ������ ��� �������� �� �� HART ���������
unsigned char menu_temp;// send_flag=0;
unsigned char tmp_ID;
unsigned char IND_flag = 1, IND = 0, IND_time = 0;

unsigned char cnt_key_ =0, key_a =0xFF, key_b =0xFF;
unsigned char key = 0, KEY_PUSH_FLAG = 0;

unsigned char edinica_izmer=0x0C, tmp_edinica_izmer;
unsigned char koren=0x00, tmp_koren;
unsigned char Mm=0, Vm=0, menu_count = 0, menu_n, num_menu_ch, cancel_edit = 0;
unsigned char IND_DIMM =0x80, IND_CURSOR = 0;
unsigned char flag_input_menu;
unsigned char flag_i_out_auto = 1,tmp_flag_i_out_auto;
unsigned char tok_signal_inv=0, tmp_tok_signal_inv;
unsigned char magnit_key_enable=1, tmp_magnit_key_enable;
unsigned char tmp_current_alarm, current_alarm = 0;

unsigned char r1, r2, r3, r4, r5;     // �������� ����� ��� ����� �������� ��� � ��� ����� ����
unsigned char mp;                     // ��� ������� ������ �������� ��� � ���
unsigned char cursor_position;        // ��������� ������� ������� ��� �������������� ��� � ���
unsigned char r5_tmp;

unsigned char Nul_magnit_corr = 0;
unsigned char KEY_magnit_PUSH_FLAG = 0;
unsigned char flag_input_menu_magnit = 0;
unsigned char crc_H, crc_L;
unsigned char HART_disable = 0, tmp_HART_disable;
unsigned char flag_error = 0, error_number = 0;
unsigned char flag_error_menu = 0, error_number_menu = 0;
unsigned char flag_message_menu = 0;

unsigned int  b_current = 0x4000;     // ������������ ��� ���������� ���� ��� ���������
float k_current = 0.00024414;         // �������� ��������� ���� �������� �� ���������

float tok_low_input, tok_hi_input;    // ��� ������� ������������� ���������� ��� ����� ����
float pressure_low_input, pressure_hi_input; // ��� ������� ������������� ���������� �� �������� ����� ����
unsigned char Nul_corr = 0;
unsigned char flag_ADC, flag_ADC_IDLE=0;
unsigned char HELLO_flag = 1;         // ���� ������ ��������������� ���������, ���. ��� ������ ������� �� ������

unsigned char ind_razr_1 = 0x00;      // �������� �����. ��� �������� ������ ������� ����� �������� �� ���������
unsigned char ind_razr_2 = 0x00;      //
unsigned char ind_razr_3 = 0x00;
unsigned char ind_razr_4 = 0x00;
unsigned char ind_razr_5 = 0x00;
unsigned char ind_razr_6 = 0x00;
unsigned char ind_cnt = 1;

unsigned char N_integr, tmp_N_integr;
unsigned char N_integr_tmp=1;
unsigned long Skolzyashie_okno[255];  // 128
unsigned long *Rx_skolz_okno, *Tx_skolz_okno;

unsigned char flag_R = 1;
unsigned char N_integr_R = 8;
unsigned long Skolzyashie_okno_R [8];
unsigned long *Rx_skolz_okno_R, *Tx_skolz_okno_R;

float diapazon_v = 1.000;             // ������� �����������
float diapazon_n = 0.000;             // ������ �����������
float tmp_diapazon_v, tmp_diapazon_n;
float min_diapazon_PD, diapazon_n_PD; // ������������ ��� ������������� �������� ���������

float tmp_damping_value;

float delta_P = 0;                    // �������� �������� ��� ���������� 0
float davlenie;
float k_pressure = 1, b_pressure = 0; // ��������� �������� ��������� �������������� �� ��������
float znach_vpi, znach_npi;           // ������������ ��� ���������� �� ��������
float atmosphera;                     // �������� ������������ �������� ��������� ��� ��������� ����

float ca [24];                        // ������ � �������������� ����������

float a0,a1,a2,a3,a4,a5;              //
float temp_u, R_mosta;

float loop_current, mastb;

unsigned char modelPD [] = {0,'2','1','5','0','M'};                     // ��� ������� �������� 0x00 � ��, 0x01 � ��, 0x02 � ���, 0x03 � ��, 0x04 ���; ������ �������

// ���������� ��� Field Status Device (2 ���� ������� � �������� ������� HART)
unsigned char flag_loop_current_saturated = 0;												// ���� 1 - ������� ����� � ���������
unsigned char flag_cold_start = 1;															// ���� 1 - �������� �������� �����/���������� �������
unsigned char flag_conf_changed = 0;														// ���� 1 - ������������ ���� ��������

#include "USART.c"                    // ������ USART

//************************************
//******** �������� ��������� ********
//************************************
void main(void)
{
WDTCTL = WDTPW + WDTHOLD;             // ��������� watchdog
//WDTCTL = WDTPW + WDTTMSEL + WDTCNTCL; // ������������ �� ACLK, ������� �������� WDT
//IE1 |= WDTIE;                         // Enable WDT interrupt


P1OUT = 0x00;                         // ���.0 �� ������� �����
P1DIR = 0xED;                         // ���� P1: P1.4, P1.1 - �� ����, ��������� �� �����.

P2OUT = 0x00;                         // ���.0 �� ������� �����
P2DIR = 0x3D;                         // ���� P2: P2.1, P2.6-P2.7 �� ����, ��������� �� �����

P2SEL = 0x00;                         // ��� ������ ����� - ��� �����-������
P2IE = 0x02;                          // ���������� �� ��������� ������ �� ����� P2.1
P2IES = 0x02;                         // ��������� ������ ������������ ���������� �� �������� �� 1 � 0.

P3OUT = 0x02;                         // ���.0 �� ������� ����� 0.7 (RTS � ���.1 � ������)
P3DIR = 0x9B;                         // ���� P3: P3.2,5,6 - �� �����, ��������� �� ����.

P4OUT = 0x00;                         // ���.0 �� ������� �����
P4DIR = 0xFF;                         // ���� P4: ��� �� �����.

P5OUT = 0x00;                         // ���.0 �� ������� ����� 6.7
P5DIR = 0xFF;                         // ���� P5: P5.0-P5.5 �� �����.

P6OUT = 0x00;                         // ���.0 �� ������� �����
P6DIR = 0xFF;                         // ���� P6: ��� �� �����.

//SVSCTL = VLD3 | VLD1 | PORON;         // �������� ����������� ����������, ������������� ����� 3,05�. ���� ���������� ������� �� ��������� ����� ������ - ������ RESET (��� ������ ��� MSP430F157)

// ��������� ����������

BCSCTL1 |= XTS + DIVA0 + DIVA1;       // ACLK = LFXT1/8 = HF XTAL/8 �������� ������� ��������� �� ������. �������� ��� ACLK = 8

  do
  {
  IFG1 &= ~OFIFG;                     // ������� ���� OSCFault
  for (i = 0xFF; i > 0; i--);         // � ������ �� ��������
  }
  while ((IFG1 & OFIFG));             // ���� ����� ��������� �� ������ ������ ��������� �������� ���� ������������ �� ����, ��� ������� ���������� ����� ��������.

BCSCTL2 |= SELM_3 + DIVM1;            // �������� ������������ �� XT1. ������������ ��� MCLK = 4, SMCLK/4

/*
DCOCTL = MOD0 + MOD1 + MOD2+ MOD3 + MOD4; // DCO = 0, �������� ���������� 32
BCSCTL1 |= RSEL2 + RSEL0;             // RSEL = 5
//BCSCTL2 &=~ DCOR;                     // ���������� Rosc
//BCSCTL2 &=~(SELM0 + SELM1 + DIVM0 + DIVM1);// MCLK ����������� �� DCOCLK, �������� 1:1
BCSCTL2 = 0x00;
*/

// ����������� ������-������� A0
TACCTL0 = CCIE;                       // ���������� �� ���������� �������� TACCR0 � TAR (���������� ������ 2 �� ��� ������������ ���������)
TACCR0 = 2000;                        // ��������, �� �������� ������� ������-�������, � ����� �������� ������ � 0
TACTL = TASSEL_2 + MC_1 + ID_0;       // �������� ������������ ��� ������� -SMCLK, ����� ����� - �����, ����������� ������������ - 1;

// ����������� ������-������� B0
TBCCTL0 = CCIE;                       // ���������� �� ���������� �������� TBCCR0 � TBR
TBCCR0 = 4151;//9009;                 // ��������, �� �������� ������� ������-�������, � ����� �������� ������ � 0; ���������� ������ ~ 9��
TBCTL = TBSSEL_1 + MC_1 + ID_0;       // �������� ������������ ��� ������� -ACLK, ����� ����� - �����, ����������� ������������ - 1;

// ����������� ������������ ����������� Flash
FCTL2 = FWKEY + FSSEL0 + FN3;         // MCLK/8 ��� Flash Timing Generator

USART_Init ();                        // ������������� ����������������� �����
P3OUT |= 0x08;                        // ��������� ������ ������

// ������������� AD7793
I_ADC_write (0xFF);                   //|
I_ADC_write (0xFF);                   //|
I_ADC_write (0xFF);                   //| ������ ���������� ����� ���
I_ADC_write (0xFF);                   //|

I_ADC_write (0x08);                   // ����. ������ � Mode register (b'00001000')
I_ADC_write (0x00);                   // Continuous Conversion Mode (Default)
I_ADC_write (0x09);                   // Internal 64 kHz Clock. Internal clock is not available at the CLK pin.(b'00001101')
                                      // ����������� ������ �� 50��, (���������� 80 ��, 120ms)

I_ADC_write (0x10);                   // ����. ������ � Configuration register (b'00010000')
I_ADC_write (0x04);                   // Bias Voltage Generator Disable, burnout currents are disabled. ���������� �����
                                      // ������������� ����. �������� �=16,
I_ADC_write (0x10);                   // �������� �������� �������� ���������� (b'00010000')
                                      // �������������� �����, ����������� ��� �� ��������� ���������� AIN1(+) � AIN1(�)

I_ADC_write (0x28);                   // ����. ������ � IO register (b'00101000')
I_ADC_write (0x00);                   // ���������� �������� ���� ��������

flag_ADC = 0x00;                      // ������������� ���� ��������� ���������� �� ������ ����������
// END init

Tx_skolz_okno = Rx_skolz_okno = (unsigned long *)& Skolzyashie_okno; // ��������� ����������� ���� - �� ������ ������ (���������� ���������)
Tx_skolz_okno_R = Rx_skolz_okno_R = (unsigned long *)& Skolzyashie_okno_R; // ��������� ����������� ���� - �� ������ ������ (���������� ���������)

_EINT();                              // ���������� ���������� ����������

CALL_ptr = (unsigned char *) Addr_EEPROM_conf;
CRC16 (CALL_ptr, 126);                // ����������� �������� CRC ��� ����� ������ � ����������� �������
CALL_ptr = (unsigned char *) Addr_EEPROM_conf + 126;

if (crc_H == *CALL_ptr)               // ��� ��������� ������ �������� CRC ������ � ����������� �������
  {
  CALL_ptr += 1;
  if (crc_L == *CALL_ptr)
    {
    read_conf ();                     // ���� ����������� ����� ��������� - ������ �������� ������� �� EEPROM
    }
  else                                // ���� ����������� ����� EEPROM �� ��������� - �� ������� �� ��������� ��������� �� ������
    {                                 // Err-6
    flag_error = 1;
    error_number = EEPROM_ERR;
    critical_error ();
    }
  }
else
  {
  flag_error = 1;
  error_number = EEPROM_ERR;
  critical_error ();
  }

//************************************

CALL_ptr = (unsigned char *) 0xFC00 + 8;  // �������� ���������� ������ ���������� �� �������
if (*CALL_ptr != 0x00)                // ���� ��� ������ ��������� ����� �������� - ���������� ������������ �������� �� ��������� (k=1, b=0)
  {
  EEPROM_write_pressure_cal ();       //
  }
else                                  // ���� ������ ��� ���� ���������, ��������� �������� ������������� ������������� �������������� �� ��������
  {
  CALL_ptr = (unsigned char *) & k_pressure;
  READ_4BYTE (CALL_ptr, 0xFC00);      //

  CALL_ptr = (unsigned char *) & b_pressure;
  READ_4BYTE (CALL_ptr, 0xFC04);      //
  }
//************************************

CALL_read ();                         // ������ ������������� �������������

CALL_ptr = (unsigned char *) & mastb;
READ_4BYTE (CALL_ptr, 0xF801);        //

CALL_ptr = (unsigned char *) 0xF800;
if (*CALL_ptr == 0x0C) mastb = mastb * 1;// ������ ����������� ������������ (�������� ������� ��������� ��) ��� ������� ��������
else mastb = mastb * 1000;            // ���� �������� ������� � ���, ����� ��������� ��������� �� 1000

CALL_ptr = (unsigned char *) & znach_vpi;
READ_4BYTE (CALL_ptr, 0xF801);        // ��������� �������� �������� ��������� ��������� ��������

CALL_ptr = (unsigned char *) & znach_npi;
READ_4BYTE (CALL_ptr, 0xF805);        // ��������� �������� �������� ��������� ��������� ��������

diapazon_n_PD = znach_npi / znach_vpi;
min_diapazon_PD = (1.0 - diapazon_n_PD)/50.5;

N_integr = (unsigned char) (damping_value / 0.0786) + 1; // ������������ �������� ���-�� �������� ��� ��������������

if (DEV_ID != 0x00)                   // ���� ����� ���������� �� ������� - ������ ������� ����� ����������
  {
  flag_i_out_auto = 0;                // ������ � ����� �������������� ����
  loop_current = 4.000;
  OUT_current ();                     // ������������� �������� ��������� ���� ������ 4 ��
  }

//WDT_ARST_1000;                      // �������� � ����������� ���������� ������ 1���;

for (;;)
  {
//WDTCTL = WDTPW + WDTCNTCL;            // ����� ����������� �������

if (CDR_END_HART == 1) parser_USART();// ���� ���������� ���� ������ ������ �����, ���� ��������� �������� �������.

//------ ������ � ����
  if (Mm != 0) work_menu();           // ������ � �������� ����
  if (Nul_corr != 0) Nul_menu();      // ������ � ���� ��������� ���� (������� ������)
  if (Nul_magnit_corr != 0) Nul_magnit_menu(); // ������ � ���� ��������� ���� � ������� ��������� ������

  if ((~key) & ((1<<menu_key)|(1<<nul_key)|(1<<minus_key)|(1<<plus_key))) // ���� ������ ����� �������
    {
    if ((!(key & (1<<menu_key))) && Mm == 0 && flag_input_menu == 0 && Nul_corr == 0) // ���� ������ ������ ����� � ���� � ����� � ���� ������
      {
      flag_input_menu = 1;            // ������������� ���� ������������� ����� � ���� ��� ������� ������� ����� � ����
      menu_count = 0;                 // ���������� ������� �������
      }
    if ((!(key & (1<<menu_key))) && Mm == 0 && flag_input_menu == 1 && menu_count > 5 && Nul_corr == 0) work_menu(); // ������ ������ ������ 0,6 ��� - �� ��������� � ������ � ����

    if ((!(key & (1<<nul_key)))  && Mm == 0 && flag_input_menu == 0 && Nul_corr == 0) // ���� ������ ������ ��������� ���� � ����� � ����� ��������� ���� ������
      {
      flag_input_menu = 1;            // ������������� ���� ������������� ����� � ���� ��������� ���� ��� ������� ������� ��������� ������
      menu_count = 0;                 // ���������� ������� �������
      }
    if ((!(key & (1<<nul_key))) && Mm == 0 && flag_input_menu == 1 && menu_count > 20 && Nul_corr == 0) Nul_menu(); // ������ ������ ������ 2 ��� - �� ��������� � ���������� 0
    }
  else
    {
    if (!(flag_error_menu)) KEY_PUSH_FLAG = 0;  // ���������� ���� ������� ������ ���� �� ��������� �� ��������� ��������� �� ������
    flag_input_menu = 0;              // ���������� ���� ��� �������� ����� � ���� ��������
    }

  if (!(P3IN & 0x40))                 // ���� ������ ��������� ������ ��������� ����
    {
    if (Mm == 0 && flag_input_menu_magnit == 0 && Nul_corr == 0) // ���� ������ ������ ����� � ���� � ����� � ���� ������
      {
      flag_input_menu_magnit = 1;     // ������������� ���� ������������� ����� � ���� ��� ������� ������� ����� � ����
      menu_count = 0;                 // ���������� ������� �������
      }
    if (Mm == 0 && flag_input_menu_magnit == 1 && menu_count > 30 && Nul_corr == 0 && Nul_magnit_corr == 0 && KEY_magnit_PUSH_FLAG == 0) Nul_magnit_menu(); // ����� 3 ��� ����� ������� � ��������� ��������� ������ - ������ � ����� ��������� ����
    }
  else
    {
    KEY_magnit_PUSH_FLAG = 0;         // ���������� ���� ������� ������
    flag_input_menu_magnit = 0;       // ���������� ���� ��� �������� ����� � ���� ��������
    }

//-----

// ����� �� ��������� ����������� ��������
if (Mm == 0 && IND_flag && IND == 0 && HELLO_flag == 0)
{
if (flag_error)                       // ���� ���� ��������� �� ������� � EEPROM
  {
  ind_razr_1 = ((1<<a7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7));           // E
  ind_razr_2 = ((1<<e7)|(1<<g7));                                   // r
  ind_razr_3 = ((1<<e7)|(1<<g7));                                   // r
  ind_razr_4 = (1<<g7);                                             // -
  ind_razr_5 = convert (error_number);                              // ����� ������
  ind_razr_6 = 0x00;
  IND_DIMM &=~0x80;
  }
else                                  //
  {
  f = davlenie;                       // ��� ������ �������� �������� �� ���������
  if (Nul_corr == 0 && Nul_magnit_corr == 0) IND_DIMM |= 0x80;// ������� ����������
  davlenie_vivod_na_indicator ();

  if (edinica_izmer == 0xED) ind_razr_6 = (1<<b7);  // ��������� ������ ��������� ���
  else ind_razr_6 &= ~(1<<b7);        //

  if (edinica_izmer == 0x0C) ind_razr_6 = (1<<g7);  // ��������� ������ ��������� ���
  else ind_razr_6 &= ~(1<<g7);        //

  if (koren) ind_razr_6 |= (1<<a7);   // ��������� ��������������� �����������
  else ind_razr_6 &=~(1<<a7);         //
  }
/*  
  if ((p <= diapazon_v * 1.1) && (p >= diapazon_n - 0.1))   // �������� �� ���������� ��������� �������� +-10% �� �������������� ���������
    {
    if (flag_error)                   // ��� ������� ������ ������ ����
      {
      if (error_number == 1 || error_number == 2 || error_number == 9) // ���� ���� ������ ���������� �������� ��� ������ ������� -
        {
        if (DEV_ID == 0)                          // � ���������� ������� ������� ���� ������ ����� ������� �����.
          {
          flag_i_out_auto = 1;
          }
        flag_error = 0;                           // ������� ���� ������� ������ � ��������� ����� ����������� �������� ��������
        IND_DIMM |= 0x80;                         // ��������� ������ �����������
        }
      }
    }
  else    // ���� �������� ������� �� ������������� �������
    {
    if (error_number != 6)            //
    {
    flag_error = 1;
    if (b_ADC_mosta < 0x0020 || b_ADC_mosta > 0xFFFFE0 || R_mosta < 0x0020 || R_mosta > 0xFFFFE0 )  // ���� ��� ��� ����� ������� �������� - ������������� �������
      {
      error_number = 9;
      critical_error ();
      }
    else // ���� ������ ��������, �� ���������� �� ����� ������� ����� ���������� �������� � ������� �������������� ����� ������
      {
      if   (p >= diapazon_v * 1.1)    error_number = 1; //
      else                            error_number = 2; //
      }
    }
    else                              // ��� ������ �6 ������������ ��������� �/� ������ ���������� ����
      {
       critical_error ();             // ��� ���� ����� ��� ����� �������� � ���� ��� �� �������� �������� ���������� ���� (Hi / Lo)
      }
    }
*/
  float down_limit = diapazon_n; 
  if(diapazon_n == 0)			// ���� ������ �������� = 0, �� ������ ���� ��� ��� >10% �� ���
  	down_limit = -0.1 * diapazon_v;
  else if (diapazon_n > 0) 	// ���� ������ �������� > 0, �� ������ ���� ��� ��� >10% �����  �� ���
  	down_limit = down_limit * 0.9;
  else if (diapazon_n < 0)	// ���� ������ �������� < 0, �� ������ ���� ��� ��� >10% ������ �� ���
  	down_limit = down_limit * 1.1;

  float up_limit = diapazon_v;
  if (up_limit == 0)			// ���� ������� �������� = 0, �� ������ ��� �������� > 10% �� |���|
  	up_limit = 0.1 * -diapazon_n;
  else if (up_limit < 0)		// ���� ������� �������� < 0, �� ������ ��� �������� > 10% ������ �� ���
  	up_limit = 0.9 * up_limit;
  else if (up_limit > 0)
  	up_limit = up_limit * 1.1; // ���� ������� �������� > 0, �� ������ ��� �������� > 10% ������ �� ���

  if ((p <= up_limit) && (p >= down_limit))                  							    // �������� �� ���������� ��������� �������� +-10% �� �������������� ���������, �� ���� ���� ��� ������ � ����������, ��
  {
    if(flag_error == 1)                                                                 // ���� �� ��� ��� ��������� ���� ������
    {
      if (error_number == PRES_OVER_HI || error_number == PRES_OVER_LO || error_number == SENS_ERR)                      // ���� ���� ������ ���������� �������� ��� ������ ������� -
      {
        if (DEV_ID == 0)                                                            // � ���������� ������� ������� ���� ������ ����� ������� �����.
        {
          flag_i_out_auto = 1;
        }
        flag_error = 0;                                                             // ������� ���� ������� ������ � ���������� �������� ������
        if(error_number != ALL_OK)                                                    // ��� ���� ���� ������ ��� �� ���� ��������, �� ����������, ���� ��� ��������, �� ����������
        {                                                                           // ������� ������ ��� ����, ����� �� ���� ���������� �������� ���������� (��� ���������� Set_error ��������� ������ �� ���������)    
          error_number = ALL_OK;
        }
      }
    }
  }
  else                                                                                    // �������� ������� �� ���������� �������
  {
    if ((error_number != EEPROM_ERR) && (error_number != PRES_OVER_HI) && (error_number != PRES_OVER_LO) && (error_number != SENS_ERR)) // ���� �� ���������� ����� ���� ������ ��������� � ���������� � �������� + ������ EEPROM ������� �������
    {
      flag_error = 1;                                                                 // ���������� ����, ��� ���� ����� �� ������ � ���������� � ���� ����������� � ����� ������� � ���� ��� ����� ���
      if (p >= diapazon_v * 1.1)    
      {
        if(error_number != PRES_OVER_HI)                                            // ���� ��� ������ ���� ��� ��� �� ����������, �� �������������
        {
          error_number = PRES_OVER_HI; 
        }
      } 
      else                            
      {
        if(error_number != PRES_OVER_LO)                                            // ���� ��� ������ ���� ��� �� ����������, �� �������������
        {
          error_number = PRES_OVER_LO; 
        }   
      }
    }

    if ((error_number != EEPROM_ERR) && (error_number != SENS_ERR))             // ��������� ����������������� ������������
    {
      if (b_ADC_mosta < 0x0020 || b_ADC_mosta > 0xFFFFE0 || R_mosta < 0x0020 || R_mosta > 0xFFFFE0 )  // ���� ��� ��� ����� ������� �������� - ������������� �������
      {
        if(error_number != SENS_ERR)                                                // ���� ��� ������ - ������������� ������� ��� �� ���������, �� ����������
        {
          error_number = SENS_ERR;                                                // ��� ������ - ������������� �������
          critical_error ();
        }
      }
    }

    if (error_number == EEPROM_ERR)                                                       // ��� ������ EEPROM ������������ ��������� �/� ������ ���������� ����
    {
      critical_error ();                                                              // ��� ���� ����� ��� ����� �������� � ���� ��� �� �������� �������� ���������� ���� (Hi / Lo)
    }
  }


IND_flag = 0;
}
else
{
if (HELLO_flag == 1 && Mm == 0)       // ���� ���������� ���� ������ ��������������� ���������
  {
  	/*
  ind_razr_1 = ((1<<b7)|(1<<c7)|(1<<e7)|(1<<f7)|(1<<g7));         // H
  ind_razr_2 = ((1<<a7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7));         // E
  ind_razr_3 = ((1<<d7)|(1<<e7)|(1<<f7));                         // L
  ind_razr_4 = ((1<<d7)|(1<<e7)|(1<<f7));                         // L
  ind_razr_5 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)); // O
  ind_razr_6 = ((1<<a7)|(1<<b7)|(1<<g7));
	*/
  	ind_razr_1 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7)|(1<<h7)); // ��������� ��� �������� �� ����������
	ind_razr_2 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7)|(1<<h7));
	ind_razr_3 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7)|(1<<h7));
	ind_razr_4 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7)|(1<<h7));
	ind_razr_5 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7)|(1<<h7));
	ind_razr_6 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7)|(1<<h7));
  }
}

if (!(P1IN & (1<<DOUT_ADC)))          // ���� ��� �������� ��������������
  {
  I_ADC_write (0x58);                 // ��������� �������� - ������ 24-� ���������� �������� ������

  if (flag_ADC_IDLE)                  // ���� ���������� ���� ��������� ��������� - �� ���������� �������� ������ ������������.
    {
    I_ADC_read();                     // ��� ���������� ��� ������������ �������� ������ ��� ������������ ������� ���������
    flag_ADC_IDLE = 0;                //
    }
  else
  {
  if (flag_ADC == 0x01)               // ���� ���������� ���� ��������� ����������� ����������
    {
    *Tx_skolz_okno_R = I_ADC_read();  // ������ ��������
      Tx_skolz_okno_R += 1;
      if (Tx_skolz_okno_R > (Skolzyashie_okno_R + N_integr_R -1)) // ���� �������� � ���������� ���� N_integr ��������
        {
        Tx_skolz_okno_R = (unsigned long *)& Skolzyashie_okno_R; // ��������� ������ ����������� ���� - �� ������ ������
        flag_R = 0;
        }

      i = N_integr_R;
      r = 0;

    if (flag_R == 0)
    {
    while (i)
      {
      r = r + *Rx_skolz_okno_R;       // ��������� �������
      Rx_skolz_okno_R += 1;           //
      i = i - 1;
      }
    r = r / N_integr_R;               // ����� ��������� �� ���-�� �������
    Rx_skolz_okno_R = (unsigned long *)& Skolzyashie_okno_R; // ��������� ������ ����������� ���� - �� ������ ������

    r_ADC_mosta = r;                  // ��������� ��� ��� ��� �������� �� HART ���������
    R_mosta = ((7500.0/0x7FFFFF) * (r-0x800000) * 3.8666)/4;

    R_mosta = okruglenie (R_mosta, 1);

    flag_ADC = 0x02;                  // ����������� �� ��������� ����������� � ������� ����������� ������� ����������� ���
    I_ADC_write (0x10);               // ����. ������ � Configuration register (b'00010000')
    I_ADC_write (0x00);               // Bias Voltage Generator Disable, burnout currents are disabled. ���������� �����
                                      // ������������� ����. �������� �=1,
    I_ADC_write (0x96);               // ���������� �������� �������� ���������� (b'10010110')
                                      // �������������� �����, ����������� ��� �� ��������� ����������� � ������� ����������� �������
    flag_ADC_IDLE = 1;                // ��������� ��������� ��� - ����������.

    f = R_mosta * R_mosta;            // ������������� ������������ ��� ������ ������������� ���������� ��� ����� �����������

    a0 = ca[0]  + ca[1]  * R_mosta + ca[2]  * f + ca[3]  * f * R_mosta;
    a1 = ca[4]  + ca[5]  * R_mosta + ca[6]  * f + ca[7]  * f * R_mosta;
    a2 = ca[8]  + ca[9]  * R_mosta + ca[10] * f + ca[11] * f * R_mosta;
    a3 = ca[12] + ca[13] * R_mosta + ca[14] * f + ca[15] * f * R_mosta;
    a4 = ca[16] + ca[17] * R_mosta + ca[18] * f + ca[19] * f * R_mosta;
    a5 = ca[20] + ca[21] * R_mosta + ca[22] * f + ca[23] * f * R_mosta;
    }
    }
  else
    {
    if (flag_ADC == 0x02)
      {
      r = I_ADC_read();               // ������ ��������
      temperatura = (((1.17/0x800000) * (r-0x800000)) / 0.00081) - 273; // �������� � �������� �������
      temperatura = okruglenie (temperatura, 1);
      flag_ADC = 0x00;
      I_ADC_write (0x10);             // ����. ������ � Configuration register (b'00010000')
      I_ADC_write (0x04);             // Bias Voltage Generator Disable, burnout currents are disabled. ���������� �����
                                      // ������������� ����. �������� �=16,
      I_ADC_write (0x10);             // �������� �������� �������� ���������� (b'00010000')
                                      // �������������� �����, ����������� ��� �� ��������� ���������� AIN1(+) � AIN1(�)
      flag_ADC_IDLE = 1;              // ��������� ��������� ��� - ����������.
      }
    else
      {
    *Tx_skolz_okno = I_ADC_read();    // ������ ��������
    Tx_skolz_okno += 1;
    if (Tx_skolz_okno > (Skolzyashie_okno + N_integr -1)) // ���� �������� � ���������� ���� N_integr ��������
      {
      Tx_skolz_okno = (unsigned long *)& Skolzyashie_okno; // ��������� ������ ����������� ���� - �� ������ ������
      }

    i = N_integr_tmp;
    bb = 0;

    while (i)
      {
      bb = bb + *Rx_skolz_okno;       // ��������� �������
      Rx_skolz_okno += 1;             //
      i = i - 1;
      }
    bb = bb / N_integr_tmp;           // ����� ��������� �� ���-�� �������
    Rx_skolz_okno = (unsigned long *)& Skolzyashie_okno; // ��������� ������ ����������� ���� - �� ������ ������

    if (N_integr_tmp < N_integr) N_integr_tmp += 1; // ����������� �� 1 ���-�� ������� �� ������ ���� �� ��������� �������������� ��������

    b_ADC_mosta = bb;                 // ��������� ��� ��� ����������� �������� ��� �������� �� HART ���������

    // ���������� ���������� �� ������ �������
    if (bb >= 0x800000)
      {
      bb = bb - 0x800000;             //
      u = 1000*((U_REF/0x800000)*bb); // ���������� ���������� � ��
      }
    else
      {
      bb = 0x800000 - bb;
      u = 1000*(-(U_REF/0x800000)*bb);// ���������� ���������� � ��
      }

    // ���������� ��������
    temp_u = u;
    p = a0 + a1 * u;
    temp_u = temp_u * u;
    p = p + a2 * temp_u;
    temp_u = temp_u * u;
    p = p + a3 * temp_u;
    temp_u = temp_u * u;
    p = p + a4 * temp_u;
    temp_u = temp_u * u;
    p = p + a5 * temp_u;
    p = p * k_pressure + b_pressure - delta_P; // ������������� �� �������� ��������� ��� ���������� + ���� �������� ���� �� ���������� ���������

    davlenie = perevod_v_davlenie (p);

    procent_diappazona = (p - diapazon_n)/((diapazon_v - diapazon_n) / 100);  // ������� ������� ���������

    if (koren)
      {
      if (p >= 0.8 * (diapazon_v / 100))     // 0,8% �� ������ �������� ������ ��������, � ��� ��� ���� - ������ ���������������
        {
        i_out = 4 + 16 * sqrt (p / diapazon_v); // ���������������� �����������
        }
      else
        {
        if (p <= 0.6 * (diapazon_v /100))
          {
          i_out = 4 + p * (16.0 / diapazon_v);               // �� 0 �� 0,6% �� ��������� � ��� ������������� �������� ������������ �� ������ ��������� ������
          }
        else
          {
          i_out = 4.096 + ((p / diapazon_v) - 0.006) * 667.5; // ��� �������� �� 0,6 �� 0,8% � ��� ������������� - ������ �������� ����������� � ������ ��������.
          }
        }
      }
    else
      {
      i_out = 4 + (p - diapazon_n) * (16.0 / (diapazon_v - diapazon_n));      // ������� �������� ��������� ���� ��� �������� �����������
      }

    if (tok_signal_inv)
      {
      i_out = 24 - i_out;             // ����������� ������� ������ ���� ���������� ��� 20-4
      }

    if ((i_out > CURRENT_OUT_ALARM_LOW) && (i_out < CURRENT_OUT_ALARM_HIGH)) // ��������� �������� ��������� ���� �� ���������� ���� � ��� �������� �������� NAMUR NE43	
	{
		flag_loop_current_saturated = 0;										// ������� ����� �� � ���������, ������� ��� �� ������ ����� HART-������� 
	}
	else
	{						
		if (i_out < CURRENT_OUT_ALARM_LOW) 	i_out = CURRENT_OUT_ALARM_LOW; 
		if (i_out > CURRENT_OUT_ALARM_HIGH) i_out = CURRENT_OUT_ALARM_HIGH;
		flag_loop_current_saturated = 1;										// ������� ����� � ���������, ������� ��� �� ������ ����� HART-�������
	}

    if (flag_i_out_auto)              // ���� ��������� � ������ �� �������������� ����
      {
      loop_current = i_out;           //
      OUT_current ();                 // ������ �� ������ ��������� �������� ����
      }

    if (Term_flag == 1)               //���� ������ ����� �������� ����������� ����������
      {
      flag_ADC = 0x01;
      I_ADC_write (0x10);             // ����. ������ � Configuration register (b'00010000')
      I_ADC_write (0x02);             // Bias Voltage Generator Disable, burnout currents are disabled. ���������� �����
                                      // ������������� ����. �������� �=4,
      I_ADC_write (0x11);             // �������� �������� �������� ���������� (b'00010000')
                                      // �������������� �����, ����������� ��� �� ��������� ���������� AIN2(+) � AIN2(�)
      Term_flag = 0;
      flag_ADC_IDLE = 1;              // ��������� ��������� ��� - ����������.
      }
     }
    }
   }
  }
 }
}

//************************************
//***** �/� ���������� ��� AD421 *****
//************************************
void OUT_current (void)
{
volatile unsigned char dac_i;
volatile unsigned long data_d;
if (loop_current < 3.50) loop_current = 3.50; // ������ �������� � ��������� ��������� �������� ����
if (loop_current > 23.0) loop_current = 23.0; //

data_d = b_current + (long)((loop_current - 4.0)/k_current); // �������� ��� ��� �������� � ���

PORT_DAC &=~ ((1<<latch_dac)|(1<<clock_dac)); // ���������� � �������� ������ �������� � ���

for (dac_i=0; dac_i<17; dac_i++)      // 17 ������ ����� ������� ����
  {                                   //
  PORT_DAC &=~ (1<<data_dac);         //
  if (data_d & 0x10000) PORT_DAC |= (1<<data_dac);// ���� ���������� ��� =1,
  data_d = data_d << 1;               // �������� data_d �� ���� ������ ����� ��� ��������� ���������� ����

  PORT_DAC |= (1<<clock_dac);         // SCLK=1 ������ �������������
  _NOP();                             //
  PORT_DAC &=~(1<<clock_dac);         //
  }
PORT_DAC |= (1<<latch_dac);           // ����� �������� ������ � ��� - ������������ �� � �������� �������

_NOP();                               //
}

//************************************
//* �/� ������ � ���  ****************
//************************************
void I_ADC_write (unsigned char byteword)
{
volatile unsigned char adc_i;
for (adc_i=0;adc_i<8;adc_i++)         //
  {                                   //
  PORT_ADC &=~ (1<<DIN_ADC);          // DIN=0
  _NOP();
  PORT_ADC &=~ (1<<SCLK_ADC);         // ������ �������������: SCLK=0
  if (byteword & 0x80) PORT_ADC |= (1<<DIN_ADC);// ���� ���������� ��� =1, �� DIN=1
  byteword = byteword << 1;           // �������� byteword �� ���� ������ �����
  _NOP();                             // ��� ��������� ���������� ����
  PORT_ADC |= (1<<SCLK_ADC);          // SCLK=1
  }
PORT_ADC &=~ (1<<DIN_ADC);            // DIN=0
}

//************************************
//* �/� ������ �� ��� ****************
//************************************
unsigned long I_ADC_read (void)
{
volatile unsigned long byte=0;        //
volatile unsigned char adc_i;
for (adc_i=0; adc_i<24; adc_i++)      //
  {
  PORT_ADC &=~ (1<<SCLK_ADC);         // SCLK=0 ������ �������������
  _NOP();                             //
  PORT_ADC |= (1<<SCLK_ADC);          // SCLK=1
  byte = byte << 1;                   // ���� DOUT=0, ����� ������ �������� tmp �����
  if (P1IN & (1<<DOUT_ADC)) byte = byte + 1;// ���� DOUT=1 - ����� ���������� ��� �������
  }                                   //
return byte;                          // ���������� 24-� ��������� �����
}

//************************************
//**** �/� ������������ ��������� ****
//************************************
#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A0 (void)
{
count_time +=1;                       // +1 � �������� �������
if (count_time == 50)                 // ���� ��������� ������ 100 ��.
  {
  count_time = 0;
  Term_time += 1;
  menu_count += 1;
  IND_time += 1;
  if (IND_time == 3) { IND_flag = 1; IND_time = 0;}
  if (Term_time == 2) IND_DIMM |= 0x01;// ����, ��� �������� ������ ������� �����������
  if (Term_time == 10)                // ���� ������ 1 ���. - ���������� ������ ���������� ���������� � ������� �����������
    {
    //send_flag = 1;
    HELLO_flag = 0;                   // ����� 1 ���. ��������� ����� �������������� ���������
    Term_time = 0;                    // ���������� ������� �������
    Term_flag = 1;                    // ������������� ���� ������ ������� � �������� �����������
    IND_DIMM &=~0x01;
    }
  }
if (ind_cnt == 1)                     // ����� ����������
  {
  if (cnt_key_ == 0)
    {
    key_a = P4IN & ((1<<menu_key)|(1<<nul_key)|(1<<minus_key)|(1<<plus_key));
    cnt_key_ = 1;
    }
  else
    {
    key_b = P4IN & ((1<<menu_key)|(1<<nul_key)|(1<<minus_key)|(1<<plus_key));
    if (key_a == key_b) key = key_a;
    cnt_key_ = 0;
    }
  P4DIR = 0xFF;                       // ����������� ����� �� ����� ��� ����������� ���������
  }

P5OUT = 0xFF;                         // ������� ���������

if (IND_DIMM != 0x00 || (IND_CURSOR == 1 && ind_cnt != cursor_position)) // ��� ������ ������� �����������
{
if (ind_cnt == 1)
  {
  P4OUT = ind_razr_1;
  P5OUT &=~ 0x01;                     // �������� ������ ������ ����������
  }

if (ind_cnt == 2)
  {
  P4OUT = ind_razr_2;
  P5OUT &=~ 0x04;
  }
if (ind_cnt == 3)
  {
  P4OUT = ind_razr_3;
  P5OUT &=~ 0x10;
  }

if (ind_cnt == 4)
  {
  P4OUT = ind_razr_4;
  P5OUT &=~ 0x20;
  }

if (ind_cnt == 5)
  {
  P4OUT = ind_razr_5;
  P5OUT &=~ 0x02;
  }
}

if (ind_cnt == 6)
  {
  P4OUT = ind_razr_6;
  P4DIR = 0xFF & (~((1<<menu_key)|(1<<nul_key)|(1<<minus_key)|(1<<plus_key)));// ������ ������ ����������� �� ����
  P5OUT &=~ 0x08;
  }

ind_cnt += 1;
if (ind_cnt > 6) ind_cnt = 1;
}

//************************************
//* �/� ���������� ���������� ��� ����������� �������*
//************************************
#pragma vector=PORT2_VECTOR
__interrupt void P2_1 (void)
{

if (P2IN & 0x02)                      // ���� ���������� ������� � ����� - ��������� ������ ����������
  {
  TACCTL0 = CCIE;                     // ���������� �� ���������� �������� ������� TA0 - ���������.
  P2IES = 0x02;                       // ��������� ������ ������������ ���������� �� �������� �� 1 � 0.
  }
else
  {
  P5OUT = 0xFF;                       // ������� ���������
  P2IES = 0x00;                       // ��������� ������ ������������ ���������� �� �������� �� 0 � 1.
  TACCTL0 &=~CCIE;                    // ���������� �� ���������� �������� ������� TA0 - ���������.
  }
P2IFG = 0;                            // ���������� ���� ������ ����������
}

//************************************
//* �/� ������������� ��� ���������� *
//************************************
char convert (char c)
{
switch (c)
  {
  case 0: return ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7));   //0
  case 1: return ((1<<b7)|(1<<c7));                                   //1
  case 2: return ((1<<a7)|(1<<b7)|(1<<d7)|(1<<e7)|(1<<g7));           //2
  case 3: return ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<g7));           //3
  case 4: return ((1<<b7)|(1<<c7)|(1<<f7)|(1<<g7));                   //4
  case 5: return ((1<<a7)|(1<<c7)|(1<<d7)|(1<<f7)|(1<<g7));           //5
  case 6: return ((1<<a7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7));   //6
  case 7: return ((1<<a7)|(1<<b7)|(1<<c7));                           //7
  case 8: return ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7));//8
  case 9: return ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<f7)|(1<<g7));   //9
  case 10:return ((1<<a7)|(1<<b7)|(1<<c7)|(1<<e7)|(1<<f7)|(1<<g7));   //A
  case 11:return ((1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7));           //B
  case 12:return ((1<<a7)|(1<<d7)|(1<<e7)|(1<<f7));                   //C
  case 13:return ((1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<g7));           //D
  case 14:return ((1<<a7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7));           //E
  case 15:return ((1<<a7)|(1<<e7)|(1<<f7)|(1<<g7));                   //F
  default: return ((1<<g7));                                          //-
  }
}

//************************************
//** �/� ������ �� ��������� �������� ��������
//************************************
void davlenie_vivod_na_indicator (void)
{
volatile unsigned char Buf[9];
volatile unsigned char num=0;

if (f < 0)                            // ���� ����� �������������
  {
  Buf [num] = '-';                    // � ����� ���������� ���� ����� � ����� ������ �����
  f = -f;
  num += 1;
  }

else                                  // ���� ����� ������������� - �� ������ �������� �� ���������� �������� ��������
  if (f < 100000)
    {
    Buf [num] = (unsigned long) f / 10000;
    f = f - Buf [num] * 10000;
    num += 1;
    }

if (f < 10000)                        // ��� ���� �������� (������ ��� ������������� ������ ���������)
  {
  Buf [num] = (unsigned int) f / 1000;
  f = f - Buf [num] * 1000;
  num += 1;

  Buf [num] = (unsigned int) f / 100;
  f = f - Buf [num] * 100;
  num += 1;

  Buf [num] = (unsigned int) f / 10;
  f = f - Buf [num] * 10;
  num += 1;

  Buf [num] = (unsigned int) f / 1;
  f = f - Buf [num] * 1;
  num += 1;

  f = f * 10000;                       // ��� ��������� ������� ����� ����� �������

  Buf [num] = (unsigned int) f / 1000;
  f = f - Buf [num] * 1000;
  num += 1;

  Buf [num] = (unsigned int) f / 100;
  f = f - Buf [num] * 100;
  num += 1;

  Buf [num] = (unsigned int) f / 10;
  f = f - Buf [num] * 10;
  num += 1;

  Buf [num] = (unsigned int) f / 1;
  f = f - Buf [num] * 1;

  num = 0;

  if (Buf[num] == '-')                // ���� ����� �������������
    {
    ind_razr_1 = (1<<g7);             // � ������ ������� ������� ���� �����
    num += 1;
    }

  while ((Buf[num] == 0) && (num < 4))
    {
    num += 1;
    }
  if (Buf[0] != '-')                  // �������������� ������ ��� ������ �� ��������� + ���������� ������ � ������� ������ ��������� �������������� �����
    {
    ind_razr_1 = convert (Buf[num]);
    if (num == 4) ind_razr_1 |= (1<<h7);
    num += 1;
    }
  ind_razr_2 = convert (Buf[num]);    //
  if (num == 4) ind_razr_2 |= (1<<h7);
  num += 1;
  ind_razr_3 = convert (Buf[num]);    //
  if (num == 4) ind_razr_3 |= (1<<h7);
  num += 1;
  ind_razr_4 = convert (Buf[num]);    //
  if (num == 4) ind_razr_4 |= (1<<h7);
  num += 1;
  ind_razr_5 = convert (Buf[num]);    //
  }
else                                  // ���� ����� ������� �������, �� ������� ��������� � ������������ ����������
  {
  ind_razr_1 = ((1<<a7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7));// E
  ind_razr_2 = ((1<<e7)|(1<<g7));                        // r
  ind_razr_3 = ((1<<e7)|(1<<g7));                        // r
  ind_razr_4 = (1<<g7);                                  // -
  ind_razr_5 = ((1<<a7)|(1<<b7)|(1<<c7));                // 7

  IND_DIMM &=~0x80;
  }
}


//************************************
//** �/� ������ �� ��������� �������� �������������
//************************************
void R_vivod_na_indicator (void)
{
  n = (unsigned int) f/1000;          // ��������� ����� ������������
  ind_razr_1 = convert (n);           //
  f = f - n*1000;

  n = (unsigned int) f/100;           // ��������� ����� ������������
  ind_razr_2 = convert (n);           //
  f = f - n*100;

  n = (unsigned int) f/10;            // ��������� �������� ������������
  ind_razr_3 = convert (n);           //
  f = f - n*10;

n = (unsigned char) f/1;              // ��������� ������ ������������
ind_razr_4 = (convert (n) | (1<<h7)); //�������������� �����
f = f - n*1;

f = f *10;

n = (unsigned char) f/1;              // ��������� ������� ����� ������������
ind_razr_5 = convert (n);             //
}

//************************************
//** �/� ������ 4-� �������� ����� ***
//********* �� EEPROM ****************
//************************************
void READ_4BYTE (unsigned char *ptr2, unsigned int addr)
{
Flash_ptr = (unsigned char *)addr;
*ptr2 = *Flash_ptr;                   // |
ptr2 += 1; Flash_ptr += 1;            // |
*ptr2 = *Flash_ptr;                   // |
ptr2 += 1; Flash_ptr += 1;            // | ��������� 4 ����� ������ � ���������� ������
*ptr2 = *Flash_ptr;                   // |
ptr2 += 1; Flash_ptr += 1;            // |
*ptr2 = *Flash_ptr;                   // |
}

//************************************
//*** �/� ������ 1 ����� �� EEPROM ***
//************************************
unsigned char EEPROM_read(unsigned int adress)
{
Flash_ptr = (unsigned char *)adress;  // �������� ������������ flash 0x1000 - 0xFFFF
return *Flash_ptr;
}

//************************************
//*������ ������������� �������������*
//************************************
void CALL_read (void)
{
volatile unsigned char i_e;           //
CALL_ptr = (unsigned char *) & ca;    // ����������� ��������� �� ������ ������� � �������������� ��������������
Flash_ptr = (unsigned char *) 0x1080; // ��������� ��������� ����� EEPROM, ��� ��������� ������������� ������������

for (i_e=0; i_e<96; i_e++)
  {
  *CALL_ptr++ = *Flash_ptr++;         // ������������ ������������
  }
}

//************************************
//***** ������ �������� ������� ******
//************************************
void read_conf (void)
{
/*if (EEPROM_read(Addr_EEPROM_conf + 125) == 0xFF) // ��� ������ ��������� ����� ���������������� ���������� ��������� ��������� � EEPROM
  {
  EEPROM_write_conf ();               //
  }*/
//N_integr =      EEPROM_read (Addr_EEPROM_conf);    // ���� ��������� �� ������ - �� ��������� ������������ �� EEPROM
tok_signal_inv =EEPROM_read (Addr_EEPROM_conf + 1);
edinica_izmer = EEPROM_read (Addr_EEPROM_conf + 2);
koren =         EEPROM_read (Addr_EEPROM_conf + 3);
DEV_ID =        EEPROM_read (Addr_EEPROM_conf + 4);
PREAMBULE =     EEPROM_read (Addr_EEPROM_conf + 5);
HART_disable =  EEPROM_read (Addr_EEPROM_conf + 6);
magnit_key_enable = EEPROM_read (Addr_EEPROM_conf + 7);
current_alarm = EEPROM_read (Addr_EEPROM_conf + 30);
flag_conf_changed = EEPROM_read (Addr_EEPROM_conf + 32);

CALL_ptr = (unsigned char *) & diapazon_v;
READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 8);

CALL_ptr = (unsigned char *) & diapazon_n;
READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 12);

CALL_ptr = (unsigned char *) & damping_value;
READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 16);

CALL_ptr = (unsigned char *) & delta_P;
READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 20);

CALL_ptr = (unsigned char *) & k_current;
READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 24);

CALL_ptr = (unsigned char *) & b_current;
*CALL_ptr = EEPROM_read (Addr_EEPROM_conf + 28);
CALL_ptr += 1;
*CALL_ptr = EEPROM_read (Addr_EEPROM_conf + 29);

CALL_ptr = (unsigned char *) & UIN + 2; // ������ ��������� ������ �������
*CALL_ptr = EEPROM_read (0xF000 + 0);
CALL_ptr += 1;
*CALL_ptr = EEPROM_read (0xF000 + 1);
CALL_ptr += 1;
*CALL_ptr = EEPROM_read (0xF000 + 2);
}

//==========================================================================================
// �/� ������ �������� ����������� ������������ ������������� ������� �� EEPROM
//==========================================================================================
void read_store_conf (void)
{
  tok_signal_inv =EEPROM_read (Addr_cur_EEPROM_conf + 1);
  edinica_izmer = EEPROM_read (Addr_cur_EEPROM_conf + 2);
  koren =         EEPROM_read (Addr_cur_EEPROM_conf + 3);
  DEV_ID =        EEPROM_read (Addr_cur_EEPROM_conf + 4);
  PREAMBULE =     EEPROM_read (Addr_cur_EEPROM_conf + 5);
  HART_disable =  EEPROM_read (Addr_cur_EEPROM_conf + 6);
  magnit_key_enable = EEPROM_read (Addr_cur_EEPROM_conf + 7);
  current_alarm = EEPROM_read (Addr_cur_EEPROM_conf + 30);
//  flagLang = EEPROM_read (Addr_cur_EEPROM_conf + 31);
  flag_conf_changed = EEPROM_read (Addr_EEPROM_conf + 32);

  CALL_ptr = (unsigned char *) & diapazon_v;
  READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 8);

  CALL_ptr = (unsigned char *) & diapazon_n;
  READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 12);

  CALL_ptr = (unsigned char *) & damping_value;
  READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 16);

  CALL_ptr = (unsigned char *) & delta_P;
  READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 20);

  CALL_ptr = (unsigned char *) & k_current;
  READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 24);

  CALL_ptr = (unsigned char *) & b_current;
  *CALL_ptr = EEPROM_read (Addr_cur_EEPROM_conf + 28);
  CALL_ptr += 1;
  *CALL_ptr = EEPROM_read (Addr_cur_EEPROM_conf + 29);
}                                   

//************************************
//* �/� ������ � EEPROM ���������� ������� �� �������� * ������� 1 *
//************************************
void EEPROM_write_pressure_cal (void)
{
_DINT();                              // ������ ���� ����������
P5OUT = 0xFF;                         // ������� ���������
Flash_ptr = (unsigned char *) 0xFC00; // ��������� �� ������ �������� ������
FCTL1 = FWKEY + ERASE;                // ������������� ��� �������� ��������
FCTL3 = FWKEY;                        // ������� ��� ������ �� ������ Lock bit
*Flash_ptr = 0x00;                    // ��� �������� �������� ��������� ��������� ������
while(FCTL3 & BUSY);                  // �������� ����������.
FCTL1 = FWKEY + WRT;                  // ������������� ����� ������ ����� ������

// ������ ������ � �������
CALL_ptr = (unsigned char *) & k_pressure; // ����������� ������������� ������� �������������� �� ��������
n = 4;
while (n)
  {
  *Flash_ptr = *CALL_ptr;
  Flash_ptr += 1; CALL_ptr += 1;
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  n -= 1;
  }

CALL_ptr = (unsigned char *) &  b_pressure; // ����������� ������������� �������� �������������� �� ��������
n = 4;
while (n)
  {
  *Flash_ptr = *CALL_ptr;
  Flash_ptr += 1; CALL_ptr += 1;
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  n -= 1;
  }

*Flash_ptr = 0x00;                    // ���������� ����� ����������� ��� ������ ������������� ���� ���������. (����������� ��� ��������� � ���� ��� ����� - �� ������������ ������������ �� ���������)
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

FCTL1 = FWKEY;                        // ���������� ��� ������ WRT
while(FCTL3 & BUSY);                  // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
FCTL3 = FWKEY + LOCK;                 // � ��������� ������ ��������
_EINT();

flag_conf_changed = 1;				  // ���������� ���� - ������������ ������� ��������
EEPROM_write_conf();
}

//************************************
//*�/� ������ Final Assembly Number ** ������� 2 *
//************************************
void write_EEPROM_ASSEMBLY_NUMBER (unsigned char *prt)
{
_DINT();                              // ������ ���� ����������
P5OUT = 0xFF;                         // ������� ���������
volatile unsigned char i_e;           //
Flash_ptr = (unsigned char *) 0xFA00; // ��������� �� ������ �������� ������
FCTL1 = FWKEY + ERASE;                // ������������� ��� �������� ��������
FCTL3 = FWKEY;                        // ������� ��� ������ �� ������ Lock bit
*Flash_ptr = 0x00;                    // ��� �������� �������� ��������� ��������� ������
while(FCTL3 & BUSY);                  // �������� ����������.
FCTL1 = FWKEY + WRT;                  // ������������� ����� ������ ����� ������

for (i_e=0; i_e<3; i_e++)
  {
  *Flash_ptr++ = *prt++;              // ���������� �� ������ �������� ����� ��
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  }

FCTL1 = FWKEY;                        // ���������� ��� ������ WRT
while(FCTL3 & BUSY);                  // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
FCTL3 = FWKEY + LOCK;                 // � ��������� ������ ��������
_EINT();

flag_conf_changed = 1;				  // ���������� ���� - ������������ ������� ��������
EEPROM_write_conf();
}

//************************************
//** �/� ������ � EEPROM ������ �� *** ������� 3 *
//************************************
void write_data_PD (unsigned char *prt)
{
_DINT();                              // ������ ���� ����������
P5OUT = 0xFF;                         // ������� ���������
volatile unsigned char i_e;           //
Flash_ptr = (unsigned char *) 0xF800; // ��������� �� ������ �������� ������
FCTL1 = FWKEY + ERASE;                // ������������� ��� �������� ��������
FCTL3 = FWKEY;                        // ������� ��� ������ �� ������ Lock bit
*Flash_ptr = 0x00;                    // ��� �������� �������� ��������� ��������� ������
while(FCTL3 & BUSY);                  // �������� ����������.
FCTL1 = FWKEY + WRT;                  // ������������� ����� ������ ����� ������

*Flash_ptr = *prt;                    // ������� ���������
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash
prt += 4;
Flash_ptr += 1;

for (i_e=0; i_e<4; i_e++)
  {
  *Flash_ptr++ = *prt--;              // ���������� �� ������ ������ ������� ��������
  }
prt += 8;
for (i_e=0; i_e<4; i_e++)
  {
  *Flash_ptr++ = *prt--;              // ���������� �� ������ ������ ������ ��������
  }
prt += 8;
for (i_e=0; i_e<4; i_e++)
  {
  *Flash_ptr++ = *prt--;              // ���������� �� ������ ������ ����������� ����������
  }

unsigned char *ptrModel;                                // ��������� ���������� ��������� ��� ������ ���� � ������ ��
ptrModel = (unsigned char *) & modelPD;                 // ��������� �� ������ ������� ���� 02550�
for (i_e=0; i_e<6; i_e++)
{
  *Flash_ptr++ = *ptrModel++;
  while(!(FCTL3 & WAIT));
}

FCTL1 = FWKEY;                        // ���������� ��� ������ WRT
while(FCTL3 & BUSY);                  // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
FCTL3 = FWKEY + LOCK;                 // � ��������� ������ ��������
_EINT();
}

//************************************
//** �/� ������ � EEPROM ����,����,��������� ** ������� 4 *
//************************************
void write_EEPROM_TEG (unsigned char *prt)
{
_DINT();                              // ������ ���� ����������
P5OUT = 0xFF;                         // ������� ���������
volatile unsigned char i_e;           //
Flash_ptr = (unsigned char *) 0xF600; // ��������� �� ������ �������� ������
FCTL1 = FWKEY + ERASE;                // ������������� ��� �������� ��������
FCTL3 = FWKEY;                        // ������� ��� ������ �� ������ Lock bit
*Flash_ptr = 0x00;                    // ��� �������� �������� ��������� ��������� ������
while(FCTL3 & BUSY);                  // �������� ����������.
FCTL1 = FWKEY + WRT;                  // ������������� ����� ������ ����� ������

for (i_e=0; i_e<21; i_e++)
  {
  *Flash_ptr++ = *prt++;              // ���������� �� ������ ���, ���������, ����
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  }

FCTL1 = FWKEY;                        // ���������� ��� ������ WRT
while(FCTL3 & BUSY);                  // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
FCTL3 = FWKEY + LOCK;                 // � ��������� ������ ��������
_EINT();

flag_conf_changed = 1;				  // ���������� ���� - ������������ ������� ��������
EEPROM_write_conf();
}

//************************************
//** �/� ������ � EEPROM ��������� *** ������� 5 *
//************************************
void write_EEPROM_MESSAGE (unsigned char *prt)
{
_DINT();                              // ������ ���� ����������
P5OUT = 0xFF;                         // ������� ���������
volatile unsigned char i_e;           //
Flash_ptr = (unsigned char *) 0xF400; // ��������� �� ������ �������� ������
FCTL1 = FWKEY + ERASE;                // ������������� ��� �������� ��������
FCTL3 = FWKEY;                        // ������� ��� ������ �� ������ Lock bit
*Flash_ptr = 0x00;                    // ��� �������� �������� ��������� ��������� ������
while(FCTL3 & BUSY);                  // �������� ����������.
FCTL1 = FWKEY + WRT;                  // ������������� ����� ������ ����� ������

for (i_e=0; i_e<24; i_e++)
  {
  *Flash_ptr++ = *prt++;              // ���������� �� ������ MESSAGE
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  }

FCTL1 = FWKEY;                        // ���������� ��� ������ WRT
while(FCTL3 & BUSY);                  // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
FCTL3 = FWKEY + LOCK;                 // � ��������� ������ ��������
_EINT();

flag_conf_changed = 1;				  // ���������� ���� - ������������ ������� ��������
EEPROM_write_conf();
}

//************************************
//** �/� ������ � EEPROM ��������� ������ �� ** ������� 6 *
//************************************
void write_EEPROM_SERIAL_PD (unsigned char *prt)
{
_DINT();                              // ������ ���� ����������
P5OUT = 0xFF;                         // ������� ���������
volatile unsigned char i_e;           //
Flash_ptr = (unsigned char *) 0xF200; // ��������� �� ������ �������� ������
FCTL1 = FWKEY + ERASE;                // ������������� ��� �������� ��������
FCTL3 = FWKEY;                        // ������� ��� ������ �� ������ Lock bit
*Flash_ptr = 0x00;                    // ��� �������� �������� ��������� ��������� ������
while(FCTL3 & BUSY);                  // �������� ����������.
FCTL1 = FWKEY + WRT;                  // ������������� ����� ������ ����� ������

for (i_e=0; i_e<3; i_e++)
  {
  *Flash_ptr++ = *prt++;              // ���������� �� ������ �������� ����� ��
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  }

FCTL1 = FWKEY;                        // ���������� ��� ������ WRT
while(FCTL3 & BUSY);                  // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
FCTL3 = FWKEY + LOCK;                 // � ��������� ������ ��������
_EINT();
}

//************************************
//*�/� ������ � EEPROM ���������� ������ ��* ������� 7 *
//************************************
void write_EEPROM_ZAVOD_NUMBER_DD (unsigned char *prt)
{
_DINT();                              // ������ ���� ����������
P5OUT = 0xFF;                         // ������� ���������
volatile unsigned char i_e;           //
Flash_ptr = (unsigned char *) 0xF000; // ��������� �� ������ �������� ������
FCTL1 = FWKEY + ERASE;                // ������������� ��� �������� ��������
FCTL3 = FWKEY;                        // ������� ��� ������ �� ������ Lock bit
*Flash_ptr = 0x00;                    // ��� �������� �������� ��������� ��������� ������
while(FCTL3 & BUSY);                  // �������� ����������.
FCTL1 = FWKEY + WRT;                  // ������������� ����� ������ ����� ������

for (i_e=0; i_e<3; i_e++)
  {
  *Flash_ptr++ = *prt++;              // ���������� �� ������ ��������� ����� ��
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  }

FCTL1 = FWKEY;                        // ���������� ��� ������ WRT
while(FCTL3 & BUSY);                  // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
FCTL3 = FWKEY + LOCK;                 // � ��������� ������ ��������
_EINT();
}

//************************************
//* �/� ������ � EEPROM ������������� ������������� * ������� A *
//************************************
void EEPROM_write_call (void)
{
_DINT();                              // ������ ���� ����������
P5OUT = 0xFF;                         // ������� ���������
volatile unsigned char i_e;           //
Flash_ptr = (unsigned char *) 0x1080; // ��������� �� ������ �������� ������
CALL_ptr  = (unsigned char *) & ca;   //
FCTL1 = FWKEY + ERASE;                // ������������� ��� �������� ��������
FCTL3 = FWKEY;                        // ������� ��� ������ �� ������ Lock bit
*Flash_ptr = 0x00;                    // ��� �������� �������� ��������� ��������� ������
while(FCTL3 & BUSY);                  // �������� ����������.
FCTL1 = FWKEY + WRT;                  // ������������� ����� ������ ����� ������

for (i_e=0; i_e<96; i_e++)
  {
  *Flash_ptr++ = *CALL_ptr++;         // ���������� �� ������ ���� ������ ������������� �������������
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  }

FCTL1 = FWKEY;                        // ���������� ��� ������ WRT
while(FCTL3 & BUSY);                  // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
FCTL3 = FWKEY + LOCK;                 // � ��������� ������ ��������
_EINT();
}

//************************************
//* �/� ������ � EEPROM ������������ ������� * ������� B *
//************************************
void EEPROM_write_conf (void)
{
_DINT();                              // ������ ���� ����������
P5OUT = 0xFF;                         // ������� ���������
Flash_ptr = (unsigned char *) Addr_EEPROM_conf;// ��������� �� ������ �������� ������
FCTL1 = FWKEY + ERASE;                // ������������� ��� �������� ��������
FCTL3 = FWKEY;                        // ������� ��� ������ �� ������ Lock bit
*Flash_ptr = 0x00;                    // ��� �������� �������� ��������� ��������� ������
while(FCTL3 & BUSY);                  // �������� ����������.
FCTL1 = FWKEY + WRT;                  // ������������� ����� ������ ����� ������

// ������ ������ � �������
*Flash_ptr = N_integr;                // 32 ��������� ��� ��������������
Flash_ptr += 1;
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

*Flash_ptr = tok_signal_inv;          // �������� ������� ������ 4-20 �� ��� 20-4 ��
Flash_ptr += 1;
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

*Flash_ptr = edinica_izmer;           // ������� ���������
Flash_ptr += 1;
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

*Flash_ptr = koren;                   // ���������������� ����������� ���������
Flash_ptr += 1;
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

*Flash_ptr = DEV_ID;                  // ����� ����������
Flash_ptr += 1;                       //
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

*Flash_ptr = PREAMBULE;               // ���������� ��������
Flash_ptr += 1;                       //
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

*Flash_ptr = HART_disable;            // ���������� ��������� ������������ �� HART
Flash_ptr += 1;                       //
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

*Flash_ptr = magnit_key_enable;       // ���������� ������ ��������� ������
Flash_ptr += 1;                       //
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

CALL_ptr = (unsigned char *) & diapazon_v;// ������� ������ 250 ���
n = 4;
while (n)
  {
  *Flash_ptr = *CALL_ptr;
  Flash_ptr += 1; CALL_ptr += 1;
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  n -= 1;
  }

CALL_ptr = (unsigned char *) & diapazon_n;// ������ ������ 0 ���
n = 4;
while (n)
  {
  *Flash_ptr = *CALL_ptr;
  Flash_ptr += 1; CALL_ptr += 1;
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  n -= 1;
  }

CALL_ptr = (unsigned char *) & damping_value;// ����� �������������
n = 4;
while (n)
  {
  *Flash_ptr = *CALL_ptr;
  Flash_ptr += 1; CALL_ptr += 1;
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  n -= 1;
  }

CALL_ptr = (unsigned char *) & delta_P;// �������� ��� ��������� 0 ��������
n = 4;
while (n)
  {
  *Flash_ptr = *CALL_ptr;
  Flash_ptr += 1; CALL_ptr += 1;
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  n -= 1;
  }

CALL_ptr = (unsigned char *) & k_current;// �������� ������������ �������� ���
n = 4;
while (n)
  {
  *Flash_ptr = *CALL_ptr;
  Flash_ptr += 1; CALL_ptr += 1;
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  n -= 1;
  }

CALL_ptr = (unsigned char *) & b_current;// �������� ������������ �������� ���� ���
n = 2;
while (n)
  {
  *Flash_ptr = *CALL_ptr;
  Flash_ptr += 1; CALL_ptr += 1;
  while(!(FCTL3 & WAIT));             // ������� ���������� ������ �� Flash
  n -= 1;
  }

Flash_ptr = (unsigned char *) Addr_EEPROM_conf + 30;
*Flash_ptr = current_alarm;           // ��������� �������� ���� ��� ��������� ������ (Alarm)
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

Flash_ptr = (unsigned char *) Addr_EEPROM_conf + 32;
*Flash_ptr = flag_conf_changed;                     // ���� - �������� ������������ �������
while(!(FCTL3 & WAIT));

Flash_ptr = (unsigned char *) Addr_EEPROM_conf + 125;
*Flash_ptr = 0x00;
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash
Flash_ptr += 1;                       //

CALL_ptr = (unsigned char *) Addr_EEPROM_conf;

CRC16 (CALL_ptr, 126);                // ����������� �������� CRC ��� ����� ������ � ����������� �������

*Flash_ptr = crc_H;                   //
Flash_ptr += 1;                       //
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

*Flash_ptr = crc_L;                   //
while(!(FCTL3 & WAIT));               // ������� ���������� ������ �� Flash

FCTL1 = FWKEY;                        // ���������� ��� ������ WRT
while(FCTL3 & BUSY);                  // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
FCTL3 = FWKEY + LOCK;                 // � ��������� ������ ��������
_EINT();
}


//==========================================================================================
// �/� ������ � EEPROM ������� ������������ ������� * ������� 9 * 0xEDFF-0xEC00
//==========================================================================================
void write_current_conf(void)
{
  WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog
  _DINT();                                                                                // ������ ���� ����������
  Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf;                                     // ��������� �� ������ �������� ������
  FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
  FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
  *Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
  while(FCTL3 & BUSY);                                                                    // �������� ����������.
  FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������

  // ������ ������ � �������
  *Flash_ptr = N_integr;                                                                  // 32 ��������� ��� ��������������
  Flash_ptr += 1;
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

  *Flash_ptr = tok_signal_inv;                                                            // �������� ������� ������ 4-20 �� ��� 20-4 ��
  Flash_ptr += 1;
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

  *Flash_ptr = edinica_izmer;                                                             // ������� ���������
  Flash_ptr += 1;
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

  *Flash_ptr = koren;                                                                     // ���������������� ����������� ���������
  Flash_ptr += 1;
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

  *Flash_ptr = DEV_ID;                                                                    // ����� ����������
  Flash_ptr += 1;                                                                         //
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

  *Flash_ptr = PREAMBULE;                                                                 // ���������� ��������
  Flash_ptr += 1;                                                                         //
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

  *Flash_ptr = HART_disable;                                                              // ���������� ��������� ������������ �� HART
  Flash_ptr += 1;                                                                         //
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

  *Flash_ptr = magnit_key_enable;                                                         // ���������� ������ ��������� ������
  Flash_ptr += 1;                                                                         //
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

  CALL_ptr = (unsigned char *) & diapazon_v;                                              // ������� ������ 250 ���
  n = 4;
  while (n)
  {
    *Flash_ptr = *CALL_ptr;
    Flash_ptr += 1; CALL_ptr += 1;
    while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
    n -= 1;
  }

  CALL_ptr = (unsigned char *) & diapazon_n;                                              // ������ ������ 0 ���
  n = 4;
  while (n)
  {
    *Flash_ptr = *CALL_ptr;
    Flash_ptr += 1; CALL_ptr += 1;
    while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
    n -= 1;
  }

  CALL_ptr = (unsigned char *) & damping_value;                                           // ����� �������������
  n = 4;
  while (n)
  {
    *Flash_ptr = *CALL_ptr;
    Flash_ptr += 1; CALL_ptr += 1;
    while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
    n -= 1;
  }

  CALL_ptr = (unsigned char *) & delta_P;                                                 // �������� ��� ��������� 0 ��������
  n = 4;
  while (n)
  {
    *Flash_ptr = *CALL_ptr;
    Flash_ptr += 1; CALL_ptr += 1;
    while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
    n -= 1;
  }

  CALL_ptr = (unsigned char *) & k_current;                                               // �������� ������������ �������� ���
  n = 4;
  while (n)
  {
    *Flash_ptr = *CALL_ptr;
    Flash_ptr += 1; CALL_ptr += 1;
    while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
    n -= 1;
  }

  CALL_ptr = (unsigned char *) & b_current;                                               // �������� ������������ �������� ���� ���
  n = 2;
  while (n)
  {
    *Flash_ptr = *CALL_ptr;
    Flash_ptr += 1; CALL_ptr += 1;
    while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
    n -= 1;
  }

  Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf + 30;
  *Flash_ptr = current_alarm;                                                             // ��������� �������� ���� ��� ��������� ������ (Alarm)
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

//  Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf + 31;
//  *Flash_ptr = flagLang;                                                                // ��������� �������� ����� ����
//  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

  Flash_ptr = (unsigned char *) Addr_EEPROM_conf + 32;
  *Flash_ptr = flag_conf_changed;                                                         // ���� - �������� ������������ �������
  while(!(FCTL3 & WAIT));

  Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf + 125;
  *Flash_ptr = 0x00;
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash
  Flash_ptr += 1;                                                                         //

  CALL_ptr = (unsigned char *) Addr_cur_EEPROM_conf;

  CRC16 (CALL_ptr, 126);                                                                  // ����������� �������� CRC ��� ����� ������ � ����������� �������

  *Flash_ptr = crc_H;                                                                     //
  Flash_ptr += 1;                                                                         //
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

  *Flash_ptr = crc_L;                                                                     //
  while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

  FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
  while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
  FCTL3 = FWKEY + LOCK;                                                                   // � ��������� ������ ��������
  WDTCTL = WDT_ARST_1000;                                         // �������� watchdog � �������� ��� ������
  _EINT();
}

//************************************
//*** �/� ���������� ���� c ������ ***
//************************************
void Nul_menu (void)
{
if (Nul_corr == 0)
  {
  Nul_corr = 1;
  menu_count = 0;                     // ���������� ������� �������
  KEY_PUSH_FLAG = 1;
  IND_DIMM = 0x00;                    // �������� ������� �����������
  }

if (flag_error_menu && menu_count > 10) // ����� ��������� �� ������ ���������� ����� 1 ���.
  {
  flag_error_menu = 0;
  Nul_corr = 0;
  IND = 0;
  }

if (KEY_PUSH_FLAG == 0 || menu_count > 5)
  {
  switch (key)
    {
    case (0xA5-(1<<nul_key)):         // ���� ������ ������ "nul"
      {
      if (KEY_PUSH_FLAG)
        {
        menu_count = 0;               // ���������� ������� �������

        if (P2IN & 0x80)              // ���� ���������� �� ������������� �� ������
        {
        if (proverka_delta_P((delta_P + p), 0) == 0) // ������ �������� �������� ����
          {
          delta_P += p;               // ������������ �������� �������� ����
          flag_conf_changed = 1;			  							// ���� - ������������ ������� ��������
          EEPROM_write_conf ();       // ��������� ����� �������� ��������� ����
          Nul_corr = 0;               // ��������� ������ � �/� ������������� ����
          }
        else                          // ���� �������� �������� ���� ������� �� ������ - ������� ��������� �� ������
          {
          flag_error_menu = 1;
          error_number_menu = 3;
          IND = 1;                    // �� ����� ������ ��������� �� ������ ��������� ����� �������� ��������
          znach_print ();
          }
        }
        else                          // ���� ���������� �������� �� ������ - ������� ��������� �� ������.
          {
          flag_error_menu = 1;
          error_number_menu = 8;
          IND = 1;                    // �� ����� ������ ��������� �� ������ ��������� ����� �������� ��������
          znach_print ();
          }
        IND_DIMM |=0x80;              // ���������� ������ �����������
        //Nul_corr = 0;
        KEY_PUSH_FLAG = 1;
        }
      else
        {
        KEY_PUSH_FLAG = 1;
        menu_count = 0;               // ���������� ������� �������
        }
      break;                          // � ���� ����� �������� ������ "����"
      }
    case (0xA5-(1<<menu_key)):        // ���� ������ ������ "menu"
      {
      Nul_corr = 0;
      menu_count = 0;                 // ���������� ������� �������
      KEY_PUSH_FLAG = 1;
      IND_DIMM |=0x80;                // ���������� ������ �����������
      break;
      }
    default:break;
    }
  }
if (menu_count >= 250) {Nul_corr = 0; IND_DIMM |= 0x80;} // ���� � ������� 25 ���. ������ �� �������� - ����� �� ���� ��� ���������� ��������� ���������
}

//************************************
//*** �/� ���������� ���� c ��������� ������ ***
//************************************
void Nul_magnit_menu (void)
{
if (magnit_key_enable)                // ���� ��������� ������ ��������� ������
  {
  if (Nul_magnit_corr == 0)           // ��� ����� � ���� ��������� ���� � ��������� ������
    {                                 // ������������� ����������� ���������
    Nul_magnit_corr = 1;
    menu_count = 0;                   // ���������� ������� �������
    KEY_magnit_PUSH_FLAG = 1;
    IND_DIMM = 0x00;                  // �������� ������� �����������
    }

  if (flag_error_menu && menu_count > 10) // ����� ��������� �� ������ ���������� ����� 1 ���.
    {
    flag_error_menu = 0;
    Nul_magnit_corr = 0;
    IND = 0;
    }

  if (KEY_magnit_PUSH_FLAG == 0 && menu_count > 10)  // ���� ������ ������ � ������ ����� 1 ��� - ��������� ��������� ������������ ��������� ������
    {
    if (!(P3IN & 0x40))
      {
      menu_count = 0;                 // ���������� ������� �������

      if (P2IN & 0x80)                // ���� ���������� �� ������������� �� ������
        {
        if (proverka_delta_P((delta_P + p), 0) == 0) // ������ �������� �������� ����
          {
          delta_P += p;               // ������������ �������� �������� ����
          flag_conf_changed = 1;			  							// ���� - ������������ ������� ��������
          EEPROM_write_conf ();       // ��������� ����� �������� ��������� ����
          Nul_magnit_corr = 0;        // ��������� ������ � ���� ��������� ����
          }
        else                          // ���� �������� �������� ���� ������� �� ������ - ������� ��������� �� ������
          {
          flag_error_menu = 1;
          error_number_menu = 3;
          IND = 1;                    // �� ����� ������ ��������� �� ������ ��������� ����� �������� ��������
          znach_print ();
          }
        }
      else                            // ���� ���������� �������� �� ������ - ������� ��������� �� ������.
        {
        flag_error_menu = 1;
        error_number_menu = 8;
        IND = 1;                      // �� ����� ������ ��������� �� ������ ��������� ����� �������� ��������
        znach_print ();
        }
      IND_DIMM |=0x80;                // ���������� ������ �����������
      //Nul_magnit_corr = 0;
      KEY_magnit_PUSH_FLAG = 1;
      }

    if (menu_count >= 250) {Nul_magnit_corr = 0; IND_DIMM |= 0x80;} // ���� � ������� 25 ���. ������ �� �������� - ����� �� ���� ��� ���������� ��������� ���������
    }
  }
}

//************************************
//********** �/� ������ � ���� *******
//************************************
void work_menu(void)
{
if (Mm == 0 || cancel_edit == 1)      // ���� � ���� ������ ������ ��� ��� ��� ����� �� ���� ��� ���������� ��������� - ������������ �� ����� ������� ��������� ������� ��� ����������� �����������
  {                                   // �������� ��� ��������� ����� � ����.
  tmp_diapazon_v      = diapazon_v;
  tmp_diapazon_n      = diapazon_n;   // ����� ������������ - ������������ �� ��������� ��������
  tmp_N_integr        = N_integr;     // ���� �� �������� ������ ������ �� ��� ����������� ������� ���������
  tmp_tok_signal_inv  = tok_signal_inv;
  tmp_edinica_izmer   = edinica_izmer;
  tmp_koren           = koren;
  tmp_magnit_key_enable = magnit_key_enable;
  tmp_ID              = DEV_ID;
  tmp_damping_value   = damping_value;
  tmp_HART_disable    = HART_disable;
  tmp_current_alarm   = current_alarm;
  tmp_flag_i_out_auto = flag_i_out_auto;
  num_menu_ch = 1;
  cancel_edit = 0;
  cursor_position = 5;
  IND_DIMM |= 0x80;                   // ������ ������ ������������
  }

if (flag_error_menu && menu_count > 10) // ����� ��������� �� ������ ���������� ����� 1 ���.
  {
  flag_error_menu = 0;
  Vm = 0;
  znach_print ();
  }

if (IND_flag == 1 && (Mm == 9 || Mm == 10 && (num_menu_ch == 1 || num_menu_ch == 3))) // � ������ ���������� �� �������� � ��������� ���� ������� ���������� �������� �� ����� IND_flag
  {                                   // ����� � �������� ����������� ��������� ����������� ��������
  IND_flag = 0;
  znach_print ();
  }

if (flag_message_menu && menu_count > 10) // ����� ��������������� ��������� ���������� ����� 1 ���.
  {
  flag_message_menu = 0;
  num_menu_ch += 1;
  if (num_menu_ch == 1)
    {
    if (Mm == 11)                     // ��� ������ ���������� �������� ������
      {
      mp = 2;
      preobrazovanie_v_chelochislennoe (4.0000, mp); // ����� �� ��������� �������� ���� 4 ��
      IND_CURSOR = 1;
      }
    }
  if (num_menu_ch == 3)
    {
    if (Mm == 11)                     // ��� ������ ���������� �������� ������
      {
      mp = 2;
      preobrazovanie_v_chelochislennoe (20.0000, mp); // ����� �� ��������� �������� ���� 20 ��
      IND_CURSOR = 1;
      }
    }
  if (num_menu_ch > 4)                // ����� ������ ��������� �� ������� ���������� ������� � �������� ����
    {
    IND_CURSOR = 0;
    Vm = 0;
    }
  znach_print ();
  }

if (KEY_PUSH_FLAG == 0)
  {
switch (key)
  {
case (0xA5-(1<<menu_key)):            // ���� ������ ������ "����"
                     {
                     menu_count = 0;  // ���������� ������� �������
                     if (Mm == 0 && Vm == 0)  // ���� ������ � ���� ������ ���
                      {
                      Mm = 1; znach_print (); // ������� �������� ������� ����
                      }
                     else
                      {
                      if (Mm != 0 && Vm == 0)
                        {
                        if (Mm == 2)  // ������������� ���
                          {
                          mp = raschet_n ();
                          if (mp < 6)
                            {
                            preobrazovanie_v_chelochislennoe (perevod_v_davlenie (tmp_diapazon_v), mp);
                            IND_CURSOR = 1;
                            }
                          else        // ���� ����� ������� ������� - ������� ��������� �� ������
                            {
                            flag_error_menu = 1;
                            error_number_menu = 7;              //
                            }
                          }
                        if (Mm == 3)  // ������������� ���
                          {
                          mp = raschet_n ();
                          if (tmp_diapazon_n < 0) mp += 1;      // ���� ��� �������������, �� ���������� ���������� ����� ������ �� 1 ������, ���������� ������ ������ ��� ���� �����
                          if (mp < 6)
                            {
                            preobrazovanie_v_chelochislennoe (perevod_v_davlenie (tmp_diapazon_n), mp);
                            IND_CURSOR = 1;
                            }
                          else        // ���� ����� ������� ������� - ������� ��������� �� ������
                            {
                            flag_error_menu = 1;
                            error_number_menu = 7; //
                            }
                          }
                        if (Mm == 10) // ���������� �� ��������
                          {
                          num_menu_ch = 0;         // ������ ���������� �� ��������.
                          flag_message_menu = 1;   // ����� ��������� � ������ ����� ����������
                          }
                        if (Mm == 11 && DEV_ID == 0) // ���������� �������� ������ � ������� ����� ������� ����� ����
                          {
                          num_menu_ch = 0;         // ������ ���������� �������� ������.
                          flag_message_menu = 1;
                          flag_i_out_auto = 0;     // ������ � ����� �������������� ����
                          loop_current = 4.000;
                          OUT_current ();          // ������������� �������� ����.
                          }

                        if (Mm != 11 || Mm == 11 && DEV_ID == 0) // ���� � ���� ���������� �� ���� �������� ������ ��� ������� ������ �������
                          {
                          Vm = 1;                  // ������ ������ ���� ����� ������ ��������� - ������� �� ��������� �������� ����� ���������
                          IND_DIMM &=~0x80;        // �������� ������� �����������
                          znach_print ();
                          }

                        if (Mm == 15)                           // ��������� ���� ������ ������������ ��������
                          {
                          mp = 3;
                          preobrazovanie_v_chelochislennoe (000.00, mp);
                          IND_CURSOR = 1;
                          znach_print ();
                          }
                        }
                      else
                        {
                        if (Mm == 10)              // ���������� �� ��������
                          {
                          if (num_menu_ch == 1)    // � ����� ���
                            {
                            flag_message_menu = 1; // ������� �������������� ��������� � ��������� ����� ����������
                            save (Mm);             // ���������� ������ ������������ �������� ���
                            num_menu_ch = 2;       // ��������� ����� ��������� ��������
                            znach_print ();
                            }
                          else                     // � ����� ���
                            {
                            //Vm = 0;              // ����� ��������� ��������� - ��������� �������� � ��������� � ������ �� ��������� �������� ����
                            save (Mm);             // ���������� ������ � ���������� ������ � ���������� ���
                            znach_print ();
                            IND_DIMM |=0x80;       // ��������� ������� �����������
                            }
                          }
                        else

                        if (Mm == 11) // ���������� �������� ������
                          {
                          if (num_menu_ch == 1)    // � ����� 4 ��
                            {
                            flag_message_menu = 1; // ������� �������������� ��������� � ����� ����������
                            save (Mm);             // ���������� ������ ������������ �������� ���� ���
                            num_menu_ch = 2;       // ��������� ����� ��������� ��������
                            znach_print ();
                            IND_CURSOR = 0;
                            cursor_position = 5;
                            loop_current = 20.000; // ����� ���������� ����� 4 �� ������� �������� ���� 20 ��
                            OUT_current ();        // ������������� �������� ����.
                            }
                          else                     // � ����� 20 ��
                            {
                            //Vm = 0;              // ����� ��������� ��������� - ��������� �������� � ��������� � ������ �� ��������� �������� ����
                            save (Mm);             // ���������� ������ � ���������� ������ � ���������� ���
                            flag_i_out_auto = 1;   // ����� �� ������ �������������� ����
                            znach_print ();
                            IND_DIMM |=0x80;       // ��������� ������� �����������
                            IND_CURSOR = 0;
                            cursor_position = 5;
                            }
                          }
                        else
                          {
                          Vm = 0;                  // ����� ��������� ��������� - ��������� �������� � ��������� � ������ �� ��������� �������� ����
                          save (Mm);
                          znach_print ();
                          IND_DIMM |=0x80;         // ��������� ������� �����������
                          IND_CURSOR = 0;
                          cursor_position = 5;
                          }
                        }
                      }
                     KEY_PUSH_FLAG = 1;
                     break;           // � ���� ����� �������� ������ "����"
                     }
case (0xA5-(1<<plus_key)):            // ���� ������ ������ "plus"
        {
         menu_count = 0;  // ���������� ������� �������
         if (Vm)
                  {
                    if (Mm == 1)     // ������� ���������
          			{
                      if (tmp_edinica_izmer == 0x0C) tmp_edinica_izmer = 0xED;
                      else
                        {
                        if (tmp_edinica_izmer == 0xED) tmp_edinica_izmer = 0x05;
                        else
                          {
                          if (tmp_edinica_izmer == 0x05) tmp_edinica_izmer = 0xEF;
                          else
                            {
                            if (tmp_edinica_izmer == 0xEF) tmp_edinica_izmer = 0x0A;
                            else
                              {
                              if (tmp_edinica_izmer == 0x0A) tmp_edinica_izmer = 0xB0;
                              else
                                {
                                if (tmp_edinica_izmer == 0xB0) tmp_edinica_izmer = 0x07;
                                else
                                  {
                                  if (tmp_edinica_izmer == 0x07) tmp_edinica_izmer = 0x0B;
                                  else
                                  	#if defined (_EDIZMER_) // ��������� ����, ������ ��� �������� � ��� �� ���������
                                    {
                                    if (tmp_edinica_izmer == 0x0B) tmp_edinica_izmer = 0x08; // �������� ��� ���� ����� ���� ����
	                                else
	                                  {
	                                  tmp_edinica_izmer = 0x0C;
	                                  }
                                    }
                                    #else
                                    {
	                                  tmp_edinica_izmer = 0x0C;
	                                }
                                    #endif /*_EDIZMER_*/
                                  }
                                }
                              }
                            }
                          }
                        }
                    }
                     if (Mm == 2 || Mm == 3 || Mm == 11 || Mm == 15)     // ��� + ��� + ��������� �� ���� + ��������� ���� ������ �������� ����������� ��������
          {
                      if (cursor_position == 1) {if (r1 < 9) r1 ++;
                                                else
                                                  {
                                                  if (Mm == 2 || Mm == 3)       // ����� ����� ����� ������ � ���� �������������� ��� � ���
                                                    {
                                                    if (r1 == 16)               // ���� � ������ ������� ������� �����
                                                      {
                                                      mp -= 1;                  // ���������� ���������� ����� ����� �� ���� ������
                                                      r2 = r3;
                                                      r3 = r4;
                                                      r4 = r5;
                                                      r5 = r5_tmp;              // ���������� ��� �������� ����� ��� ���������� ����� �����.
                                                      }
                                                    if (r1 == 9)                // ���� � ������ ������� �������� 9
                                                      {
                                                      r1 = 16;                  // ������� ���� �����
                                                      mp += 1;                  // ���������� ���������� ����� ������ �� ���� ������
                                                      r5_tmp = r5;              // ���������� ��� �������� ������ ��� ������ ����� �����.
                                                      r5 = r4;
                                                      r4 = r3;
                                                      r3 = r2;
                                                      r2 = 0;
                                                      }
                                                    else r1 = 0;
                                                    }
                                                  else r1 = 0;
                                                  }}
                      if (cursor_position == 2) {if (r2 < 9) r2 ++; else r2 = 0;}
                      if (cursor_position == 3) {if (r3 < 9) r3 ++; else r3 = 0;}
                      if (cursor_position == 4) {if (r4 < 9) r4 ++; else r4 = 0;}
                      if (cursor_position == 5) {if (r5 < 9) r5 ++; else r5 = 0;}
          }
         if (Mm == 4)     //  ����� ��������������
          {
          if (tmp_damping_value <= 0.05) tmp_damping_value = 0.1;
                      else
                        {
                        if (tmp_damping_value <= 0.1) tmp_damping_value = 0.2;
                        else
                          {
                          if (tmp_damping_value <= 0.2) tmp_damping_value = 0.5;
                          else
                            {
                            if (tmp_damping_value <= 0.5) tmp_damping_value = 1.0;
                            else
                              {
                              if (tmp_damping_value <= 1.0) tmp_damping_value = 2.0;
                              else
                                {
                                if (tmp_damping_value <= 2.0) tmp_damping_value = 5.0;
                                else
                                  {
                                  if (tmp_damping_value <= 5.0) tmp_damping_value = 10.0;
                                  else
                                    {
                                    if (tmp_damping_value <= 10.0) tmp_damping_value = 20.0;
                                    else
                                      {
                                      tmp_damping_value = 20.0;
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
          }
         if (Mm == 5)     // ���������������� �����������
          {
          tmp_koren ^= 0x01;
          }
         if (Mm == 6)     // ������ / �������� ����������� ��������� �������
          {
          tmp_tok_signal_inv ^= 0x01;
          }
         if (Mm == 7)     // ������� ����� �������
          {
                      if (tmp_ID < 15) tmp_ID +=1;
                      else tmp_ID = 0;
          }
                     if (Mm == 8)     // ���������� ������ ���������� ������ ��������� ����
          {
                      tmp_magnit_key_enable ^= 0x01;
          }
                     if (Mm == 9)
          {
                      // ��� ������ ��� ����� ���� �� ������������
                      }
                     if (Mm == 10)    // ���������� �� ��������
          {
                      // ��� ������ ��� ����� ���� �� ������������
                      }
                   /*  if (Mm == 11)    // ���������� �� �������� ������
          {

                      } */
                     if (Mm == 12)
          {
                      if (DEV_ID == 0)// ���� ���������� ����� 0 ����� - ����� ����� ���������� � ����� �������������� ����
                        {
                        if (flag_i_out_auto != 0)
                          {
                          flag_i_out_auto = 0;    // ������ � ����� �������������� ����
                          loop_current = 3.8001;
                          }
                        else
                          {
                          if (loop_current <= 3.8001) loop_current = 4.000;
                          else if (loop_current <= 4.000) loop_current = 8.000;
                            else if (loop_current <= 8.000) loop_current = 12.000;
                              else if (loop_current <= 12.000) loop_current = 16.000;
                                else if (loop_current <= 16.000) loop_current = 20.000;
                                  else if (loop_current <= 20.000) loop_current = 22.500;
                                     else flag_i_out_auto = 1;  // ����� �� ������ �������������� ����
                          }
                        OUT_current (); // ������������� ����� �������� ����.
                        }
                      }
                     if (Mm == 13)    // ������ ��������� ������������ �� HART ���������
          {
                      tmp_HART_disable ^= 0x01;
                      }
                     if (Mm == 14)    // ��������� �������� ���� ��� ��������� ������ (Alarm)
                      {
                      tmp_current_alarm ^= 0x01;
                      }
                     }
                     else             // ��������� �� ��������� ����
                      {
                        unsigned char *ptrTypeTrans;                            // ��������� ��������� �� ��� ��������� �������� � ������
                        ptrTypeTrans = (unsigned char *) 0xF80D;                // ������ ��������� �� ����� � ������ � ����� ��������� ��������
                        if (*ptrTypeTrans == 1)                                 // ���� ������ -��
                        {
                          if (Mm < 15) Mm++;//                                  // 15 ������� ����
                          else Mm = 1;    //
                        }
                        else
                        {
                          if (Mm < 14) Mm++;//                                  // 14 ������� ����
                          else Mm = 1;    //
                        }
                      }
                     znach_print ();
                     KEY_PUSH_FLAG = 1;
         break;
         }
case (0xA5-(1<<nul_key)):       // ���� ������ ������ "nul"
         {
         menu_count = 0;  // ���������� ������� �������
         if (Vm)          // ���� ����������� ��������� ��������
                      {               // �� ������� �� ������� �����
                      if (Mm == 12 && tmp_flag_i_out_auto == 1) // ���� � ������ �������������� ����, �� ��� ������ �� ������� ������
                        {
                        flag_i_out_auto = 1;  // ����� �� ������ �������������� ���� ��� ������� ��� �� ����� � ���� �� ���� ������ �������������� ����
                        }
                      if (Mm == 11)   // ��� ������ ���������� �������� ������ -
                        {
                        flag_i_out_auto = 1;  // ����� �� ������ �������������� ����
                        }
                      Vm = 0;         //
                      znach_print (); //
                      IND_DIMM |=0x80;// ��������� ������� �����������
                      cancel_edit = 1;// ��� ������ ��� ���������� - ������������ ��������� ������� ��� ����������� ����������� ��� ��������� ����� � ����� ����
                      IND_CURSOR = 0;
                      }               // ���� ������� �������� ���� -
                     else
                      {
                      Mm = 0;         // ��������� ������ � ����
                      }
                     KEY_PUSH_FLAG = 1;
         break;
         }
case (0xA5-(1<<minus_key)):           // ���� ������ ������ "minus"
         {
         menu_count = 0;  // ���������� ������� �������
         if (Vm==0)
                      {
                        unsigned char *ptrTypeTrans;                            // ��������� ��������� �� ��� ��������� �������� � ������
                        ptrTypeTrans = (unsigned char *) 0xF80D;                // ������ ��������� �� ����� � ������ � ����� ��������� ��������
                        if (*ptrTypeTrans == 1)                                 // ���� ������ -��
                        {
                          if (Mm > 1) Mm--; // | ��������� �� ��������� ����
                          else Mm = 15;     //
                        }
                        else
                        {
                          if (Mm > 1) Mm--; // | ��������� �� ��������� ����
                          else Mm = 14;     //
                        }
                        // if (Mm > 1) Mm--; // | ��������� �� ��������� ����
                        // else Mm = 15;     //
                      }
                     else
                      {
                      if (Mm == 1)    // ������� ���������
            {
                        #if defined (_EDIZMER_) // ��������� ����, ������ ��� �������� � ��� �� ���������
						if (tmp_edinica_izmer == 0x0C) tmp_edinica_izmer = 0x08;
            			#else
                        if (tmp_edinica_izmer == 0x0C) tmp_edinica_izmer = 0x0B;
                        #endif
                        else
                          {
                          if (tmp_edinica_izmer == 0xED) tmp_edinica_izmer = 0x0C;
                          else
                            {
                            if (tmp_edinica_izmer == 0x05) tmp_edinica_izmer = 0xED;
                            else
                              {
                              if (tmp_edinica_izmer == 0xEF) tmp_edinica_izmer = 0x05;
                              else
                                {
                                if (tmp_edinica_izmer == 0x0A) tmp_edinica_izmer = 0xEF;
                                else
                                  {
                                  if (tmp_edinica_izmer == 0xB0) tmp_edinica_izmer = 0x0A;
                                  else
                                    {
                                    if (tmp_edinica_izmer == 0x07) tmp_edinica_izmer = 0xB0;
                                    else
                                      {
                                      if (tmp_edinica_izmer == 0x0B) tmp_edinica_izmer = 0x07;
                                      else
                                      	#if defined (_EDIZMER_) // ��������� ����, ������ ��� �������� � ��� �� ���������
	                                    {
	                                      if (tmp_edinica_izmer == 0x08) tmp_edinica_izmer = 0x0B;
	                                      else tmp_edinica_izmer = 0x0C;
	                                    }
				            			#else
				                        tmp_edinica_izmer = 0x0C;
				                        #endif
                                      
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      if (Mm == 2 || Mm == 3 || Mm == 11 || Mm == 15)    // ��� + ��� + ��������� �� ���� + ��������� ���� ������ �������� ����������� ��������
            {
                        if (cursor_position == 5) cursor_position = 4;        // ���������� ������ ������ ������
                        else
                          if (cursor_position == 4) cursor_position = 3;
                          else
                            if (cursor_position == 3) cursor_position = 2;
                            else
                              if (cursor_position == 2) cursor_position = 1;
                              else cursor_position = 5;
                        }
          if (Mm == 4)    // ����� ��������������
            {
                        if (tmp_damping_value >= 20.0) tmp_damping_value = 10.0;
                        else
                        {
            if (tmp_damping_value >= 10.0) tmp_damping_value = 5.0;
                        else
                          {
                          if (tmp_damping_value >= 5.0) tmp_damping_value = 2.0;
                          else
                            {
                            if (tmp_damping_value >= 2.0) tmp_damping_value = 1.0;
                            else
                              {
                              if (tmp_damping_value >= 1.0) tmp_damping_value = 0.5;
                              else
                                {
                                if (tmp_damping_value >= 0.5) tmp_damping_value = 0.2;
                                else
                                  {
                                  if (tmp_damping_value >= 0.2) tmp_damping_value = 0.1;
                                  else
                                    {
                                    if (tmp_damping_value >= 0.1) tmp_damping_value = 0.05;
                                    else
                                    tmp_damping_value = 0.05;
                                    }
                                  }
                                }
                              }
                            }
                          }
            }
                        }
                     if (Mm == 5)     // ���������������� �����������
          {
          tmp_koren ^= 0x01;
          }
         if (Mm == 6)     // ������ / �������� ����������� ��������� �������
          {
          tmp_tok_signal_inv ^= 0x01;
          }
         if (Mm == 7)     // ������� ����� �������
          {
                      if (tmp_ID > 0) tmp_ID -=1;
                      else tmp_ID = 15;
          }
                     if (Mm == 8)     // ���������� ������ ���������� ������ ��������� ����
          {
                      tmp_magnit_key_enable ^= 0x01;
          }
                     if (Mm == 9)     // ��������� ����
          {
                      // ��� ������ ��� ����� ���� �� ������������
                      }
                     if (Mm == 10)    // ���������� �� ��������
          {
                      // ��� ������ ��� ����� ���� �� ������������
                      }
                   /*  if (Mm == 11)    // ���������� �� �������� ������
          {

                      } */
                     if (Mm == 12)
          {
                      if (DEV_ID == 0)// ���� ���������� ����� 0 ����� - ����� ����� ���������� � ����� �������������� ����
                        {
                        if (flag_i_out_auto != 0)
                          {
                          flag_i_out_auto = 0;    // ������ � ����� �������������� ����
                          loop_current = 22.500;
                          }
                        else
                          {
                          if (loop_current >= 22.500) loop_current = 20.000;
                          else if (loop_current >= 20.000) loop_current = 16.000;
                            else if (loop_current >= 16.000) loop_current = 12.000;
                              else if (loop_current >= 12.000) loop_current = 8.000;
                                else if (loop_current >= 8.000) loop_current = 4.000;
                                  else if (loop_current >= 4.000) loop_current = 3.8001;
                                    else flag_i_out_auto = 1;  // ����� �� ������ �������������� ����
                          }
                        OUT_current (); // ������������� ����� �������� ����.
                        }
                      }
                     if (Mm == 13)    // ������ ��������� ������������ �� HART ���������
          {
                      tmp_HART_disable ^= 0x01;
                      }
                     if (Mm == 14)    // ��������� �������� ���� ��� ��������� ������ (Alarm)
                      {
                      tmp_current_alarm ^= 0x01;
                      }
                     }
                     znach_print ();
                     KEY_PUSH_FLAG = 1;
         break;
         }
default: break;
  }
  }
if (menu_count >= 250)                // ���� � ������� 25 ���. ������ �� �������� - ����� �� ���� ��� ���������� ��������� ���������
  {
  if (Mm == 12 && tmp_flag_i_out_auto == 1) // ���� � ������ �������������� ����, �� ��� ������ �� ������� ������
    {
    flag_i_out_auto = 1;              // ����� �� ������ �������������� ���� ��� ������� ��� �� ����� � ���� �� ���� ������ �������������� ����
    }
  Mm = 0; Vm = 0; IND_DIMM |=0x80; IND_CURSOR = 0;
  }
}

//************************************
//** �/� ������ �������� ���� � ��  **
//************* �������� *************
//************************************
void znach_print (void)
{
if (flag_error_menu)
  {
  ind_razr_1 = ((1<<a7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7));     // E
  ind_razr_2 = ((1<<e7)|(1<<g7));                             // r
  ind_razr_3 = ((1<<e7)|(1<<g7));                             // r
  ind_razr_4 = (1<<g7);                                       // -
  ind_razr_5 = convert (error_number_menu);                   // ����� ������
  ind_razr_6 = 0x00;

  cancel_edit = 1;                    // ��� ������� ��������� ��������� � ������������� ������ �� ������
  }                                   // ��������������� �������������� �������� �� ��������� ��������� ��� ����������� ����������� ������� �������� ��� ��������� ����� � ����� ����
else
{
if (Vm != 0)
{
switch (Mm)
{
case 1 :                              // ������� ���������
      {
      ind_razr_1 = 0x00;
      ind_razr_2 = 0x00;
      ind_razr_3 = 0x00;

      if (tmp_edinica_izmer == 0x0C) menu_temp = 0; // ���
      if (tmp_edinica_izmer == 0xED) menu_temp = 1; // ���
      if (tmp_edinica_izmer == 0x05) menu_temp = 2; // ��.��.��.
      if (tmp_edinica_izmer == 0xEF) menu_temp = 3; // ��.���.��.
      if (tmp_edinica_izmer == 0x0A) menu_temp = 4; // ���/��2
      if (tmp_edinica_izmer == 0xB0) menu_temp = 5; // ���/�2
      if (tmp_edinica_izmer == 0x07) menu_temp = 6; // ���
      if (tmp_edinica_izmer == 0x0B) menu_temp = 7; // ��
      if (tmp_edinica_izmer == 0x08) menu_temp = 8; // ����

      ind_razr_4 = convert (menu_temp);
      ind_razr_5 = 0x00;
      ind_razr_6 = 0x00;
      break;
      }                               //
case 2 :                              // ������� ������
      {
      ind_razr_1 = convert (r1);
      if (mp == 1) ind_razr_1 |= (1<<h7);
      ind_razr_2 = convert (r2);
      if (mp == 2) ind_razr_2 |= (1<<h7);
      ind_razr_3 = convert (r3);
      if (mp == 3) ind_razr_3 |= (1<<h7);
      ind_razr_4 = convert (r4);
      if (mp == 4) ind_razr_4 |= (1<<h7);
      ind_razr_5 = convert (r5);
      ind_razr_6 = 0x00;
      break;
      }                               //
case 3 :                              // ������ ������
      {
      ind_razr_1 = convert (r1);
      if (mp == 1) ind_razr_1 |= (1<<h7);
      ind_razr_2 = convert (r2);
      if (mp == 2) ind_razr_2 |= (1<<h7);
      ind_razr_3 = convert (r3);
      if (mp == 3) ind_razr_3 |= (1<<h7);
      ind_razr_4 = convert (r4);
      if (mp == 4) ind_razr_4 |= (1<<h7);
      ind_razr_5 = convert (r5);
      ind_razr_6 = 0x00;
      break;
      }
case 4 :                              // ����� ��������������
      {
      f = tmp_damping_value;
      davlenie_vivod_na_indicator ();
      break;
      }
case 5 :                              // ���������������� �����������
      {
      ind_razr_1 = 0x00;
      if (tmp_koren)
        {
        ind_razr_2 = ((1<<a7)|(1<<c7)|(1<<d7)|(1<<d7)|(1<<f7)|(1<<g7)); // S
        ind_razr_3 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<f7)|(1<<g7));         // q
        ind_razr_4 = ((1<<e7)|(1<<g7));                                 // r
        }
      else
        {
        ind_razr_2 = ((1<<f7)|(1<<e7)|(1<<d7)); // L
        ind_razr_3 = (1<<c7);                   // i
        ind_razr_4 = ((1<<e7)|(1<<g7)|(1<<c7)); // n
        }
      ind_razr_5 = 0x00;
      ind_razr_6 = 0x00;
      break;
      }
case 6 :                              // ������ / �������� ����������� ��������� �������
      {
      ind_razr_1 = 0x00;
      if (tmp_tok_signal_inv)
        {
        ind_razr_2 = ((1<<a7)|(1<<b7)|(1<<g7)|(1<<e7)|(1<<d7));         //2
        ind_razr_3 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)); //0
        ind_razr_4 = (1<<g7);                                           //-
        ind_razr_5 = ((1<<b7)|(1<<c7)|(1<<f7)|(1<<g7));                 //4
        }
      else
        {
        ind_razr_2 = ((1<<b7)|(1<<c7)|(1<<f7)|(1<<g7));                 //4
        ind_razr_3 = (1<<g7);                                           //-
        ind_razr_4 = ((1<<a7)|(1<<b7)|(1<<g7)|(1<<e7)|(1<<d7));         //2
        ind_razr_5 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7)); //0
        }
      ind_razr_6 = 0x00;
      break;
      }
case 7 :                              // ������� ����� �������
      {
      ind_razr_1 = 0x00;
      ind_razr_2 = 0x00;

      if (tmp_ID / 10)  ind_razr_3 = convert (tmp_ID / 10);
      else ind_razr_3 = 0x00;
      ind_razr_4 = convert (tmp_ID - ((tmp_ID/10) * 10));

      ind_razr_5 = 0x00;
      ind_razr_6 = 0x00;
      break;
      }
case 8 :                              // ���������� ������ ���������� ������ ��������� ����
      {
      ind_razr_1 = 0x00;
      if (tmp_magnit_key_enable)
        {
        ind_razr_2 = 0x00;
        ind_razr_3 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7));   //0
        ind_razr_4 = ((1<<c7)|(1<<e7)|(1<<g7));         // n
        }
      else
        {
        ind_razr_2 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7));   //0
        ind_razr_3 = ((1<<a7)|(1<<e7)|(1<<f7)|(1<<g7)); // F
        ind_razr_4 = ((1<<a7)|(1<<e7)|(1<<f7)|(1<<g7)); // F
        }
      ind_razr_5 = 0x00;
      ind_razr_6 = 0x00;
      break;
      }
case 9 :                              // ��������� ����
      {
      f = davlenie;
      davlenie_vivod_na_indicator ();
      ind_razr_6 = 0x00;
      break;
      }
case 10:                              // ���������� �� ��������
      {
      if (num_menu_ch == 0)           // ��������� ��������� ����� ������� ���������� � ������ �����
        {
        ind_razr_1 = ((1<<d7)|(1<<e7)|(1<<f7));                 // L
        ind_razr_2 = ((1<<c7)|(1<<d7)|(1<<e7)|(1<<g7));         // o
        ind_razr_3 = 0x00;                                      //
        ind_razr_4 = ((1<<a7)|(1<<b7)|(1<<e7)|(1<<f7)|(1<<g7)); // P
        ind_razr_5 = ((1<<e7)|(1<<g7));                         // r
        }
      else if (num_menu_ch == 2)      // ��������� ��������� ����� ������� ���������� �� ������ �����
        {
        ind_razr_1 = ((1<<b7)|(1<<c7)|(1<<e7)|(1<<f7)|(1<<g7)); // H
        ind_razr_2 = (1<<c7);                                   // i
        ind_razr_3 = 0x00;                                      //
        ind_razr_4 = ((1<<a7)|(1<<b7)|(1<<e7)|(1<<f7)|(1<<g7)); // P
        ind_razr_5 = ((1<<e7)|(1<<g7));                         // r
        }
      else if (num_menu_ch == 4)
        {
        ind_razr_1 = 0x00;                                      //
        ind_razr_2 = ((1<<b7)|(1<<c7)|(1<<e7)|(1<<d7)|(1<<g7)); // d
        ind_razr_3 = ((1<<c7)|(1<<d7)|(1<<e7)|(1<<g7));         // o
        ind_razr_4 = ((1<<c7)|(1<<e7)|(1<<g7));                 // n
        ind_razr_5 = ((1<<a7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7)); // E
        }
      else                            // ����� �� ��������� �������� ����������� ��������
        {
        f = davlenie;
        davlenie_vivod_na_indicator ();
        }
      ind_razr_6 = 0x00;
      break;
      }
case 11:                              // ���������� �������� ������
      {
      if (num_menu_ch == 0)           // ��������� ��������� ����� ������� ���������� � ������ �����
        {
        ind_razr_1 = ((1<<d7)|(1<<e7)|(1<<f7));                 // L
        ind_razr_2 = ((1<<c7)|(1<<d7)|(1<<e7)|(1<<g7));         // o
        ind_razr_3 = ((1<<a7)|(1<<d7)|(1<<e7)|(1<<f7));         // C
        ind_razr_4 = ((1<<c7)|(1<<d7)|(1<<e7));                 // u
        ind_razr_5 = ((1<<e7)|(1<<g7));                         // r
        }
      else if (num_menu_ch == 2)      // ��������� ��������� ����� ������� ���������� �� ������ �����
        {
        ind_razr_1 = ((1<<b7)|(1<<c7)|(1<<e7)|(1<<f7)|(1<<g7)); // H
        ind_razr_2 = (1<<c7);                                   // i
        ind_razr_3 = ((1<<a7)|(1<<d7)|(1<<e7)|(1<<f7));         // C
        ind_razr_4 = ((1<<c7)|(1<<d7)|(1<<e7));                 // u
        ind_razr_5 = ((1<<e7)|(1<<g7));                         // r
        }
      else if (num_menu_ch == 4)
        {
        ind_razr_1 = 0x00;                                      //
        ind_razr_2 = ((1<<b7)|(1<<c7)|(1<<e7)|(1<<d7)|(1<<g7)); // d
        ind_razr_3 = ((1<<c7)|(1<<d7)|(1<<e7)|(1<<g7));         // o
        ind_razr_4 = ((1<<c7)|(1<<e7)|(1<<g7));                 // n
        ind_razr_5 = ((1<<a7)|(1<<d7)|(1<<e7)|(1<<f7)|(1<<g7)); // E
        }
      else                            // ����� �� ��������� �������� ������� ������ ����������� ����
        {
        ind_razr_1 = convert (r1);
        if (mp == 1) ind_razr_1 |= (1<<h7);
        ind_razr_2 = convert (r2);
        if (mp == 2) ind_razr_2 |= (1<<h7);
        ind_razr_3 = convert (r3);
        if (mp == 3) ind_razr_3 |= (1<<h7);
        ind_razr_4 = convert (r4);
        if (mp == 4) ind_razr_4 |= (1<<h7);
        ind_razr_5 = convert (r5);
        }
      ind_razr_6 = 0x00;
      break;
      }
case 12:                              // ����� �������������� ����
      {
      if (flag_i_out_auto != 0)       // ���� �� � ������ �������������� ����
        {
        ind_razr_1 = 0x00;
        ind_razr_2 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7));   // 0
        ind_razr_3 = ((1<<a7)|(1<<e7)|(1<<f7)|(1<<g7));                   // F
        ind_razr_4 = ((1<<a7)|(1<<e7)|(1<<f7)|(1<<g7));                   // F
        ind_razr_5 = 0x00;
        ind_razr_6 = 0x00;
        }
      else
        {
        f = loop_current;             // ����� �� ��������� �������� �������������� ����
        curent_print ();
        }
      break;
      }
case 13:                              // ��������� ������������ �� HART ���������
      {
      ind_razr_1 = 0x00;
      if (tmp_HART_disable)
        {
        ind_razr_2 = 0x00;
        ind_razr_3 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7));   // 0
        ind_razr_4 = ((1<<c7)|(1<<e7)|(1<<g7));                           // n
        }
      else
        {
        ind_razr_2 = ((1<<a7)|(1<<b7)|(1<<c7)|(1<<d7)|(1<<e7)|(1<<f7));   // 0
        ind_razr_3 = ((1<<a7)|(1<<e7)|(1<<f7)|(1<<g7));                   // F
        ind_razr_4 = ((1<<a7)|(1<<e7)|(1<<f7)|(1<<g7));                   // F
        }
      ind_razr_5 = 0x00;
      ind_razr_6 = 0x00;
      break;
      }
case 14:
      {
      ind_razr_1 = 0x00;
      ind_razr_2 = 0x00;
      if (tmp_current_alarm)
        {
        ind_razr_3 = ((1<<b7)|(1<<c7)|(1<<g7)|(1<<e7)|(1<<f7));           // Hi
        ind_razr_4 = (1<<c7);                                             //
        }
      else
        {
        ind_razr_3 = ((1<<d7)|(1<<e7)|(1<<f7));                           // Lo
        ind_razr_4 = ((1<<c7)|(1<<d7)|(1<<e7)|(1<<g7));                   //
        }
      ind_razr_5 = 0x00;
      ind_razr_6 = 0x00;
      break;
      }
case 15:                            // ����� �� ��������� �������� ������������ �������� ��� ������������� ����
      {
      ind_razr_1 = convert (r1);
      if (mp == 1) ind_razr_1 |= (1<<h7);
      ind_razr_2 = convert (r2);
      if (mp == 2) ind_razr_2 |= (1<<h7);
      ind_razr_3 = convert (r3);
      if (mp == 3) ind_razr_3 |= (1<<h7);
      ind_razr_4 = convert (r4);
      if (mp == 4) ind_razr_4 |= (1<<h7);
      ind_razr_5 = convert (r5);
      ind_razr_6 = 0x00;
      break;
      }
default : break;
}
}
else
  {
  ind_razr_1 = ((1<<a7)|(1<<e7)|(1<<f7)|(1<<g7)); // F
  ind_razr_2 = ((1<<c7)|(1<<d7)|(1<<e7));         // u
  ind_razr_3 = ((1<<c7)|(1<<g7)|(1<<e7));         // n
  if (Mm / 10)  ind_razr_4 = convert (Mm / 10);
  else ind_razr_4 = 0x00;
  ind_razr_5 = convert (Mm - ((Mm/10) * 10));
  ind_razr_6 = 0x00;
  }
}
}
//************************************
//** �/� �������� ��������� � ********
//** ���������� ����������� ��������**
//************************************
void save (unsigned char _num)
{
if (P2IN & 0x80)                      // ��������� ���������� ������������� ���������� ��������� ������������
{
switch (Mm)
  {
  case 1:
    {
    if (tmp_edinica_izmer != edinica_izmer)
      {
      edinica_izmer = tmp_edinica_izmer;
      flag_conf_changed = 1;			// ���� - ������������ ������� ��������
      EEPROM_write_conf ();           // ��������� ����� ������� ��������� � EEPROM
      }
    break;
    }

  case 2:
    {
    tmp_diapazon_v = perevod_v_uslovnie_edinici (preobrazovanie_v_vehestvenoe(), edinica_izmer); // �������� ����������� � �������� �������� ��������� �������� ��� � ���������
    if (tmp_diapazon_v <= 1.0 && (tmp_diapazon_v >= diapazon_n_PD - 0.001) && (tmp_diapazon_v - tmp_diapazon_n >= min_diapazon_PD - 0.001))         // ���� ��������� �������� ���������, �� ��������� ��������
      {
      if (tmp_diapazon_v != diapazon_v)
        {
        diapazon_v = tmp_diapazon_v;
        flag_conf_changed = 1;			// ���� - ������������ ������� ��������
        EEPROM_write_conf ();         	// ��������� ����� ��� � EEPROM
        }
      }
    else                              	// ���� �������� ������� �� ����������, �������� �� ������
      {
      flag_error_menu = 1;
      error_number_menu = 0;          	//
      cancel_edit = 1;                	// ������������ ��������� ��� �������������� tmp_diapazon_v
      }
    break;
    }

  case 3:
    {
    tmp_diapazon_n = perevod_v_uslovnie_edinici (preobrazovanie_v_vehestvenoe(), edinica_izmer); // �������� ����������� � �������� �������� ��������� �������� ��� � ���������
    if (tmp_diapazon_n <= 1.0 && (tmp_diapazon_n >= diapazon_n_PD - 0.001) && (tmp_diapazon_v - tmp_diapazon_n >= min_diapazon_PD - 0.001))         // ���� ��������� �������� ���������, �� ��������� ��������
      {
      if (tmp_diapazon_n != diapazon_n)
        {
        diapazon_n = tmp_diapazon_n;
        flag_conf_changed = 1;			// ���� - ������������ ������� ��������
        EEPROM_write_conf ();         	// ��������� ����� ��� � EEPROM
        }
      }
    else
      {
      flag_error_menu = 1;
      error_number_menu = 0;          //
      cancel_edit = 1;                // ������������ ��������� ��� �������������� tmp_diapazon_n
      }
    break;
    }

  case 4:
    {
    if (tmp_damping_value != damping_value)
      {
      damping_value = tmp_damping_value;
      N_integr = (unsigned char) (damping_value / 0.0786) + 1;
      N_integr_tmp = 1;
      flag_conf_changed = 1;			// ���� - ������������ ������� ��������
      EEPROM_write_conf ();           	// ��������� ����� �������� ������� ������������� � EEPROM
      }
    break;
    }

  case 5:
    {
    if (tmp_koren != koren)
      {
      koren = tmp_koren;
      flag_conf_changed = 1;			// ���� - ������������ ������� ��������
      EEPROM_write_conf ();           	// ��������� ����� �������� ������� ��������� ������� � EEPROM
      }
    break;
    }

  case 6:
    {
    if (tmp_tok_signal_inv != tok_signal_inv)
      {
      tok_signal_inv = tmp_tok_signal_inv;
      flag_conf_changed = 1;			// ���� - ������������ ������� ��������
      EEPROM_write_conf ();           	// ��������� ����� �������� ������� ��������� ������� � EEPROM
      }
    break;
    }

  case 7:
    {
    if (tmp_ID != DEV_ID)
      {
      DEV_ID = tmp_ID;
      flag_conf_changed = 1;		  // ���� - ������������ ������� ��������
      EEPROM_write_conf ();           // ��������� ����� ����� ���������� � EEPROM
      if (DEV_ID != 0x00)             // ���� ����� ���������� �� ������� - ������ ������� ����� ����������
        {
        flag_i_out_auto = 0;          // ������ � ����� �������������� ����
        loop_current = 4.000;
        OUT_current ();               // ������������� �������� ��������� ���� ������ 4 ��
        }
      else
        {
        flag_i_out_auto = 1;          // ��������� ������ �������� ������
        }
      }
    break;
    }

  case 8:
    {
    if (tmp_magnit_key_enable != magnit_key_enable)
      {
      magnit_key_enable = tmp_magnit_key_enable;
      flag_conf_changed = 1;			// ���� - ������������ ������� ��������
      EEPROM_write_conf ();           	// ��������� ����� �������� ��������� ������ ��������� ������ � EEPROM
      }
    break;
    }

  case 9:                             // ��������� ����
    {
    if (proverka_delta_P((delta_P + p), 0) == 0) // ������ �������� ������� �������� ���� �� ��������� +-5% �� ���������
      {
      delta_P += p;                   // ������������ �������� �������� ����
      flag_conf_changed = 1;		  // ���� - ������������ ������� ��������
      EEPROM_write_conf ();           // ��������� ����� �������� ��������� ����
      }
    else                              // ���� �������� �������� ���� ������� �� ������ - ������� ��������� �� ������
      {
      flag_error_menu = 1;
      error_number_menu = 3;          //
      }
    break;
    }

  case 10:                            // ����� ���������� �� ��������
    {
    if (num_menu_ch == 1)             // ������ ����. ��������
      {
      pressure_low_input = p;         // ��������� �������� �������� ��� ��������� ��� ����������� ������� ������������� ����������
      if (pressure_low_input > (diapazon_n - 0.02) && pressure_low_input < (diapazon_n + 0.02)) // ������ �������� �� ���������� �������� ���������� �������� (������ +-2%)
        {
        // ��� ������ ������ ������ �� �����. ������ � ���������� ������������� ������������� �������������� ��� ���������� ����� ���
        }
      else                            // ���� ��������� �������� ��� ����������� ��������� - ������� ��������� �� ������ � ��������� ������� ����������
        {                             // � ������� � �������� ���� ��� ���������� ����������
        flag_error_menu = 1;
        error_number_menu = 4;
        }
      }
    else                              // ������ ����. �������
      {
      pressure_hi_input = p;          // ��������� �������� ���
      if (pressure_hi_input > (diapazon_v - 0.02) && pressure_hi_input < (diapazon_v + 0.02)) // ��������� �� ���������� �������� ��������. ���� ��������� �������� ��������� - ���������� ������ ������������� ���������� �� ��������
        {
        k_pressure = k_pressure * ((diapazon_v - diapazon_n) / (pressure_hi_input - pressure_low_input));  // ����������� ��������
        b_pressure = b_pressure + (diapazon_n - pressure_low_input); // ����������� ��������

        EEPROM_write_pressure_cal();  // ��������� ����� ������������� ������������ � EEPROM
        flag_message_menu = 1;        // ������� �������������� ��������� �� �������� ����������
        num_menu_ch = 4;              //
        }
      else                            // ���� ��������� �������� ��� ����������� ��������� - ������� ��������� �� ������ � ��������� ������� ����������
        {                             // � ������� � �������� ���� ��� ���������� ����������
        flag_error_menu = 1;
        error_number_menu = 4;
        }
      }
    break;
    }

  case 11:                            // ����� ���������� �������� ������
    {
    if (num_menu_ch == 1)             // ������ ����. ��������
      {
      tok_low_input = preobrazovanie_v_vehestvenoe(); // ��������� �������� ���� ��������� ��� ����������� ������� ������������� ����������
      if (tok_low_input > 3.5 && tok_low_input < 4.5) // ������ �������� �� ���������� �������� ���������� ��������
        {
        // ��� ������ ������ ������ �� �����. ������ � ���������� ������������� ������������� �������������� ��� ���������� ����� 20 ��
        }
      else                            // ���� ��������� �������� ��� ����������� ��������� - ������� ��������� �� ������ � ��������� ������� ����������
        {                             // � ������� � �������� ���� ��� ���������� ����������
        flag_error_menu = 1;
        error_number_menu = 4;
        }
      }
    else                              // ������ ����. �������
      {
      tok_hi_input = preobrazovanie_v_vehestvenoe(); // ��������� �������� ����
      if (tok_hi_input > 19.5 && tok_hi_input < 20.5) // ��������� �� ���������� �������� ��������. ���� ��������� �������� ��������� - ���������� ������ ������������� ���������� ���
        {
        k_current = (tok_hi_input - tok_low_input) / ((long)((16.) / k_current)); // �������� ����� ����������� �������� ���
        b_current = b_current + (int)((4.0 - tok_low_input) / k_current);         // � ����������� ��������
        flag_conf_changed = 1;			  						// ���� - ������������ ������� ��������
        EEPROM_write_conf ();         // ��������� ����� �������� ������������ � EEPROM
        flag_message_menu = 1;        // ������� �������������� ��������� �� �������� ����������
        num_menu_ch = 4;              //
        }
      else                            // ���� ��������� �������� ��� ����������� ��������� - ������� ��������� �� ������ � ��������� ������� ����������
        {                             // � ������� � �������� ���� ��� ���������� ����������
        flag_error_menu = 1;
        error_number_menu = 4;
        }
      }
    break;
    }

  case 12:                            // ����� �������������� ����
    {
//  ��� ������ �� ����� ���������.
    break;
    }

  case 13:
    {
    if (tmp_HART_disable != HART_disable)
      {
      HART_disable = tmp_HART_disable;
      flag_conf_changed = 1;			  						// ���� - ������������ ������� ��������
      EEPROM_write_conf ();           // ��������� ����� ��������� ���������� ������������ �� HART ��������� � EEPROM
      }
    break;
    }

  case 14:
    {
    if (tmp_current_alarm != current_alarm)
      {
      current_alarm = tmp_current_alarm;
      flag_conf_changed = 1;			  						// ���� - ������������ ������� ��������
      EEPROM_write_conf ();           // ��������� ����� ��������� ���������� ������������ �� HART ��������� � EEPROM
      }
    break;
    }
  case 15:                            // ��������� ���� ������ �������� ������������ ��������
    {
    atmosphera = preobrazovanie_v_vehestvenoe();                // ��������� �������� ������������ ��������
    if (atmosphera > 85.44 && atmosphera < 108.7)               // ������ �������� �� ���������� �������� ���������� ��������
      {
      atmosphera = perevod_v_uslovnie_edinici (atmosphera, 0x0C);
      if (proverka_delta_P((delta_P + (p - atmosphera)), 1) == 0)    		// ������ �������� �������� ����
        {
        delta_P += p - atmosphera;      // ������������ �������� �������� ����
        flag_conf_changed = 1;			  						// ���� - ������������ ������� ��������
        EEPROM_write_conf ();           // ��������� ����� �������� ��������� ����
        }
      else
        {
        flag_error_menu = 1;
        error_number_menu = 3;          //
        }
      }
    else
      {
      flag_error_menu = 1;
      error_number_menu = 3;            //
      }
    break;
    }
  default:break;
  }
}
else                                  // ���� ����� ��������� ������������ ���������, �� ������� ��������� �� ������
  {
  flag_error_menu = 1;
  error_number_menu = 8;
  }
}

//************************************
//* �/� ������� ��������� ���������� ��� HART ��������� *
//************************************
#pragma vector=TIMERB0_VECTOR
__interrupt void Timer_B0 (void)
{
count_time_HART += 1;
if (count_time_HART == time_8_HART) { CDR_END_HART = 1;}
if (count_time_HART == 255) count_time_HART = 254;
}

//************************************
//* �/� ���������� ����� ���� float **
//************************************
float okruglenie (float abc, unsigned char num)
{
volatile float znc;
volatile long y;
abc = abc * 10 * num + 0.5;
y = (long) abc;
znc = (float) y / (10 * num);
return znc;
}

//************************************
//* �/� ������ �� ��������� �������� ����*
//************************************
void curent_print (void)
{
menu_temp = (unsigned char)(f/10);
if (menu_temp) ind_razr_1 = convert (menu_temp);
else ind_razr_1 = 0x00;
f = f - menu_temp * 10;

menu_temp = (unsigned char)(f/1);
ind_razr_2 = convert (menu_temp) | (1<<h7);
f = f - menu_temp * 1;

f = f * 10;
menu_temp = (unsigned char)(f/1);
ind_razr_3 = convert (menu_temp);
f = f - menu_temp * 1;

f = f * 10;
menu_temp = (unsigned char)(f/1);
ind_razr_4 = convert (menu_temp);
f = f - menu_temp * 1;

f = f * 10;
menu_temp = (unsigned char)(f/1);
ind_razr_5 = convert (menu_temp);
f = f - menu_temp * 1;

ind_razr_6 = 0x00;
}

//************************************
//*�/� �������� � ������� ��. ���������
//************************************
float perevod_v_davlenie (float znach)
{
switch (edinica_izmer)
  {
  case 0x0C: {znach = znach * mastb;                break;} // ���
  case 0xED: {znach = znach * mastb * 0.00100000;   break;} // ���
  case 0x05: {znach = znach * mastb * 7.50060000;   break;} // ��. ��. ��.
  case 0xEF: {znach = znach * mastb * 101.972000;   break;} // ��. ���. ��.
  case 0x0A: {znach = znach * mastb * 0.01019720;   break;} // ���/��2
  case 0xB0: {znach = znach * mastb * 101.972000;   break;} // ���/�2
  case 0x07: {znach = znach * mastb * 0.01000000;   break;} // ���
  case 0x08: {znach = znach * mastb * 10.0000000;   break;} // ����
  case 0x0B: {znach = znach * mastb * 1000;         break;} // ��
  default: break;
  }
return znach;
}

//************************************
//*�/� �������� � ������� ��. ���������
//************************************
float perevod_v_uslovnie_edinici (float znach, char edinica)
{
switch (edinica)
  {
  case 0x0C: {znach = znach / (mastb * 1.);           break;} // ���
  case 0xED: {znach = znach / (mastb * 0.00100000);   break;} // ���
  case 0x05: {znach = znach / (mastb * 7.50060000);   break;} // ��. ��. ��.
  case 0xEF: {znach = znach / (mastb * 101.972000);   break;} // ��. ���. ��.
  case 0x0A: {znach = znach / (mastb * 0.01019720);   break;} // ���/��2
  case 0xB0: {znach = znach / (mastb * 101.972000);   break;} // ���/�2
  case 0x07: {znach = znach / (mastb * 0.01000000);   break;} // ���
  case 0x08: {znach = znach * mastb * 10.0000000;  	  break;} // ����
  case 0x0B: {znach = znach / (mastb * 1000);         break;} // ��
  default: break;
  }
return znach;
}

//==========================================================================================
// �/� ��� �������� ������������ ��������� ����
//==========================================================================================
char proverka_delta_P (float delt_P, unsigned char par)										// par - ��������, 0 - ��������� � ������(����)/�������, 1 - ��������� ����� ����� ���
{
	if (delt_P <= 0.05)                                                                     //
	{
		if (delt_P >= -0.05)                                                                //
		{
			if (!par) // 																	// ��������� � ������/�������
			{
				unsigned char *ptrTypeTrans;												// ��������� ��������� �� ��� ��������� �������� � ������
				ptrTypeTrans = (unsigned char *) 0xF80D;									// ������ ��������� �� ����� � ������ � ����� ��������� ��������
				if (*ptrTypeTrans == 1)														// ���� ������ -��
				{
					if(p < perevod_v_uslovnie_edinici (80.0, 0x0C))
						return 0;                                                           // ���������� 0 ���� ������� �������� ���� �� ��������� +-5% �� ���������
					else
						return 1;                                                           // ���������� 1 ���� ������� �������� > 80 ���
				}
				else
					return 0;                                                               // ���������� 0 ���� ������� �������� ���� �� ��������� +-5% �� ���������
			}
			else																			// ��������� ����� ����� ���������
			{
				return 0;                                                                   // ���������� 0 ���� ������� �������� ���� �� ��������� +-5% �� ���������
			}
		}
		else
		{
			return 2;                                                                       // ���������� 2 ���� �������� ������ -5%
		}
	}
	else return 1;                                                                          // ���������� 1 ���� �������� ������ 5%
}

//************************************
//******** �/� ������� CRC16 *********
//***** �������� �������� 0xA001 *****
//************************************
void CRC16 (unsigned char *nach, unsigned char dlina)
{
unsigned char j,flag_CRC;
unsigned int temp_CRC;
temp_CRC = 0xFFFF;
while(dlina--)
  {
  temp_CRC ^= *nach++;
  for(j=0; j<8; j++)
    {
    flag_CRC = (unsigned char)(temp_CRC & 0x0001);
    temp_CRC = temp_CRC >> 1;
    if (flag_CRC)
    temp_CRC ^= 0xA001;
    }
  }
crc_H= (unsigned char)(temp_CRC & 0x00FF);
crc_L= (unsigned char)((temp_CRC & 0xFF00)>>8);
}

//************************************
//**�/� ������� ���-�� ������ �� ������� ��� ������ �� ��������� �������� ��� � ���
//************************************
char raschet_n (void)
{
volatile float znachenie_p;
znachenie_p = perevod_v_davlenie (1.);// ����������� ������������ �������� �������� � ������� �������� ���������.

if (znachenie_p >= 100000) return 6;  // ���������� ����� ������ �� �������
if (znachenie_p >= 10000) return 5;   // ������� ����� ���������� �� ���������
if (znachenie_p >= 1000) return 4;    // ��� ��������� ��� � ��� ����� ����
if (znachenie_p >= 100) return 3;     //
if (znachenie_p >= 10) return 2;      //
return 1;                             //
}

//************************************
//** �/� �������� ����� ���� float � �������������
//************************************
void preobrazovanie_v_chelochislennoe (float znach, char m)
{
volatile unsigned char buf[9], number;
volatile unsigned char flag_minus = 0;

if (znach < 0)                        // ���� ����� �������������
  {
  znach = -znach;                     // ������� ���� ����� ��� �������������� � �������������
  flag_minus = 1;
  }

buf[0] = (unsigned long) znach/ 10000;// � ����� ���������� �������� ����� � ������� �����.���� ��� ���������� ���������
znach -= buf[0] * 10000;

buf[1] = (unsigned int) znach / 1000;
znach -= buf[1] * 1000;

buf[2] = (unsigned int) znach / 100;
znach -= buf[2] * 100;

buf[3] = (unsigned int) znach / 10;
znach -= buf[3] * 10;

buf[4] = (unsigned int) znach / 1;
znach -= buf[4] * 1;

znach = znach * 10000;                // ����������� ����� �� 4-�� ����� ����� �������

buf[5] = (unsigned long) znach / 1000;
znach -= buf[5] * 1000;

buf[6] = (unsigned int) znach / 100;
znach -= buf[6] * 100;

buf[7] = (unsigned int) znach / 10;
znach -= buf[7] * 10;

buf[8] = (unsigned int) znach / 1;
znach -= buf[8] * 1;

m = 5 - m;                            //

if (flag_minus) // ���� ����� �������������
  {
  r1 = 16;                            // ������� ���� �����
  m = m + 1;                          // ������������ ������ ������ �� ������ ��� ����������� ����������� ����� �� ��������� ������ ��� ������ ����� �����
  r2 = buf[m];                        // �������� ������� ����� ���������������� � ����.
  r3 = buf[m + 1];
  r4 = buf[m + 2];
  r5 = buf[m + 3];
  }
else            // ���� ����� �������������
  {
  r1 = buf[m];                        // � �������� ����� ������������ ������ ����� � �������� ������ �������� �� �������.
  r2 = buf[m + 1];                    // �������� ������� ����� ���������������� � ����.
  r3 = buf[m + 2];
  r4 = buf[m + 3];
  r5 = buf[m + 4];
  }
}

//************************************
//* �/� �������������� �������������� � ������������ ����� ���� float
//************************************
float preobrazovanie_v_vehestvenoe (void)
{
unsigned long celoe_chislo;
unsigned char mn;

if (r1 == 16) // ��� ������������� �����
  {
  celoe_chislo = r2 * 1000 + r3 * 100 + r4 * 10 + r5;     // � celoe_chislo ������������ �������� �� ������ �� ����� ������� ������������� � �������� � ����� ���������� �������� �����
//  if (mp == 1) vehestvennoe = celoe_chislo / 10000.;    // � ����������� �� ��������� ���������� ����� �� ����������, ���������� ��������
  if (mp == 2) return ( -(celoe_chislo / 1000.));         // �� ������� ���������� ��������� ������������� ����� ��� ��������� ������������� ����� � ��������� ������.
  if (mp == 3) return ( -(celoe_chislo / 100.));
  if (mp == 4) return ( -(celoe_chislo / 10.));
  if (mp == 5) return ( -(celoe_chislo / 1.));
  return 0;   // ��������������� ����� ���� float ���������� 0 � ����� ���������� ��������
  }
else          // ��� ������������� �����
  {
//  celoe_chislo = (r1 * 10000) + (r2 * 1000) + (r3 * 100) + (r4 * 10) + r5;  // � celoe_chislo ������������ �������� �� ������ �� ����� ������� ������������� � �������� � ����� ���������� �������� �����
  celoe_chislo = (r2 * 1000) + (r3 * 100) + (r4 * 10) + r5;  // � celoe_chislo ������������ �������� �� ������ �� ����� ������� ������������� � �������� � ����� ���������� �������� �����
  mn = r1;
  while (mn)                          // ������� ����� ��������� ������� ������������, �.�. ������� ��������� ���������� ������������ �����������
    {
    celoe_chislo += 10000;
    mn -= 1;
    }
  if (mp == 1) return (celoe_chislo / 10000.);      // � ����������� �� ��������� ���������� ����� �� ����������, ���������� ��������
  if (mp == 2) return (celoe_chislo / 1000.);       // �� ������� ���������� ��������� ������������� ����� ��� ��������� ������������� ����� � ��������� ������.
  if (mp == 3) return (celoe_chislo / 100.);
  if (mp == 4) return (celoe_chislo / 10.);
  if (mp == 5) return (celoe_chislo / 1.);
  return 0;   // ��������������� ����� ���� float ���������� 0 � ����� ���������� ��������
  }
}

//************************************
//* �/� ������ ������ ������ �� ��������� ��� ��������� ��������� � ��������� ���������� ������� � ������� �����
//************************************
void critical_error (void)
{
if (DEV_ID == 0)
  {
  flag_i_out_auto = 0;                // ������ � ����� �������������� ����

  if (current_alarm)  loop_current = 22.500; //  � ����������� �� �������� ��������� ����� ��� �������� ��� ����������� ����������� ������
  else                loop_current = 3.800;  //

  OUT_current ();                     // ������������� ��������� �������� ����.
  }
}

// Watchdog Timer interrupt service routine
#pragma vector=WDT_VECTOR
__interrupt void watchdog_timer(void)
{
P2OUT ^= 0x04;                        // ������ �� ������ P2.2
}
