//************************************
//****** ���������� � ��������� ******
//************************************
#define N_Resive    58                // ������ ���������� ������ ���������
#define N_Transmit  58                // ������ ������ �����������
#define N_comnd     38                // ������ ������ ��� ������� �������� �������

unsigned char Rx_bufer[N_Resive];     //
unsigned char Tx_bufer[N_Transmit];   //
unsigned char Command[N_comnd];       //

unsigned char *Rx_read,*Rx_write;     // |��������� �� ������ � ��������� �������
unsigned char *Tx_write,*Tx_read;     // |
unsigned char *comnd;                 //
unsigned char flag_USART;             // ���� ��������� �������� ������
unsigned char flag_HART_start;        //
unsigned char CDR_END_HART;           // ���� �������� ������ �����
unsigned char count_time_HART;        // ������� ��������� ����������
unsigned char time_1_HART = 2;        // 1 �������� ��� ������������� �������� USART
unsigned char time_8_HART = 4;        // 8 ���������� ��� ������������� �������� USART
unsigned char count_Rx_byte_HART;     // ������� �������� ���� � ������
float USART_temp_float;               //
unsigned char flag_HART_;             // ���� ������� ������ � �������� ������
unsigned char flag_HART_EXT;          // ���� �������� ������� � ���� ����������� �������
unsigned char DEV_ID = 0x00;          // ������� ����� ���������� �� ���������
unsigned char *ptr_HART;              //
unsigned char frame_HART = 0;         // ����� ���������
unsigned char STATUS1_HART = 0;       // ����� ��� �������� ��������� ������� � �������� �������
unsigned char STATUS2_HART = 0;       //
unsigned char DATA_NUMBER_HART;       // ���������� �������� ���� ������ � �������

#define TYPE_DEV    0xCC              // ��� ���� ����������
#define CODE_MADE   0x21              // ��� ������������
#define VERSION_NUMBER_1 0x05         // ������ ������������� ������ (��������� HART)
#define VERSION_NUMBER_2 0x05         // ������ ������������� ������
#define REV_PROG 101                  // ������ ������������ �����������
#define REV_DEV 0x03 << 3             // ������ ����������� ����������� (2)
#define DISTRIBUTE_CODE 0xFB          // ��� ������������� (none)

float damping_value = 2.500;          // ����� ������������� (����� ����������������)
unsigned char UIN [5] = {CODE_MADE, TYPE_DEV, 0x00, 0x00, 0x01}; // ���������� ����� ����������, ��� �������� � ������� ������

unsigned char PREAMBULE = 5;          // ���-�� ������ 0xFF � ��������� (����� ����������������)
unsigned char temp_HART;              //
unsigned char adres_MASTER;           // ����� �������� ���������� (0, 1)
unsigned char flag_HART_restart = 0;  // ���� ������������� ������������� ������
const unsigned char PASSWORD [6] = {'A', 's', 'b', 'M', 'k', '9'}; // ������ ��� �������� ������� � ��������� ����� ��� ������ � ���������� ���������.
unsigned char SERVICE_MODE = 0;       // ���� ������������ � ��������� �����

//************************************
//******** ���������� ������� ********
//************************************
void USART_Init (void);               // ������������� USART
void parser_USART(void);              // ������ ��������� ������
unsigned char CRC (unsigned char *nach, unsigned char dlina); // ���������� CRC
void write_to_tx_bufer (void);
void command_parcer (void);
void CRC_Tx (void);
void add_status2_HART (void);		  // �������� ������ ���� ������� � HART �����